#include "gecko_world.h"
#include <stdio.h>

mapchunk* CHUNK_ARRAY[ WORLD_SCREENS_W * WORLD_SCREENS_H ];


void init_world() {
	for (int k = 0; k < ( WORLD_SCREENS_W * WORLD_SCREENS_H ); k++) {
		CHUNK_ARRAY[k] = NULL; //so that chunks that haven't been defined yet aren't spawned 
	}
	
	char str[200];
	
	for (int cy = 0; cy < WORLD_SCREENS_H; cy++)
	for (int cx = 0; cx < WORLD_SCREENS_W; cx++) {
		sprintf(str, "map/x%dy%d%s", cx, cy, ".map"); 
		mapchunk* mc = mapchunk_load_from_file(str);
		if (mc) { printf("\nSuccesfully loaded %s", str); }
		CHUNK_ARRAY[ cx + cy * WORLD_SCREENS_W ] = mc;
	}
	
}