#ifndef _GECKO_BODY2D_H
#define _GECKO_BODY2D_H

#include <stdint.h>

typedef struct {
	
	float x,y,w,h;
	float xspeed, yspeed;
	int8_t xoffset, yoffset;
	
} body2d;

void body2d_initialize(body2d* body, float x,float y, float w, float h, int8_t xoffset, int8_t yoffset);
int  body2d_1on1_check(body2d* b1, body2d* b2);

//checks if the given body at the given coords is touching an object of type <type> that has a body2d component.
//will only look for objects in the main object list 
int  body2d_place_meeting( body2d* body, float x, float y, uint8_t type ); 

void body2d_solid_col(body2d* body); //will clamp coords and stop speed accordingly 
void body2d_platform_col(body2d* body); //will clamp coords and stop speed accordingly 

//same as place meeting but returns the address of the first thing it finds 
//return type is object* 
void* body2d_object_at( body2d* body, float x, float y, uint8_t type ); 

//result will be stored in ux,uy 
void body2d_unit_vector_at( body2d* body, float target_x, float target_y, float* ux, float* uy );

float body2d_distance_from( body2d* body, float target_x, float target_y );

#endif