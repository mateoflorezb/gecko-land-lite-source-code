#include "dhex_font.h"
#include <stdlib.h>
#include <math.h>

static inline int local_charset_from_char(char c) {
	
	if (c >= 97 && c <= 122) { //if lowercase
		c = c - 32; //to uppercase
	}
	
	switch(c) {
		
		case '0': return DHEX_FONT_CHARSET_0; break;
		case '1': return DHEX_FONT_CHARSET_1; break;
		case '2': return DHEX_FONT_CHARSET_2; break;
		case '3': return DHEX_FONT_CHARSET_3; break;
		case '4': return DHEX_FONT_CHARSET_4; break;
		case '5': return DHEX_FONT_CHARSET_5; break;
		case '6': return DHEX_FONT_CHARSET_6; break;
		case '7': return DHEX_FONT_CHARSET_7; break;
		case '8': return DHEX_FONT_CHARSET_8; break;
		case '9': return DHEX_FONT_CHARSET_9; break;
		
		case 'A': return DHEX_FONT_CHARSET_A; break;
		case 'B': return DHEX_FONT_CHARSET_B; break;
		case 'C': return DHEX_FONT_CHARSET_C; break;
		case 'D': return DHEX_FONT_CHARSET_D; break;
		case 'E': return DHEX_FONT_CHARSET_E; break;
		case 'F': return DHEX_FONT_CHARSET_F; break;
		case 'G': return DHEX_FONT_CHARSET_G; break;
		case 'H': return DHEX_FONT_CHARSET_H; break;
		case 'I': return DHEX_FONT_CHARSET_I; break;
		case 'J': return DHEX_FONT_CHARSET_J; break;
		case 'K': return DHEX_FONT_CHARSET_K; break;
		case 'L': return DHEX_FONT_CHARSET_L; break;
		case 'M': return DHEX_FONT_CHARSET_M; break;
		case 'N': return DHEX_FONT_CHARSET_N; break;
		case 'O': return DHEX_FONT_CHARSET_O; break;
		case 'P': return DHEX_FONT_CHARSET_P; break;
		case 'Q': return DHEX_FONT_CHARSET_Q; break;
		case 'R': return DHEX_FONT_CHARSET_R; break;
		case 'S': return DHEX_FONT_CHARSET_S; break;
		case 'T': return DHEX_FONT_CHARSET_T; break;
		case 'U': return DHEX_FONT_CHARSET_U; break;
		case 'V': return DHEX_FONT_CHARSET_V; break;
		case 'W': return DHEX_FONT_CHARSET_W; break;
		case 'X': return DHEX_FONT_CHARSET_X; break;
		case 'Y': return DHEX_FONT_CHARSET_Y; break;
		case 'Z': return DHEX_FONT_CHARSET_Z; break;
		
		case '-': return DHEX_FONT_CHARSET_N; break; //quick hack since we don't have - symbol yet
		
	}
	
	return DHEX_FONT_CHARSET_0;
}

void dhex_font_draw_text(dhex_font* font, dhex_palette* pal, dhex_canvas* cv, const char* txt, int align,
	int x, int y, float xscale, float yscale) {
	//distance between characters is 1 pixel (aka 1 x xscale)
	int xoffset = 0;
	int yoffset = 0;
	
	int spaceDist = floor( (float)(font->charset[0]->w) * xscale);
	
	if (align == DHEX_FONT_ALIGN_CENTERED) {
		int xlength = 0;
		int ylength = floor( (float)(font->charset[0]->h) * yscale);
							
		int cid = 0;
		while (txt[cid] != '\0') {
			
			int charset_id = DHEX_FONT_CHARSET_0;
			
			if (txt[cid] == '%') {
				cid++;
				if (txt[cid] == '\0') {break;}
				switch(txt[cid]) {
					case 'u': charset_id = DHEX_FONT_CHARSET_UP; break;
					case 'r': charset_id = DHEX_FONT_CHARSET_RIGHT; break;
					case 'd': charset_id = DHEX_FONT_CHARSET_DOWN; break;
					case 'l': charset_id = DHEX_FONT_CHARSET_LEFT; break;
				}
			}
			else {
				charset_id = local_charset_from_char(txt[cid]);
			}
			
			if (txt[cid] != ' ') {
				dhex_sprite* char_spr = font->charset[charset_id];
				xlength += floor( (float)(char_spr->w+1) * xscale );
			}
			else {
				xlength += floor( (float)(spaceDist+1) * xscale);				
			}

			cid++;
		}
		
		xoffset = xlength/2;
		yoffset = ylength/2;
	}
	
	int xlength = 0;
	int cid = 0;
	while (txt[cid] != '\0') {
		
		int charset_id = DHEX_FONT_CHARSET_0;
		
		if (txt[cid] == '%') {
			cid++;
			if (txt[cid] == '\0') {return;}
			switch(txt[cid]) {
				case 'u': charset_id = DHEX_FONT_CHARSET_UP; break;
				case 'r': charset_id = DHEX_FONT_CHARSET_RIGHT; break;
				case 'd': charset_id = DHEX_FONT_CHARSET_DOWN; break;
				case 'l': charset_id = DHEX_FONT_CHARSET_LEFT; break;
			}
		}
		
		else {
			charset_id = local_charset_from_char(txt[cid]);
		}
		
		if (txt[cid] != ' ') {
		
			dhex_sprite* char_spr = font->charset[charset_id];
			dhex_draw_sprite_ex(char_spr, pal, cv, x - xoffset + xlength, y - yoffset,
					0,0, xscale, yscale);
					
			xlength += floor( (float)(char_spr->w + 1) * xscale);
		}
		else {
			xlength += floor( (float)(spaceDist + 1) * xscale);			
		}
		
		cid++;
	}
	
}


static dhex_sprite* local_initialize_spr_from_rescomp(unsigned char* ramfile) {
	dhex_sprite* spr = malloc(sizeof(dhex_sprite));
	int32_t w, h;
	w = *(int32_t*)(&ramfile[0]);
	h = *(int32_t*)(&ramfile[4]);
	
	spr->w = w;
	spr->h = h;
	
	spr->pixeldata = malloc(sizeof(u8) * w * h);
	
	for (int yy = 0; yy < h; yy++)
	for (int xx = 0; xx < w; xx++) {
		spr->pixeldata[xx + w*yy] = ramfile[xx + w*yy + 8]; //+8 since 8 first bytes are two int32 for w and h 
	}
	
	return spr;
}

dhex_font DHEX_FONT_DEFAULT; //implementation (no initialization)

void dhex_font_init() { //initialization

	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_0] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_0);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_1] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_1);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_2] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_2);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_3] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_3);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_4] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_4);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_5] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_5);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_6] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_6);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_7] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_7);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_8] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_8);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_9] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_9);
	
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_A] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_A);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_B] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_B);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_C] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_C);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_D] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_D);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_E] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_E);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_F] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_F);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_G] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_G);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_H] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_H);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_I] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_I);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_J] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_J);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_K] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_K);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_L] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_L);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_M] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_M);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_N] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_N);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_O] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_O);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_P] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_P);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_Q] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_Q);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_R] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_R);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_S] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_S);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_T] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_T);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_U] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_U);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_V] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_V);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_W] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_W);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_X] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_X);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_Y] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_Y);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_Z] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_Z);

	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_UP]    = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_UP);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_RIGHT] = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_RIGHT);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_DOWN]  = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_DOWN);
	DHEX_FONT_DEFAULT.charset[DHEX_FONT_CHARSET_LEFT]  = local_initialize_spr_from_rescomp(_DHEX_FONT_DEFAULT_LEFT);
	
}

unsigned char _DHEX_FONT_DEFAULT_0[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,
   0,   0,   0, 0X1, 0X1,   0, 0X1,   0,   0,   0,   0, 0X1, 0X1,   0,   0, 0X1,   0,   0,   0, 0X1,
 0X1,   0,   0,   0, 0X1,   0,   0, 0X1, 0X1,   0,   0,   0,   0, 0X1,   0, 0X1, 0X1,   0,   0,   0,
   0,   0, 0X1, 0X1,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_1[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0,   0,   0,   0, 0X1,   0,   0,   0,   0,   0,   0, 0X1,
 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,
   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1,
 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0
};

unsigned char _DHEX_FONT_DEFAULT_2[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0, 0X1,   0,   0,
   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,
   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1,   0,
   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_3[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1,   0,   0,   0,
   0, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,
   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,
   0, 0X1, 0X1, 0X1,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_4[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1,   0,
 0X1, 0X1,   0,   0,   0, 0X1,   0,   0, 0X1, 0X1,   0,   0, 0X1,   0,   0,   0, 0X1, 0X1,   0,   0,
 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0,
 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0
};

unsigned char _DHEX_FONT_DEFAULT_5[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,
   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,
   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,
   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_6[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1,
 0X1,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,
 0X1, 0X1, 0X1,   0,   0, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,   0,   0, 0X1,   0,   0, 0X1, 0X1,   0,
   0, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0
};

unsigned char _DHEX_FONT_DEFAULT_7[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,
   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,
   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1,
 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0
};

unsigned char _DHEX_FONT_DEFAULT_8[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,
   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,
 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,
   0,   0,   0, 0X1,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_9[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0, 0X1,   0,   0,   0,
   0, 0X1, 0X1,   0, 0X1,   0,   0,   0,   0, 0X1, 0X1,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,
   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,
 0X1, 0X1, 0X1,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0
};

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

unsigned char _DHEX_FONT_DEFAULT_A[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,
   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1,
 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,
   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1
};

unsigned char _DHEX_FONT_DEFAULT_B[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1, 0X1,   0,
   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,
 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,
   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_C[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1, 0X1,   0,
   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0,
 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,
   0,   0,   0, 0X1,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_D[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0, 0X1, 0X1,   0,   0,
   0,   0, 0X1,   0, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1,
 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,
   0,   0, 0X1,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0
};

unsigned char _DHEX_FONT_DEFAULT_E[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,
   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,
 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,
   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1
};

unsigned char _DHEX_FONT_DEFAULT_F[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,
   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0,
 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,
   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0
};

unsigned char _DHEX_FONT_DEFAULT_G[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,
   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0,
 0X1, 0X1,   0, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,
   0,   0,   0, 0X1,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_H[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,
   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1,
 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,
   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1
};

unsigned char _DHEX_FONT_DEFAULT_I[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1,
 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,
   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1,
 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0
};

unsigned char _DHEX_FONT_DEFAULT_J[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1, 0X1,
 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,
   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1,
 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0
};

unsigned char _DHEX_FONT_DEFAULT_K[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,
 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0, 0X1, 0X1, 0X1,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,
 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1,   0, 0X1, 0X1, 0X1,   0,   0, 0X1, 0X1,   0,   0,
 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1
};

unsigned char _DHEX_FONT_DEFAULT_L[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,
   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0,
 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,
   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_M[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,
   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1, 0X1,
 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1, 0X1,   0, 0X1, 0X1, 0X1,   0,   0,
 0X1,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1
};

unsigned char _DHEX_FONT_DEFAULT_N[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0, 0X1,   0, 0X1, 0X1, 0X1, 0X1,
   0,   0, 0X1,   0, 0X1, 0X1, 0X1, 0X1,   0,   0, 0X1,   0, 0X1, 0X1,   0, 0X1, 0X1,   0, 0X1,   0,
 0X1, 0X1,   0, 0X1, 0X1,   0, 0X1,   0, 0X1, 0X1,   0,   0, 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,
 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,   0, 0X1, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_O[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,
   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1,
 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,
 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_P[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,
   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1,
 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,
   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0
};

unsigned char _DHEX_FONT_DEFAULT_Q[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,
   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1,
 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1,
 0X1,   0,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_R[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,
   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1,
 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1,   0, 0X1,
 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1
};

unsigned char _DHEX_FONT_DEFAULT_S[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,
 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,
   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,
 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_T[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,
 0X1, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,
   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1,
 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0
};

unsigned char _DHEX_FONT_DEFAULT_U[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,
   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1,
 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,
   0,   0, 0X1, 0X1,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_V[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,
   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,   0, 0X1,   0,
   0, 0X1, 0X1,   0,   0, 0X1, 0X1,   0,   0,   0, 0X1, 0X1,   0, 0X1,   0,   0,   0,   0, 0X1, 0X1,
 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0
};

unsigned char _DHEX_FONT_DEFAULT_W[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,
 0X1,   0,   0, 0X1, 0X1, 0X1,   0,   0, 0X1,   0,   0, 0X1, 0X1, 0X1,   0,   0, 0X1,   0,   0, 0X1,
 0X1, 0X1,   0, 0X1,   0, 0X1,   0, 0X1, 0X1, 0X1,   0, 0X1,   0, 0X1,   0, 0X1, 0X1, 0X1, 0X1, 0X1,
   0, 0X1, 0X1, 0X1,   0, 0X1, 0X1,   0,   0,   0, 0X1,   0
};

unsigned char _DHEX_FONT_DEFAULT_X[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1,   0,
   0, 0X1, 0X1, 0X1,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,
   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0, 0X1, 0X1, 0X1,   0,
   0, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1
};

unsigned char _DHEX_FONT_DEFAULT_Y[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1,   0,
   0, 0X1, 0X1, 0X1,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,
   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1,
 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0
};

unsigned char _DHEX_FONT_DEFAULT_Z[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1,
 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,
   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,
 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1, 0X1
};

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

unsigned char _DHEX_FONT_DEFAULT_UP[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1,
 0X1,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0, 0X1,   0,   0, 0X1,   0,   0,
   0, 0X1, 0X1,   0,   0, 0X1, 0X1,   0,   0, 0X1,   0,   0,   0,   0, 0X1,   0, 0X1, 0X1,   0,   0,
   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,   0, 0X1
};

unsigned char _DHEX_FONT_DEFAULT_RIGHT[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,
   0,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,
   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1,
   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,   0,   0
};

unsigned char _DHEX_FONT_DEFAULT_DOWN[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0, 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,
   0,   0, 0X1, 0X1,   0, 0X1,   0,   0,   0,   0, 0X1,   0,   0, 0X1, 0X1,   0,   0, 0X1, 0X1,   0,
   0,   0, 0X1,   0,   0, 0X1,   0,   0,   0,   0, 0X1, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0, 0X1,
 0X1,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0
};

unsigned char _DHEX_FONT_DEFAULT_LEFT[72] = {
 0X8,   0,   0,   0, 0X8,   0,   0,   0,   0,   0,   0,   0,   0,   0, 0X1, 0X1,   0,   0,   0,   0,
 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,
 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,   0,   0, 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,   0,   0,
 0X1, 0X1, 0X1,   0,   0,   0,   0,   0,   0,   0, 0X1, 0X1
};