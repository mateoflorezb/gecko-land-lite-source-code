#include "bit1cd.h"
#include "stb_image.h"
#include <string.h>
#include <math.h>
#include <stdio.h>

static inline float _float_abs(float v) {
	if (v < 0) return -v;
	return v;
}

////////////////////////////////////////////////////////////////////////
bit1cd_canvas* bit1cd_create_canvas(int16_t w, int16_t h) {
	bit1cd_canvas* cv = malloc(sizeof(bit1cd_canvas));
	cv->buffer.w = w;
	cv->buffer.h = h;
	
	int npixels = ((int)w) * ((int)h);
	
	//+1 to have enough mem since I think integer division will round down
	int32_t nbytes = sizeof(uint8_t) * ( 1 + (npixels / 4) );
	cv->buffer.data = malloc(nbytes);
	cv->buffer.nbytes = nbytes;
	
	cv->current_colormode = BIT1CD_COLORMODE_NORMAL;
	
	bit1cd_clear_canvas(cv);
	
	return cv;
}

////////////////////////////////////////////////////////////////////////
void bit1cd_free_canvas(bit1cd_canvas* cv) {
	free(cv->buffer.data);
	free(cv);
}

////////////////////////////////////////////////////////////////////////
void bit1cd_clear_canvas(bit1cd_canvas* cv) {
	memset(cv->buffer.data, 0, cv->buffer.nbytes); //clear pixel data to transparent light 
}

//support data for the below function
//access is [pixel_offset]
static const uint8_t _local_support_bitmasks[4] = {
	0b00111111,
	0b11001111,
	0b11110011,
	0b11111100
};

//only first 2 bits of <value> are taken
//ANY (x,y) COORD NOT INSIDE THE ACTUAL PIXELSPACE IS CONSIDERED UNDEFINED BEHAVIOUR
//The function will assume that <value> has every unused bit SET TO 0 (aka, bitwise-and with & 0b00000011 before passing)
static inline void _local_bit1cd_set_pixelvalue(bit1cd_pixelbuffer* buffer, int16_t x, int16_t y, uint8_t value) {
	int nth_pixel = (int)x + ((int)buffer->w) * (int)y;
	int nth_byte  = nth_pixel / 4;
	int pixel_offset = nth_pixel % 4;
	//value = value & 0b00000011; //raise every unused bit to 0
	value = value << ( (3 - pixel_offset) * 2 ); //move relevant pixel data into their relative byte position 
	buffer->data[nth_byte] = buffer->data[nth_byte] | value; //raise relevant bits to 1
	buffer->data[nth_byte] = buffer->data[nth_byte] & (value | _local_support_bitmasks[pixel_offset] ); //raise relevant bits to 0
}

static inline void _local_bit1cd_pixel_normal(bit1cd_pixelbuffer* buffer, int16_t x, int16_t y, uint8_t source) {
	if ( (source & BIT1CD_COLORFORMAT_ALPHA) == 0) return; //if transparent do nothing 
	_local_bit1cd_set_pixelvalue(buffer, x, y, source); //just set the pixel color 
}

static inline void _local_bit1cd_pixel_sub(bit1cd_pixelbuffer* buffer, int16_t x, int16_t y, uint8_t source) {
	if ( (source & BIT1CD_COLORFORMAT_ALPHA) == 0) return; //if transparent do nothing 
	_local_bit1cd_set_pixelvalue(buffer, x, y, BIT1CD_COLOR_NONE); //just set the pixel color to none
}

static inline void _local_bit1cd_pixel_invert(bit1cd_pixelbuffer* buffer, int16_t x, int16_t y, uint8_t source) {
	if ( (source & BIT1CD_COLORFORMAT_ALPHA) == 0) return; //if transparent do nothing 
	uint8_t dest = bit1cd_read_pixelvalue(buffer, x, y);
	//xor is sort of "swap bits that have 1 in the mask"
	_local_bit1cd_set_pixelvalue(buffer, x, y, dest ^ BIT1CD_COLORFORMAT_COLOR); 
}

static inline void _local_bit1cd_pixel_invert_source(bit1cd_pixelbuffer* buffer, int16_t x, int16_t y, uint8_t source) {
	if ( (source & BIT1CD_COLORFORMAT_ALPHA) == 0) return; //if transparent do nothing 
	//xor is sort of "swap bits that have 1 in the mask"
	_local_bit1cd_set_pixelvalue(buffer, x, y, source ^ BIT1CD_COLORFORMAT_COLOR);
}

//////////////////////////////////////////////////////////////////////
bit1cd_pixelbuffer* bit1cd_load_sprite_png(const char* filename) {
	
	int w, h, n;
	uint8_t* file_data = stbi_load(filename, &w, &h, &n, 4);
	if ( file_data ) {
		bit1cd_sprite* new_spr = malloc(sizeof(bit1cd_sprite));
		new_spr->w = (uint8_t)w;
		new_spr->h = (uint8_t)h;
		
		int npixels = w*h;
		//+1 to have enough mem since I think integer division will round down
		int32_t nbytes = sizeof(uint8_t) * ( 1 + (npixels / 4) );
		new_spr->data = malloc(nbytes);
		memset(new_spr->data, 0x00, nbytes);
		//new_spr->nbytes = nbytes;
		
		for (int yy = 0; yy < h; yy++) 
		for (int xx = 0; xx < w; xx++) {

			uint8_t rchannel = file_data[4*(xx + w*yy)];
			uint8_t achannel = file_data[4*(xx + w*yy) + 3];
			
			uint8_t color = BIT1CD_COLOR_NONE;
			if (achannel > 127) {
				color = ( rchannel < 127 ) ? BIT1CD_COLOR_DARK : BIT1CD_COLOR_LIGHT;
			}
			
			_local_bit1cd_set_pixelvalue( new_spr, xx, yy, color );// & 0b00000011);
		}

		stbi_image_free(file_data);
		return new_spr;
	}
	
	return NULL;	
}

////////////////////////////////////////////////////////////////////////
int bit1cd_dump_sprite_bin(bit1cd_sprite* spr, const char* filename) {
	FILE* f = fopen(filename, "wb");
	if (!f) { return 0; }
	
	fwrite(&spr->w, sizeof(uint8_t), 1, f);
	fwrite(&spr->h, sizeof(uint8_t), 1, f);
	
	for (int16_t py = 0; py < (int16_t)spr->h; py++)
	for (int16_t px = 0; px < (int16_t)spr->w; px++) {
		uint8_t color = bit1cd_read_pixelvalue(spr, px, py);
		fwrite(&color, sizeof(uint8_t), 1, f);
	}
	
	fclose(f);
	return 1;
}

////////////////////////////////////////////////////////////////////////
bit1cd_sprite* bit1cd_load_sprite_memory(const uint8_t* rescomp_file) {
	bit1cd_sprite* spr = malloc(sizeof(bit1cd_sprite));
	spr->w = rescomp_file[0];
	spr->h = rescomp_file[1];
	
	int npixels = (int)spr->w * (int)spr->h;
	//+1 to have enough mem since I think integer division will round down
	int32_t nbytes = sizeof(uint8_t) * ( 1 + (npixels / 4) );
	spr->data = malloc(nbytes);
	memset(spr->data, 0x00, nbytes);
	
	for (int16_t yy = 0; yy < spr->h; yy++) 
	for (int16_t xx = 0; xx < spr->w; xx++) {
		uint8_t color = rescomp_file[2 + xx + spr->w * yy]; // +2 since first two bytes are w,h
		_local_bit1cd_set_pixelvalue( spr, xx, yy, color );
	}

	return spr;
	
}

////////////////////////////////////////////////////////////////////////
void bit1cd_free_sprite(bit1cd_sprite* s) {
	free(s->data);
	free(s);
}

////////////////////////////////////////////////////////////////////////
void bit1cd_set_colormode(bit1cd_canvas* cv, uint8_t colormode) { cv->current_colormode = colormode; }

////////////////////////////////////////////////////////////////////////


#define _FIND_PIXEL_FUNCTION \
	void (*pixel_function)(bit1cd_pixelbuffer*, int16_t, int16_t, uint8_t) = NULL; \
	switch(cv->current_colormode) { \
		\
		case BIT1CD_COLORMODE_NORMAL:        pixel_function = &_local_bit1cd_pixel_normal;        break; \
		case BIT1CD_COLORMODE_SUB_ALPHA:     pixel_function = &_local_bit1cd_pixel_sub;           break; \
		case BIT1CD_COLORMODE_INVERT:        pixel_function = &_local_bit1cd_pixel_invert;        break; \
		case BIT1CD_COLORMODE_INVERT_SOURCE: pixel_function = &_local_bit1cd_pixel_invert_source; break; \
		\
		default: break; \
	}

void bit1cd_draw_rect(bit1cd_canvas* cv, int16_t x, int16_t y, int16_t w, int16_t h, uint8_t color) {
	
	_FIND_PIXEL_FUNCTION;

	if (x < 0) {
		w += x;
		x = 0;
	}
	
	if (y < 0) {
		h += y;
		y = 0;
	}
	
	int16_t xtarget = x + w;
	int16_t ytarget = y + h;
	
	if (xtarget > cv->buffer.w) { xtarget = cv->buffer.w; }
	if (ytarget > cv->buffer.h) { ytarget = cv->buffer.h; }
	
	for (int16_t py = y; py < ytarget; py++)
	for (int16_t px = x; px < xtarget; px++) {
		pixel_function(&cv->buffer, px, py, color); 
	}
	
	
}

void bit1cd_draw_sprite(bit1cd_canvas* cv, int16_t x, int16_t y, int16_t xoffset, int16_t yoffset,
	uint8_t flip_flags, bit1cd_sprite* sprite) {
	
	_FIND_PIXEL_FUNCTION;
	
	if ( (flip_flags & BIT1CD_FLIP_HORIZONTAL) == 0 ) 
		x = x - xoffset;
	else 
		x = x - (sprite->w - xoffset); 
	
	if ( (flip_flags & BIT1CD_FLIP_VERTICAL) == 0 ) 
		y = y - yoffset;
	else 
		y = y - (sprite->h - yoffset); 
	
	int16_t image_xref = x;
	int16_t image_yref = y;
	
	int16_t w,h;
	w = sprite->w;
	h = sprite->h;
	
	uint8_t hflip = 0, vflip = 0;
	if ( (flip_flags & BIT1CD_FLIP_HORIZONTAL) != 0) hflip = 1;
	if ( (flip_flags & BIT1CD_FLIP_VERTICAL  ) != 0) vflip = 1;
	
	if (hflip == 1) image_xref = image_xref + w - 1; //the -1 makes sense when looking at the _LOCAL_RASTER's
	if (vflip == 1) image_yref = image_yref + h - 1;
	
	if (x < 0) {
		w += x;
		x = 0;
	}
	
	if (y < 0) {
		h += y;						
		y = 0;
	}
	
	int16_t xtarget = x + w;
	int16_t ytarget = y + h;
	
	if (xtarget > cv->buffer.w) { xtarget = cv->buffer.w; }
	if (ytarget > cv->buffer.h) { ytarget = cv->buffer.h; }	
	
	#define _LOCAL_RASTER(sample_x, sample_y) \
	for (int16_t py = y; py < ytarget; py++) \
	for (int16_t px = x; px < xtarget; px++) { \
		pixel_function(&cv->buffer, px, py, \
			bit1cd_read_pixelvalue(sprite, (sample_x), (sample_y)) ); \
	}
	
	if (hflip == 0 && vflip == 0) { _LOCAL_RASTER(px - image_xref, py - image_yref); return; }
	if (hflip == 0 && vflip == 1) { _LOCAL_RASTER(px - image_xref, image_yref - py); return; }
	if (hflip == 1 && vflip == 0) { _LOCAL_RASTER(image_xref - px, py - image_yref); return; }
	if (hflip == 1 && vflip == 1) { _LOCAL_RASTER(image_xref - px, image_yref - py); return; }
	
	#undef _LOCAL_RASTER
	
}


////////////////////////////////////////////////////////////////////////
void bit1cd_draw_sprite_ex(bit1cd_canvas* cv, int16_t x, int16_t y, int16_t xoffset, int16_t yoffset,
	float xscale, float yscale, bit1cd_sprite* sprite) {
		
	_FIND_PIXEL_FUNCTION;
		
	if (xscale > 0) 
		x = x - floor( ((float)xoffset) * xscale);
	else 
		x = x + floor( (float)(sprite->w - xoffset) * xscale);
	
	if (yscale > 0) 
		y = y - floor( ((float)yoffset) * yscale);
	else 
		y = y + floor( (float)(sprite->h - yoffset) * yscale);
	
	int16_t image_xref = x;
	int16_t image_yref = y;
	
	int16_t w,h;
	w = sprite->w * _float_abs(xscale);
	h = sprite->h * _float_abs(yscale);
	
	int16_t scaled_w = w;
	int16_t scaled_h = h;
	
	int16_t source_w = sprite->w;
	int16_t source_h = sprite->h;
	
	if (x < 0) {
		w += x;
		x = 0;
	}
	
	if (y < 0) {
		h +=y;						
		y = 0;
	}

	int16_t xtarget = x + w;
	int16_t ytarget = y + h;
	
	if (xtarget > cv->buffer.w) { xtarget = cv->buffer.w; }
	if (ytarget > cv->buffer.h) { ytarget = cv->buffer.h; }		

	#define _LOCAL_RASTER(sample_x, sample_y) \
	for (int16_t py = y; py < ytarget; py++) \
	for (int16_t px = x; px < xtarget; px++) { \
		int16_t mgx = sample_x; \
		if (mgx >= source_w) mgx = source_w - 1; \
		int16_t mgy = sample_y; \
		if (mgy >= source_h) mgy = source_h - 1; \
		pixel_function(&cv->buffer, px, py, \
			bit1cd_read_pixelvalue(sprite, mgx, mgy ) ); \
	}

	#define _SAMPLE_XSCALE_PLUS ( ((float)source_w) * ( (float)(px - image_xref) / (float)(scaled_w) ) )
	#define _SAMPLE_YSCALE_PLUS ( ((float)source_h) * ( (float)(py - image_yref) / (float)(scaled_h) ) )
	
	#define _SAMPLE_XSCALE_MINUS (source_w - 1) - ( ((float)source_w) * ( (float)(px - image_xref) / (float)(scaled_w) ) )
	#define _SAMPLE_YSCALE_MINUS (source_h - 1) - ( ((float)source_h) * ( (float)(py - image_yref) / (float)(scaled_h) ) )
	
	if (xscale > 0 && yscale > 0) { _LOCAL_RASTER(_SAMPLE_XSCALE_PLUS , _SAMPLE_YSCALE_PLUS ); return; }
	if (xscale > 0 && yscale < 0) { _LOCAL_RASTER(_SAMPLE_XSCALE_PLUS , _SAMPLE_YSCALE_MINUS); return; }	
	if (xscale < 0 && yscale > 0) { _LOCAL_RASTER(_SAMPLE_XSCALE_MINUS, _SAMPLE_YSCALE_PLUS ); return; }
	if (xscale < 0 && yscale < 0) { _LOCAL_RASTER(_SAMPLE_XSCALE_MINUS, _SAMPLE_YSCALE_MINUS); return; }
	
	#undef _SAMPLE_XSCALE_PLUS
	#undef _SAMPLE_XSCALE_MINUS
	#undef _SAMPLE_YSCALE_PLUS
	#undef _SAMPLE_YSCALE_MINUS
	#undef _LOCAL_RASTER
	
}


////////////////////////////////////////////////////////////////////////
void bit1cd_draw_sprite_part(bit1cd_canvas* cv, int16_t x, int16_t y, int16_t xoffset, int16_t yoffset,
	int16_t xsection, int16_t ysection, int16_t wsection, int16_t hsection, bit1cd_sprite* sprite) {
	
	_FIND_PIXEL_FUNCTION;

	x = x - xoffset;
	y = y - yoffset;
	
	int16_t image_xref = x;
	int16_t image_yref = y;
	
	int16_t w,h;
	w = sprite->w;
	h = sprite->h;
	
	if (w > wsection) { w = wsection; }
	if (h > hsection) { h = hsection; }
	
	if (xsection > 0) {
		x += xsection;
		//w -= xsection;
	}
	
	if (ysection > 0) {
		y += ysection;
		//h -= ysection;
	}
	
	if (x < 0) {
		w += x;
		x = 0;
	}
	
	if (y < 0) {
		h += y;						
		y = 0;
	}

	int16_t xtarget = x + w;
	int16_t ytarget = y + h;
	
	if (xtarget > cv->buffer.w) { xtarget = cv->buffer.w; }
	if (ytarget > cv->buffer.h) { ytarget = cv->buffer.h; }	
	
	for (int16_t py = y; py < ytarget; py++) 
	for (int16_t px = x; px < xtarget; px++) {
		pixel_function(&cv->buffer, px, py,
			bit1cd_read_pixelvalue(sprite, px - image_xref, py - image_yref ) ); 
	}
	
}