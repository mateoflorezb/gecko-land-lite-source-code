#ifndef _GECKO_INPUT_H
#define _GECKO_INPUT_H

//Little interface in case I want to port this to something else 

enum _INPUTS {
	INPUT_A = 0,
	INPUT_B,
	INPUT_R,
	INPUT_L,
	INPUT_U,
	INPUT_D,
	INPUT_START,
	INPUT_MAX
};

//assumes global is already init 
void input_init();

int input_pressed (int input);
int input_held    (int input);
int input_released(int input);

#endif 