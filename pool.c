#include "pool.h"

pool* pool_create(uint16_t element_size, uint16_t nelements) {
	
	pool* P = malloc(sizeof(pool));
	P->nelements = nelements;
	P->memblock = malloc( element_size * nelements );
	P->stack = malloc( sizeof(void*) * nelements );
	P->stack_pointer = (int) (nelements - 1);
	
	for (int k = 0; k < nelements; k++) {
		P->stack[k] = (void*) ((uintptr_t) P->memblock + (uintptr_t)( element_size * k ));
	}	
	
	return P;
}

void* pool_get(pool* P) {
	
	if ( P->stack_pointer < 0 ) { return NULL; }
	void* mem = P->stack[P->stack_pointer];
	P->stack_pointer--;
	return mem;
}

void pool_release(pool* P, void* a) {
	P->stack_pointer++;
	P->stack[P->stack_pointer] = a;
}

void pool_free(pool* P) {
	free(P->memblock);
	free(P->stack);
	free(P);
}