#include "gecko_mapchunk.h"
#include "gecko_misc.h"
#include <stdio.h>
#include <stdlib.h>
#include "gecko_magic.h"
#include "gecko_sprite.h"

mapchunk* mapchunk_new() {
	
	mapchunk* mc = malloc(sizeof(mapchunk));
	mc->bg_color = 0;
	mc->shadow   = 0;
	mc->heat     = 0;
	
	for (int ty = 0; ty < MAPCHUNK_TILE_H; ty++)
	for (int tx = 0; tx < MAPCHUNK_TILE_W; tx++) {
		int id = tx + MAPCHUNK_TILE_W * ty;
		mc->tiledata[id] = MAPCHUNK_NULL_TILE;
	}
	
	mc->nodes = 0;
	mc->areas = 0;
	
	mc->node_list = clist_create(mapchunk_node);
	mc->area_list = clist_create(mapchunk_area);
	
	return mc;
}

void mapchunk_dump_file(mapchunk* mc, const char* filename) {
	
	FILE* f = fopen(filename, "wb");
	if (!f) { return; }
	
	fwrite(&mc->bg_color, sizeof(uint8_t), 1, f);
	fwrite(&mc->shadow,   sizeof(uint8_t), 1, f);
	fwrite(&mc->heat,     sizeof(uint8_t), 1, f);
	
	for (int ty = 0; ty < MAPCHUNK_TILE_H; ty++)
	for (int tx = 0; tx < MAPCHUNK_TILE_W; tx++) {
		int id = tx + MAPCHUNK_TILE_W * ty;
		//could probably write the whole block with 1 call but idk if the compiler will add sneaky padding?
		fwrite( &mc->tiledata[id], sizeof(uint8_t), 1, f);
	}
	
	fwrite(&mc->nodes, sizeof(uint8_t), 1, f);
	
	clist_iterator it = clist_start(mc->node_list);
	while (it != NULL) {
		
		fwrite( &clist_get(it, mapchunk_node).type, sizeof(uint8_t), 1, f);
		fwrite( &clist_get(it, mapchunk_node).data, sizeof(uint8_t), 1, f);
		fwrite( &clist_get(it, mapchunk_node).x,    sizeof(uint8_t), 1, f);
		fwrite( &clist_get(it, mapchunk_node).y,    sizeof(uint8_t), 1, f);
		
		it = clist_next(it);
	}
	
	//now areas 
	fwrite(&mc->areas, sizeof(uint8_t), 1, f);
	
	it = clist_start(mc->area_list);
	while (it != NULL) {
		
		fwrite( &clist_get(it, mapchunk_area).type, sizeof(uint8_t), 1, f);
		fwrite( &clist_get(it, mapchunk_area).data, sizeof(uint8_t), 1, f);
		fwrite( &clist_get(it, mapchunk_area).x,    sizeof(uint8_t), 1, f);
		fwrite( &clist_get(it, mapchunk_area).y,    sizeof(uint8_t), 1, f);
		fwrite( &clist_get(it, mapchunk_area).w,    sizeof(uint8_t), 1, f);
		fwrite( &clist_get(it, mapchunk_area).h,    sizeof(uint8_t), 1, f);
		
		it = clist_next(it);
	}
	
	fclose(f);
	
}

mapchunk* mapchunk_load_from_file(const char* filename) {
	uint8_t* memblock = load_file_memblock(filename);
	if (!memblock) return NULL;
	mapchunk* mc = mapchunk_load_from_memblock(memblock);
	free(memblock);
	return mc;
}

mapchunk* mapchunk_load_from_memblock(const uint8_t* memblock) {
	
	if (!memblock) return NULL;
	
	mapchunk* mc = mapchunk_new();
	
	mc->bg_color = memblock[0];
	mc->shadow   = memblock[1];
	mc->heat     = memblock[2];
	
	int memPointer = 3;
	
	for (int ty = 0; ty < MAPCHUNK_TILE_H; ty++)
	for (int tx = 0; tx < MAPCHUNK_TILE_W; tx++) {
		int id = tx + MAPCHUNK_TILE_W * ty;
		mc->tiledata[id] = memblock[memPointer];
		memPointer++;
	}
	
	mc->nodes = memblock[memPointer];
	memPointer++;
	
	for (int k = 0; k < mc->nodes; k++) {
		mapchunk_node node;
		node.type = memblock[ memPointer    ];
		node.data = memblock[ memPointer + 1];
		node.x    = memblock[ memPointer + 2];
		node.y    = memblock[ memPointer + 3];
		
		clist_push(mc->node_list, &node);
		memPointer += 4;
	}
	
	mc->areas = memblock[memPointer];
	memPointer++;
	
	for (int k = 0; k < mc->areas; k++) {
		
		mapchunk_area area;
		area.type = memblock[ memPointer    ];
		area.data = memblock[ memPointer + 1];
		area.x    = memblock[ memPointer + 2];
		area.y    = memblock[ memPointer + 3];
		area.w    = memblock[ memPointer + 4];
		area.h    = memblock[ memPointer + 5];
		
		clist_push(mc->area_list, &area);
		memPointer += 6;
	}
	
	return mc;
}

void mapchunk_free(mapchunk* mc) {
	clist_free(mc->node_list);
	clist_free(mc->area_list);
	free(mc);
}

void mapchunk_draw(bit1cd_canvas* cv, int16_t x, int16_t y, mapchunk* mc) {
	
	bit1cd_set_colormode( cv, BIT1CD_COLORMODE_NORMAL );
	//uint8_t bg_color = BIT1CD_COLOR_LIGHT;
	//if (mc->bg_color) { bg_color = BIT1CD_COLOR_DARK; }
	//bit1cd_draw_rect( cv, x, y, SCREEN_W, SCREEN_H, bg_color );
	
	if (mc->bg_color) bit1cd_draw_rect( cv, x, y, SCREEN_W, SCREEN_H, BIT1CD_COLOR_DARK );
	
	//now for the tiles 
	for (int ty = 0; ty < MAPCHUNK_TILE_H; ty++)
	for (int tx = 0; tx < MAPCHUNK_TILE_W; tx++) {
		int id = tx + MAPCHUNK_TILE_W * ty;
		uint8_t tile_id = mc->tiledata[id];
		
		if (tile_id == MAPCHUNK_NULL_TILE) { continue; }
		
		int tile_type_x = tile_id % 7;
		int tile_type_y = tile_id / 7;
		
		bit1cd_draw_sprite_part(cv,
			x + 8*tx - 1 - 9 * tile_type_x,
			y + 8*ty - 1 - 9 * tile_type_y,
			
			0,0,
				
			1 + 9 * tile_type_x, //section 
			1 + 9 * tile_type_y,
			8, 8,
			SPR_TILESET
		);
		
	}
}