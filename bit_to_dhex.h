#ifndef _BIT_TO_DHEX_H
#define _BIT_TO_DHEX_H

#include "bit1cd.h"
#include "dhex_draw.h"

//copy a bit1cd pixelbuffer into a dhex image (canvas or otherwise)
//they are assumed to be the exact same dimensions 
void bit_to_dhex(dhex_rgba dark_color, dhex_rgba light_color, bit1cd_pixelbuffer* bitbuffer, dhex_image* rgb_buffer);


#endif