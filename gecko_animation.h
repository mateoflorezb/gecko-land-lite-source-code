#ifndef _GECKO_ANIMATION_H
#define _GECKO_ANIMATION_H

#include "gecko_sprite.h"

#define PARTICLE_NFRAMES (5)
#define DEATH_PARTICLE_NFRAMES (4)

extern bit1cd_sprite* ANIMATION_PARTICLE[ PARTICLE_NFRAMES ];
extern bit1cd_sprite* ANIMATION_DEATH_PARTICLE[ DEATH_PARTICLE_NFRAMES ];

void init_animation();

#endif 