#include "bit_to_dhex.h"

void bit_to_dhex(dhex_rgba dark_color, dhex_rgba light_color, bit1cd_pixelbuffer* bitbuffer, dhex_image* rgb_buffer) {
	dhex_rgba* pdata = rgb_buffer->pixeldata;
	int w = rgb_buffer->w;
	int h = rgb_buffer->h;
	
	for (int py = 0; py < h; py++)
	for (int px = 0; px < w; px++) {
		uint8_t pval = bit1cd_read_pixelvalue( bitbuffer, px, py );
		dhex_rgba p_color = light_color;
		if ( (pval & BIT1CD_COLORFORMAT_COLOR) != 0 ) { p_color = dark_color; }
		
		//dhex_draw_rect(p_color, app->main_canvas, px, py, 1, 1);
		pdata[px + py * w] = p_color;
	}
}