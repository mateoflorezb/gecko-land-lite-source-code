#include "gecko_gamestate.h"
#include "gecko_object.h"
#include "gecko_misc.h"
#include "gecko_magic.h"
#include "gecko_global.h"

#include "obj/block.h"
#include "obj/water.h"
#include "obj/pipe.h"
#include "obj/spike.h"
#include "obj/fly.h"
#include "obj/blob.h"
#include "obj/turret.h"
#include "obj/save.h"
#include "obj/rock.h"
#include "obj/item.h"
#include "obj/portal.h"
#include "obj/moongazer.h"
#include "obj/tec.h"

#include "gecko_sprite.h"

#include <string.h>
#include <stdio.h>

void gamestate_initialize(gamestate* gs) {
	gs->tickcount = 0;
	gs->real_tickcount = 0;
	gs->pause_state = 1;
	gs->queued_pause_state = 1;
	gs->id_counter = 0;
	gs->heat_intensity = 0.0;
	
	gs->main_object_queue = clist_create( object* );
	gs->main_object_list  = clist_create( object* );
	gs-> sub_object_queue = clist_create( object* );
	gs-> sub_object_list  = clist_create( object* );
	
	gs->cam_x = 0;
	gs->cam_y = 0;
	
	gs->chunk_x = -10; // -10 so that the first inbounds chunk spawn call will actually spawn stuff 
	gs->chunk_y = -10;
	
	gs->checkpoint_x = 2; //initial screen 
	gs->checkpoint_y = 1;
	
	gs->centered_in_shadow = 0;
	gs->queued_message = 0;
	
	gs->credits = 0;
	
	//memset( gs->unlocks, 0, sizeof( gs->unlocks ) );
	memset( gs->unlocks, 0, sizeof( gs->unlocks ) );
	memset( gs->checkpoint_unlocks, 0, sizeof( gs->checkpoint_unlocks ) );
	
	//gs->unlocks[ UNLOCKED_FEATHER ] = 1;
	
	gs->midnight = 0;
	gs->checkpoint_midnight = 0;
	
	memset( gs->known_mapchunks, 0, sizeof( gs->known_mapchunks ) );
	
	gamestate_set_camera(gs, 2 * SCREEN_W, 0 );
	gamestate_set_chunk( gs, 2, 0 );
	
	gs->app_signal.current_palette = PALETTE_BASE;
	
	gs->app_signal.n_sfx = 0;
	gs->app_signal.music_volume = MAX_VOLUME;
	gs->app_signal.music_action = MUSIC_NOP;
	
	gs->app_signal.waterfall_volume = 0.0;
	
	gs->music_vdelta = 0;
	
	gs->settings.heat_envelope = HEAT_ENVELOPE_RANGE;
	gs->settings.music_envelope = VOLUME_ENVELOPE_RANGE;
	gs->settings.sfx_envelope   = VOLUME_ENVELOPE_RANGE;
	
	memset( gs->checkpoint_locations, 0, sizeof( gs->checkpoint_locations ) );
	
	for (int cy = 0; cy < WORLD_SCREENS_H; cy++)
	for (int cx = 0; cx < WORLD_SCREENS_W; cx++) {
		
		int id = cx + cy * WORLD_SCREENS_W;	
		mapchunk* mc = CHUNK_ARRAY[id];
		if (mc) {
			clist_iterator it = clist_start(mc->node_list);
			while (it) {
				
				if ( clist_get(it, mapchunk_node).type == NODE_SAVE ) {
					gs->checkpoint_locations[id] = 1;
					break;
				}
				
				it = clist_next(it);
			}
		}
	}
}

void gamestate_step(gamestate* gs) {
	
	gamestate_music_volume( gs, gs->app_signal.music_volume + gs->music_vdelta );
	
	gs->app_signal.music_action = MUSIC_NOP;
	gs->app_signal.n_sfx = 0;
	gs->pause_state = gs->queued_pause_state;
	
	gs->real_tickcount++;
	
	if (!gs->pause_state)
	gs->tickcount++;
	
	//check destroyed objects 
	for (int k = 0; k < 2; k++) {
		
		clist* list = ( k == 0 ) ? gs->main_object_list : gs->sub_object_list;
		
		clist_iterator it = clist_start( list );
		while (it != NULL) {
			
			clist_iterator next = clist_next( it );
			object* obj = clist_get( it, object* );
			
			if ( obj->destroy ) {
				
				obj->release( obj );
				pool_release( global.object_pool, obj ); 
				clist_delete( it );
			}
			
			it = next;	
		}	
	}
	
	//check queue 
	for (int k = 0; k < 2; k++) {
		
		clist* queue;
		clist* list; 
		
		if (k == 0) {
			queue = gs->main_object_queue;
			list  = gs->main_object_list;
		}
		else {
			queue = gs->sub_object_queue;
			list  = gs->sub_object_list;
		}
		
		clist_iterator it = clist_start( queue );
		while (it != NULL) {
			
			object* obj = clist_get(it, object*);
			clist_push( list, &obj );
			it = clist_next(it);
		}
		
		clist_clear( queue );
	}
	
	//step stuff 
	for (int step = STEP_0; step <= STEP_2; step++) 
	for (int k = 0; k < 2; k++) {
		
		clist* list = ( k == 0 ) ? gs->main_object_list : gs->sub_object_list;
		
		clist_iterator it = clist_start( list );
		while (it != NULL) {
			
			object* obj = clist_get( it, object* );
			if ( !obj->destroy )
			if ( !gs->pause_state || obj->ignore_pause ) { obj->step(gs, obj, step); }
			
			it = clist_next( it );
		}	
	}

	gs->centered_in_shadow = 0;
	
	const float heat_speed = 1.0 / 20.0;
	int chunk_id = gs->chunk_x + gs->chunk_y * WORLD_SCREENS_W;
	mapchunk* mc = CHUNK_ARRAY[chunk_id];
	
	int apply_heat = 0;
	
	if (mc) {
	
		if ( mc->heat )   { apply_heat = 1; }    
		if ( mc->shadow ) { gs->centered_in_shadow = 1; }
	}
	
	if (apply_heat) { gs->heat_intensity += heat_speed; }
	else            { gs->heat_intensity -= heat_speed; }
	
	gs->heat_intensity = fclamp( gs->heat_intensity, 0, 1 );
	
	
	gs->known_mapchunks[ chunk_id ] = 1;
	
	//app signal stuff 
	gs->app_signal.current_palette = CHUNK_PALETTE[ chunk_id ];
	
	//printf("N objects: %d\n", (int)clist_size(gs->main_object_list) + (int)clist_size(gs->sub_object_list)  );
	//printf("Object pool stack: %d\n", global.object_pool->stack_pointer );
}

void gamestate_draw(gamestate* gs) {
	
	bit1cd_clear_canvas( global.main_canvas );
	bit1cd_clear_canvas( global.heat_canvas );
	bit1cd_clear_canvas( global.shadow_canvas );
	
	uint8_t color = (gs->midnight) ? BIT1CD_COLOR_DARK : BIT1CD_COLOR_LIGHT;
	bit1cd_draw_rect( global.main_canvas, 0, 0, SCREEN_W, SCREEN_H, color );
	
	for ( int k = 0; k < 2; k++ ) {
		
		clist* list = ( k == 0 ) ? gs->main_object_list : gs->sub_object_list;
		
		clist_iterator it = clist_start( list );
		while (it != NULL) {
			
			object* obj = clist_get( it, object* );
			if ( !obj->destroy ) { obj->draw( gs, obj, LAYER_PRE_TILES ); }
			
			it = clist_next( it );
		}	
	}
	
	for (int dy = -1; dy <= 1; dy++)
	for (int dx = -1; dx <= 1; dx++) {
		
		int chunk_x = ((int)gs->chunk_x) + dx;
		int chunk_y = ((int)gs->chunk_y) + dy;
		if (chunk_x < 0 || chunk_x >= WORLD_SCREENS_W) { continue; }
		if (chunk_y < 0 || chunk_y >= WORLD_SCREENS_H) { continue; }
		
		mapchunk* mc = CHUNK_ARRAY[ chunk_x + WORLD_SCREENS_W * chunk_y ];
		
		if (!mc) { continue; }
		
		int16_t chunk_px = (int16_t)(chunk_x * SCREEN_W) - gs->cam_x;
		int16_t chunk_py = (int16_t)(chunk_y * SCREEN_H) - gs->cam_y;
		
		mapchunk_draw( global.main_canvas, chunk_px, chunk_py, mc );
		
		if (mc->shadow) {
			bit1cd_draw_rect( global.shadow_canvas, chunk_px, chunk_py, SCREEN_W, SCREEN_H, BIT1CD_COLOR_DARK );
		}
		
		//mega temp 
		//if ( chunk_x == 2 && chunk_y == 0 )
		//bit1cd_draw_sprite( global.main_canvas, chunk_px, chunk_py, -10, -10,
		//0, SPR_TITLE);
		
	}
	
	for ( int layer = LAYER_START; layer < LAYER_END; layer++ ) 
	for ( int k = 0; k < 2; k++ ) {
		
		clist* list = ( k == 0 ) ? gs->main_object_list : gs->sub_object_list;
		
		clist_iterator it = clist_start( list );
		while (it != NULL) {
			
			object* obj = clist_get( it, object* );
			if ( !obj->destroy ) { obj->draw(gs, obj, layer); }
			
			it = clist_next( it );
		}	
	}	
}

void gamestate_spawn_chunk(gamestate* gs, mapchunk* mc, int16_t x, int16_t y, int16_t origin_chunk_id ) {
	
	if (!mc) return;
	
	//nodes 
	clist_iterator it = clist_start(mc->node_list);
	while (it != NULL) {
		
		switch ( clist_get( it, mapchunk_node).type ) {
			
			case NODE_PIPE:
			{	
				object* pipe = create_pipe(
					x + (int)clist_get( it, mapchunk_node ).x,
					y + (int)clist_get( it, mapchunk_node ).y
				);
				
				pipe->origin_chunk_id = origin_chunk_id;
			}
			break;
			
			case NODE_SPIKE:
			{	
				object* spike = create_spike(
					x + (int)clist_get( it, mapchunk_node ).x,
					y + (int)clist_get( it, mapchunk_node ).y,
					clist_get( it, mapchunk_node ).data
				);
				
				spike->origin_chunk_id = origin_chunk_id;
			}
			break;
			
			case NODE_SAVE:
			{
				object* save = create_save(
					x + (int)clist_get( it, mapchunk_node ).x,
					y + (int)clist_get( it, mapchunk_node ).y
				);
				
				save->origin_chunk_id = origin_chunk_id;
			}
			break;
			
			case NODE_FLY:
			{
				object* fly = create_fly(
					x + (int)clist_get( it, mapchunk_node ).x,
					y + (int)clist_get( it, mapchunk_node ).y,
					clist_get( it, mapchunk_node ).data
				);
				
				fly->origin_chunk_id = origin_chunk_id;
			}
			break;
			
			case NODE_BLOB:
			{
				object* blob = create_blob(
					x + (int)clist_get( it, mapchunk_node ).x,
					y + (int)clist_get( it, mapchunk_node ).y
				);
				
				blob->origin_chunk_id = origin_chunk_id;
			}
			break;
			
			case NODE_TURRET:
			{
				object* turret = create_turret(
					x + (int)clist_get( it, mapchunk_node ).x,
					y + (int)clist_get( it, mapchunk_node ).y,
					clist_get( it, mapchunk_node ).data
				);
				
				turret->origin_chunk_id = origin_chunk_id;
			}
			break;
			
			case NODE_ROCK:
			{
				object* rock = create_rock(
					x + (int)clist_get( it, mapchunk_node ).x,
					y + (int)clist_get( it, mapchunk_node ).y
				);
				
				rock->origin_chunk_id = origin_chunk_id;
			}
			break;
			
			case NODE_ITEM:
			{
				object* item = create_item(
					x + (int)clist_get( it, mapchunk_node ).x,
					y + (int)clist_get( it, mapchunk_node ).y,
					clist_get( it, mapchunk_node ).data
				);
				
				if (item)
				item->origin_chunk_id = origin_chunk_id;
			}
			break;
			
			case NODE_PORTAL:
			{
				object* portal = create_portal(
					x + (int)clist_get( it, mapchunk_node ).x,
					y + (int)clist_get( it, mapchunk_node ).y
				);
				
				portal->origin_chunk_id = origin_chunk_id;
				PORTAL(portal).id = clist_get( it, mapchunk_node ).data;
			}
			break;
			
		}
		
		it = clist_next(it);
	}	

	
	//areas 
	it = clist_start(mc->area_list);
	while (it != NULL) {
		
		switch ( clist_get( it, mapchunk_area ).type ) {
			
			case AREA_BLOCK:
			{
				object* block = create_block (
					x + (int)clist_get( it, mapchunk_area ).x,
					y + (int)clist_get( it, mapchunk_area ).y,
					clist_get( it, mapchunk_area ).w,
					clist_get( it, mapchunk_area ).h
				);
				
				block->origin_chunk_id = origin_chunk_id;
			}
			break;
			
			
			case AREA_PLATFORM:
			{
				object* plat = create_platform (
					x + (int)clist_get( it, mapchunk_area ).x,
					y + (int)clist_get( it, mapchunk_area ).y,
					clist_get( it, mapchunk_area ).w,
					clist_get( it, mapchunk_area ).h
				);
				
				plat->origin_chunk_id = origin_chunk_id;
			}
			break;
			
			case AREA_WATER:
			{
				object* water = create_water(
					x + (int)clist_get( it, mapchunk_area ).x,
					y + (int)clist_get( it, mapchunk_area ).y,
					clist_get( it, mapchunk_area ).w,
					clist_get( it, mapchunk_area ).h,
					clist_get( it, mapchunk_area ).data
				);
				
				water->origin_chunk_id = origin_chunk_id;
			}
			break;
			
		}
		
		it = clist_next(it);
	}
	
	//special cases cuz I'm lazy lmao 
	
	int16_t boss_chunk = 8; // + 0 * WORLD_SCREENS_W
	
	if (boss_chunk == origin_chunk_id) {
		object* mg = create_moongazer( x + SCREEN_W / 2, -20.0 );
		mg->origin_chunk_id = origin_chunk_id;
	}
}

void gamestate_set_camera(gamestate* gs, int16_t cam_x, int16_t cam_y) {
	gs->cam_x = (int16_t)iclamp(cam_x, 0, ( WORLD_SCREENS_W - 1 ) * SCREEN_W );
	gs->cam_y = (int16_t)iclamp(cam_y, 0, ( WORLD_SCREENS_H - 1 ) * SCREEN_H );
}

void gamestate_set_chunk(gamestate* gs, int8_t chunk_x, int8_t chunk_y) {
	
	int8_t target_x = (int8_t)iclamp(chunk_x, 0, WORLD_SCREENS_W - 1);
	int8_t target_y = (int8_t)iclamp(chunk_y, 0, WORLD_SCREENS_H - 1);
	
	//no need to spawn/despawn anything 
	if (target_x == gs->chunk_x && target_y == gs->chunk_y) { return; }
	
	int8_t prev_chunk_x = gs->chunk_x;
	int8_t prev_chunk_y = gs->chunk_y;
	
	int old_chunks_id[9];
	int new_target_chunks_id[9];
	int a = 0; //index to above 
	
	//fill old chunk ids 
	for (int dy = -1; dy <= 1; dy++)
	for (int dx = -1; dx <= 1; dx++) {
		
		int id = -99;
		
		int chunk_xx = ((int) prev_chunk_x ) + dx;
		int chunk_yy = ((int) prev_chunk_y ) + dy;
		
		if ( chunk_xx >= 0 && chunk_x < WORLD_SCREENS_W
		&&   chunk_yy >= 0 && chunk_y < WORLD_SCREENS_H ) {
			id = chunk_xx + WORLD_SCREENS_W * chunk_yy;
		}
		
		old_chunks_id[a] = id;
		a++;
		
	}
	
	//fill new chunk ids 
	a = 0;
	for (int dy = -1; dy <= 1; dy++)
	for (int dx = -1; dx <= 1; dx++) {
		
		int id = -99;
		
		int chunk_xx = ((int) target_x ) + dx;
		int chunk_yy = ((int) target_y ) + dy;
		
		if ( chunk_xx >= 0 && chunk_xx < WORLD_SCREENS_W
		&&   chunk_yy >= 0 && chunk_yy < WORLD_SCREENS_H ) {
			id = chunk_xx + WORLD_SCREENS_W * chunk_yy;
		}
		
		new_target_chunks_id[a] = id;
		a++;
		
	}
	
	//despawn objs from unloaded chunks 
	for ( int k = 0; k < 2; k++ ) {
		
		clist* list = ( k == 0 ) ? gs->main_object_list : gs->sub_object_list;
		
		clist_iterator it = clist_start( list );
		while (it != NULL) {
			
			object* obj = clist_get( it, object* );
			
			if ( obj->origin_chunk_id < 0 || obj->destroy ) {
				it = clist_next( it );
				continue;
			}
			
			int found = 0;
			for (int i = 0; i < 9; i++) {
				if ( obj->origin_chunk_id == new_target_chunks_id[i] ) {
					found = 1;
					break;
				}
			}
			
			if (!found) { obj->destroy = 1; }
				
			it = clist_next( it );
		}	
	}
	
	//spawn objs 
	for (int new = 0; new < 9; new++) {
		
		if ( new_target_chunks_id[new] < 0 ) { continue; }
		
		int found = 0; 
		for (int old = 0; old < 9; old++) {
			
			if ( new_target_chunks_id[new] == old_chunks_id[old] ) {
				found = 1;
				break;
			}
		}
		
		if (!found) {
			
			
			int chunk_xx = new_target_chunks_id[new] % WORLD_SCREENS_W;
			int chunk_yy = new_target_chunks_id[new] / WORLD_SCREENS_W;
			
			int16_t chunk_wx = (int16_t)(chunk_xx * SCREEN_W);
			int16_t chunk_wy = (int16_t)(chunk_yy * SCREEN_H);
			
			gamestate_spawn_chunk( gs, CHUNK_ARRAY[ new_target_chunks_id[new]  ] , chunk_wx, chunk_wy,
				(int16_t) new_target_chunks_id[new] );
				
			//printf("Spawning!! ");
		}
	}
	
	gs->chunk_x = target_x; 
	gs->chunk_y = target_y;
	
}

void gamestate_pause(gamestate* gs, uint8_t pause) {
	gs->queued_pause_state = pause;
}

void gamestate_nuke_objects(gamestate* gs) {
	
	for (int k = 0; k < 2; k++) {
		
		clist* list = ( k == 0 ) ? gs->main_object_list : gs->sub_object_list;
		
		clist_iterator it = clist_start( list );
		while (it != NULL) {
			
			object* obj = clist_get( it, object* );
			if ( obj->type != OBJECT_TYPE_TEC ) { obj->destroy = 1; }
			
			it = clist_next( it );
		}	
	}
}

void gamestate_set_message( gamestate* gs, const char* title, const char* message, int16_t delay ) {
	gs->queued_message = 1;
	strcpy( gs->message_title, title );
	strcpy( gs->message, message );
	gs->message_delay = delay;
}

void gamestate_music_volume(gamestate* gs, int v) {
	gs->app_signal.music_volume = iclamp(v, 0, MAX_VOLUME);
}

void gamestate_music(gamestate* gs, int action) {
	
	if ( action == MUSIC_PAUSE ) {
		gamestate_music_volume( gs, 0 );
		gs->music_vdelta = 0;
		return;
	}
	
	gs->app_signal.music_action = action;
}

void gamestate_play_sfx(gamestate* gs, int sfx) {
	
	if ( gs->app_signal.n_sfx >= SFX_PER_FRAME ) return;
	
	for (int k = 0; k < gs->app_signal.n_sfx; k++) {
		if ( gs->app_signal.queued_sfx[k] == sfx ) return;
	}
	
	gs->app_signal.queued_sfx[ gs->app_signal.n_sfx ] = sfx;
	gs->app_signal.n_sfx++;
}

int CHUNK_PALETTE[ WORLD_SCREENS_W * WORLD_SCREENS_H ] = {
	4,0,0,0,1,1,1,1,  4,
	0,0,0,0,1,1,1,1,  0,
	0,0,0,0,1,1,1,1,  4,
	2,2,2,0,0,0,1,1,  4,
	2,2,2,0,0,0,3,3,  4,
	2,2,2,0,0,3,3,3,  0,
	2,2,2,0,3,3,3,3,  0,
	0,0,0,0,3,3,3,3,  0
};