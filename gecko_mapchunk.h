/*
	File format:
	
	uint8_t bg_color;
	uint8_t shadow; 
	uint8_t heat;
	MAPCHUNK_TILE_W * MAPCHUNK_TILE_H number of uint8_t for the tile data 
	uint8_t nodes;
	<nodes> number of nodes defined as 
		uint8_t type;
		uint8_t data;
		uint8_t x,y;
	uint8_t areas;
	<areas> number of areas defined as 
		uint8_t type;
		uint8_t data; 
		uint8_t x,y,w,h;

*/

#ifndef _GECKO_MAPCHUNK_H
#define _GECKO_MAPCHUNK_H

#define MAPCHUNK_TILE_W (20)
#define MAPCHUNK_TILE_H (18)
#define MAPCHUNK_NULL_TILE (255)

#include <stdint.h>
#include "clist.h"
#include "bit1cd.h"

enum _NODE_TYPES {
	NODE_SAVE = 0,
	NODE_FLY, //data should be initial direction in multiples of pi/2 (from 0 to 3)
	NODE_BLOB,
	NODE_TURRET, //data should be shooting direction in multiples of pi/2 (from 0 to 3)
	NODE_PIPE,
	NODE_ITEM, //check gamestate's UNLOCKED values 
	NODE_SPIKE, //data should be direction in multiples of pi/2 (from 0 to 3)
	NODE_ROCK,
	NODE_PORTAL //data should be portal id 
};

enum _AREA_TYPES {
	AREA_BLOCK = 0,
	AREA_PLATFORM,
	AREA_WATER //data should be 0(default) or 1(inactive). Inactive water doesn't draw anything and ignores signals 
};

typedef struct {
	uint8_t type;
	uint8_t data; //for a lil trolling and custom initialization 
	uint8_t x,y; //in pixels relative to chunk top left 
	
} mapchunk_node;

typedef struct {
	uint8_t type;
	uint8_t data; //idk just in case? don't think will be needed 
	uint8_t x,y,w,h; //pos and dimensions relative to chunk, in pixels 
	
} mapchunk_area;

typedef struct {
	uint8_t bg_color; //0 light, 1 dark 
	uint8_t shadow; //if true, fill mapchunk with shadow canvas 
	uint8_t heat; //if true, apply heat effect if camera is centered here 
	
	//access with chunk_tile_x + MAPCHUNK_TILE_W * chunk_tile_y
	//value is to be interpreted as index into tileset (this index should be calculated as tile_x + 7 * tile_y)
	// 7 since tileset is 7x7 
	//a value of MAPCHUNK_NULL_TILE means no tile 
	uint8_t tiledata[ MAPCHUNK_TILE_W * MAPCHUNK_TILE_H ]; 
	
	uint8_t nodes; // # of nodes 
	uint8_t areas; // # of areas 
	
	clist* node_list;
	clist* area_list;
	
} mapchunk;

mapchunk* mapchunk_new(); //default empty chunk 
void mapchunk_dump_file(mapchunk* mc, const char* filename);
mapchunk* mapchunk_load_from_file(const char* filename);
mapchunk* mapchunk_load_from_memblock(const uint8_t* memblock);
void mapchunk_free(mapchunk* mc);

//x,y is the top left corner of the mapchunk 
//will draw background color and tiles 
void mapchunk_draw(bit1cd_canvas* cv, int16_t x, int16_t y, mapchunk* mc);

#endif