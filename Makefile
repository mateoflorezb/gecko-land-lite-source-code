INCLUDE = -I"C:\Users\mateo\Documents\SDL2-2.26.2\x86_64-w64-mingw32\include\SDL2" \
	-I"C:\Users\mateo\Documents\SDL2_mixer-2.6.2\x86_64-w64-mingw32\include\SDL2" 

LINK_LIB = -L"C:\Users\mateo\Documents\SDL2-2.26.2\x86_64-w64-mingw32\lib" \
	-L"C:\Users\mateo\Documents\SDL2_mixer-2.6.2\x86_64-w64-mingw32\lib" 

LINK_FLAGS = -lmingw32 -lSDL2main -lSDL2 -lSDL2_mixer -lopengl32

SOURCE = clist.c dhex_draw.c dhex_window_app.c stbi.c gecko_mapchunk.c gecko_misc.c bit1cd.c \
	gecko_sprite.c gecko_rescomp.c gecko_rescomp_font.c bit_to_dhex.c gecko_draw.c

GAME_SOURCE = gecko_global.c gecko_body2d.c gecko_gamestate.c gecko_object.c gecko_input.c pool.c \
	gecko_world.c gecko_enemy.c gecko_animation.c \
	obj/gecko.c obj/block.c obj/tec.c obj/water.c obj/waterdrop.c obj/temp.c obj/waterfall.c obj/pipe.c \
	obj/spike.c obj/save.c obj/fly.c obj/blob.c obj/turret.c obj/enemyball.c obj/geckoball.c obj/rock.c \
	obj/item.c obj/portal.c obj/moongazer.c obj/moonorb.c

CC = gcc

main : 
	$(CC) _gecko_main.c $(SOURCE) $(GAME_SOURCE) $(INCLUDE) $(LINK_LIB) $(LINK_FLAGS) -o gecko.exe -mwindows -O2
	
game_debug :
	$(CC) _gecko_main.c $(SOURCE) $(GAME_SOURCE) $(INCLUDE) $(LINK_LIB) $(LINK_FLAGS) -o gecko.exe -g
	
maptool:
	$(CC) _maptool.c dhex_font.c $(SOURCE) $(INCLUDE) $(LINK_LIB) $(LINK_FLAGS) -o maptool.exe
	
mapgen:
	$(CC) _mapgen.c gecko_world.c $(SOURCE) $(INCLUDE) $(LINK_LIB) $(LINK_FLAGS) -o mapgen.exe 
	
button:
	$(CC) _buttonconfig.c $(SOURCE) $(INCLUDE) $(LINK_LIB) $(LINK_FLAGS) -o buttonconfig.exe 