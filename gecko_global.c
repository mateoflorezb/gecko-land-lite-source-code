#include "gecko_global.h"
#include "gecko_magic.h"
#include "gecko_misc.h"
#include "gecko_object.h"
#include <math.h>

#include "obj/tec.h"

#define OBJECT_POOL_SIZE (4000)
#define ASPECT_RATIO (10.0 / 9.0)
//#define ASPECT_RATIO (1.0 / 1.0)

_global global;

void init_global() {
	
	global.object_pool = pool_create( sizeof(object), OBJECT_POOL_SIZE );
	global.float_array_pool = pool_create( 20 * 4 * sizeof(float), 120 ); //20 w tiles and 4 surfaces per tile 
	
	global.main_canvas   = bit1cd_create_canvas( SCREEN_W, SCREEN_H );
	global.heat_canvas   = bit1cd_create_canvas( SCREEN_W, SCREEN_H );
	global.shadow_canvas = bit1cd_create_canvas( SCREEN_W, SCREEN_H );
	
	global.app = dhex_create_app("Gecko Land Lite v1.0", SCREEN_W, SCREEN_H, ASPECT_RATIO, 1,
		DHEX_APP_SFX |
		DHEX_APP_ASPECT_RATIO_CORRECTION |
		DHEX_APP_HIDE_STDOUT 
	);
	
	int dw, dh;
	dhex_get_display_size(&dw, &dh);
	
	int times_w = (int)floor( ((float) dw ) / ((float) SCREEN_W ) );
	int times_h = (int)floor( ((float) dh ) / ((float) SCREEN_H ) );
	
	int min_factor = (times_w < times_h) ? times_w : times_h;
	min_factor = iclamp( min_factor - 1, 1, 20 );
	
	dhex_set_window_size( global.app, SCREEN_W * min_factor, SCREEN_H * min_factor );
	dhex_window_center( global.app );
	
	gamestate_initialize( &global.gs );

	create_tec();
}