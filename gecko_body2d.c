#include "gecko_body2d.h"
#include "gecko_misc.h"
#include "gecko_global.h"
#include "gecko_object.h"
#include <string.h>
#include <math.h>

void body2d_initialize(body2d* body, float x,float y, float w, float h, int8_t xoffset, int8_t yoffset) {
	body->x = x;
	body->y = y;
	body->w = w;
	body->h = h;
	body->xoffset = xoffset;
	body->yoffset = yoffset;
	body->xspeed = 0;
	body->yspeed = 0;
}

int body2d_1on1_check(body2d* b1, body2d* b2) {
	
	float x1 = b1->x - (float)b1->xoffset;
	float y1 = b1->y - (float)b1->yoffset;
	float w1 = b1->w;
	float h1 = b1->h;
	
	float x2 = b2->x - (float)b2->xoffset;
	float y2 = b2->y - (float)b2->yoffset;
	float w2 = b2->w;
	float h2 = b2->h;
	
	return AABB(x1,y1,w1,h1,  x2,y2,w2,h2);
	
}

int body2d_place_meeting( body2d* body, float x, float y, uint8_t type ) {
	
	body2d clone;
	memcpy( &clone, body, sizeof(body2d) );
	clone.x = x;
	clone.y = y;
	
	clist_iterator it = clist_start(global.gs.main_object_list);
	while (it != NULL) {
		
		object* obj = clist_get(it, object*);
		
		if (obj->destroy || (obj->type != type) ) {
			it = clist_next(it);
			continue;
		}
		
		body2d* obj_body = obj->get_component(obj, COMPONENT_BODY2D);
		
		if (obj_body) {
			if ( body2d_1on1_check( obj_body, &clone ) ) {
				return 1;
			}
		}
		
		it = clist_next(it);
	}
	
	return 0;
}


void body2d_solid_col(body2d* body) {
	
	//already inside block, abort
	if (  body2d_place_meeting( body, body->x, body->y, OBJECT_TYPE_BLOCK ) ) { return; }
	
	//no collision next frame, exit 
	if ( !body2d_place_meeting( body, body->x + body->xspeed, body->y + body->yspeed, OBJECT_TYPE_BLOCK ) ) { return; }
	
	int collided = 0;
	
	if ( body->xspeed != 0 )
	if ( body2d_place_meeting( body, body->x + body->xspeed, body->y, OBJECT_TYPE_BLOCK ) ) {
		
		body->x = ( body->xspeed > 0 ) ? floorf( body->x + body->xspeed ) : ceilf( body->x + body->xspeed );
		float dx = fsign(body->xspeed);
		
		int panic = 100;
		
		while ( body2d_place_meeting( body, body->x, body->y, OBJECT_TYPE_BLOCK ) && panic > 0 ) {
			panic--;
			body->x -= dx;
		}
		
		body->xspeed = 0;	
		collided = 1;
	}
	
	if ( body->yspeed != 0 )
	if ( body2d_place_meeting( body, body->x, body->y + body->yspeed, OBJECT_TYPE_BLOCK ) ) {
		
		body->y = ( body->yspeed > 0 ) ? floorf( body->y + body->yspeed ) : ceilf( body->y + body->yspeed );
		float dy = fsign(body->yspeed);
		
		int panic = 100;
		
		while ( body2d_place_meeting( body, body->x, body->y, OBJECT_TYPE_BLOCK ) && panic > 0 ) {
			panic--;
			body->y -= dy;
		}
		
		body->yspeed = 0;
		collided = 1;
	}
	
	//if no collision was detected by this point then it was a diagonal only collision
	if (!collided) { body->xspeed = 0; }
	
}


void body2d_platform_col(body2d* body) {
	
	if (body->yspeed <= 0) return;

	clist_iterator it = clist_start( global.gs.main_object_list );
	while (it != NULL) {
		
		object* obj = clist_get(it, object*);
		if (obj->destroy) { it = clist_next(it); continue; }
		
		if (obj->type == OBJECT_TYPE_PLATFORM) {
			
			body2d* platform_body = obj->get_component( obj, COMPONENT_BODY2D );
			
			if (!platform_body) 
			
			//already in platform, can't collide 
			if ( body2d_1on1_check(body, platform_body) ) { it = clist_next(it); continue; }
			
			float original_y = body->y;
			body->y = body->y + body->yspeed;
			
			//not touching next frame, continue 
			if ( !body2d_1on1_check(body, platform_body) ) {
				body->y = original_y;
				it = clist_next(it); continue;
			}
			
			body->y = original_y;
			
			float body_bottom = body->y - (float)body->yoffset + body->h;
			float platform_top = platform_body->y - (float)platform_body->yoffset;
			
			if ( body_bottom > platform_top ) { it = clist_next(it); continue; }
			
			//found the good shit 
			body->y = floorf(body->y + body->yspeed);
			int panic = 100;
			while ( body2d_1on1_check(body, platform_body) && panic > 0 ) {
				panic--;
				body->y -= 1.0;
			}
			
			body->yspeed = 0;
			return;
			
		}
		
		
		it = clist_next(it);
	}
}


void* body2d_object_at( body2d* body, float x, float y, uint8_t type ) {
	
	body2d clone;
	memcpy( &clone, body, sizeof(body2d) );
	clone.x = x;
	clone.y = y;
	
	clist_iterator it = clist_start(global.gs.main_object_list);
	while (it != NULL) {
		
		object* obj = clist_get(it, object*);
		
		if (obj->destroy || (obj->type != type) ) {
			it = clist_next(it);
			continue;
		}
		
		body2d* obj_body = obj->get_component(obj, COMPONENT_BODY2D);
		
		if (obj_body) {
			if ( body2d_1on1_check( obj_body, &clone ) ) {
				return obj;
			}
		}
		
		it = clist_next(it);
	}
	
	return NULL;
}


void body2d_unit_vector_at( body2d* body, float target_x, float target_y, float* ux, float* uy ) {
	
	float relative_x = target_x - body->x;
	float relative_y = target_y - body->y;
	
	float dist = sqrtf( relative_x*relative_x + relative_y*relative_y );
	
	*ux = 0;
	*uy = 0;
	
	if (dist > 0) {
		*ux = relative_x / dist;
		*uy = relative_y / dist;
	}
}


float body2d_distance_from( body2d* body, float target_x, float target_y ) {
	float relative_x = target_x - body->x;
	float relative_y = target_y - body->y;
	return sqrtf( relative_x*relative_x + relative_y*relative_y );
}