#include "gecko_misc.h"
#include <stdio.h>
#include <stdlib.h>

int64_t file_size(const char* file) {
	
    FILE * f = fopen(file, "r");
    fseek(f, 0, SEEK_END);
    int64_t len = ftell(f);
    fclose(f);
    return len;
}

uint8_t* load_file_memblock(const char* file) {
	
	int64_t bytes = file_size(file);
	FILE* f = fopen(file, "rb");
	if (!f) return NULL;
	uint8_t* memblock = malloc(bytes);
	fread(memblock, 1, bytes, f);
	fclose(f);
	return memblock;
}

int AABB( float x1, float y1, float w1, float h1,
		  float x2, float y2, float w2, float h2 ) {

	if (
		x1 < x2 + w2 &&
		x1 + w1 > x2 &&
		y1 < y2 + h2 &&
		y1 + h1 > y2
	)
	return 1;
	return 0;
}

float fsign(float v) {
	
	if (v > 0) return  1.0;
	if (v < 0) return -1.0;
	return 0.0;
	
}

int isign(int v) {
	if (v > 0) return  1;
	if (v < 0) return -1;
	return 0;
}

float uniform() {
	return (float)rand() / (float)RAND_MAX;
}

int irandom(int range) {
	
	return rand() % ( abs(range) + 1 );
	
}

int rand_sign() {
	return (irandom(1) * 2) - 1;
}