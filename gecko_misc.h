#ifndef _GECKO_MISC_H
#define _GECKO_MISC_H 

#include <stdint.h>

int64_t file_size(const char* file);
uint8_t* load_file_memblock(const char* file);

static inline int iclamp(int v, int min, int max) {
	if ( v < min ) return min;
	if ( v > max ) return max;
	return v;
}

static inline float fclamp(float v, float min, float max) {
	if ( v < min ) return min;
	if ( v > max ) return max;
	return v;
}

int AABB( float x1, float y1, float w1, float h1,
		  float x2, float y2, float w2, float h2 );
		  
int   isign(int   v);
float fsign(float v);

//gets a random number from 0 to 1 
float uniform();

//gets a random int number from 0 to range 
int irandom(int range);

//returns -1 or 1 randomly 
int rand_sign();

static inline uint16_t rng_next(uint16_t current) {
	
	current += 531;
	current *= 97;
	current = current ^ 0b0100101101001101;
	current = (current << 8) | (current >> 8);
	return current;
}

#endif