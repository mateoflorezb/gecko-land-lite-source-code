#ifndef _OBJ_FLY_H
#define _OBJ_FLY_H

#include "../gecko_object.h"

object* create_fly(float x, float y, uint8_t node_data);

#endif 