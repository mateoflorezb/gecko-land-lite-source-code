#include "enemyball.h"
#include "temp.h"
#include "../gecko_global.h"
#include "../gecko_draw.h"
#include "../gecko_animation.h"
#include "../gecko_misc.h"
#include "../gecko_magic.h"
#include <math.h>

#define EBALL(self) (self->class.enemyball)

static void* _eball_component( object* self, int component ) {
	
	switch(component) {
		
		case COMPONENT_BODY2D:    return &EBALL(self).body;
		case COMPONENT_ENEMYDATA: return &EBALL(self).enemy;
	}
	
	return NULL;
}

static void _eball_step( gamestate* gs, object* self, int step ) {
	
	if (step != STEP_0) return;
	
	EBALL(self).TTL--;
	if ( EBALL(self).TTL <= 0 ) { self->destroy = 1; return; }
	
	body2d* body = &EBALL(self).body;
	
	if ( body2d_place_meeting( body, body->x, body->y, OBJECT_TYPE_BLOCK ) ) {
		self->destroy = 1;
		return;
	}
	
	body->x += body->xspeed;
	body->y += body->yspeed;
	
	//if ( (gs->tickcount % 2) == 0 ) {
	
		int holds = 1 + irandom(2);
		
		float xspeed, yspeed;
		float theta = M_PI * (float)irandom(360) / 180.0;
		float spd = 0.5 * uniform() + 0.5;
		xspeed = spd*cosf(theta);
		yspeed = spd*sinf(theta);
		
		create_temp_ex(
			body->x, //+ irandom(1)*rand_sign(), 
			body->y, //+ irandom(1)*rand_sign(), 
			xspeed,yspeed,
			ANIMATION_PARTICLE, PARTICLE_NFRAMES, holds, ((PARTICLE_NFRAMES) * (holds+1)), LAYER_SPR_3,  5,
			0.85,
			BIT1CD_COLORMODE_INVERT
			//irandom(1) ? BIT1CD_COLORMODE_NORMAL : BIT1CD_COLORMODE_INVERT_SOURCE
		);
	//}
	
}

object* create_eball(float x, float y, float xspeed, float yspeed, int16_t TTL) {
	
	object* self = object_new( OBJECT_LIST_MAIN );
	self->step = _eball_step;
	self->get_component = _eball_component;
	
	body2d_initialize( &EBALL(self).body, x, y, 6, 6, 3, 3);
	enemy_initialize ( &EBALL(self).enemy, 1, 1, 1);
	EBALL(self).body.xspeed = xspeed;
	EBALL(self).body.yspeed = yspeed;
	EBALL(self).TTL = TTL;
	
	return self;
}