#include "waterfall.h"
#include "water.h"
#include "temp.h"
#include "../gecko_object.h"
#include "../gecko_misc.h"
#include "../gecko_sprite.h"
#include "../gecko_global.h"
#include "../gecko_draw.h"
#include "../gecko_animation.h"
#include <math.h>

#define WATERFALL(self) (self->class.waterfall)

static void _waterfall_step( gamestate* gs, object* self, int step ) {
	
	if ( step != STEP_0 ) return;
	
	body2d* body = &WATERFALL(self).body;
	
	if (WATERFALL(self).ignore_blocks > 0) { WATERFALL(self).ignore_blocks--; }
	
	WATERFALL(self).TTL--;
	if (WATERFALL(self).TTL <= 0) { self->destroy = 1; }
	
	//WATERFALL(self).body.yspeed = fclamp( WATERFALL(self).body.yspeed + 0.07, 0, 1 );
	WATERFALL(self).body.y += 1;//WATERFALL(self).body.yspeed;
	
	object* water = body2d_object_at( body, body->x, body->y, OBJECT_TYPE_WATER ); 

	if (water) {
		
		for (int xoffset = -1; xoffset <= 1; xoffset++)
		for (int k = 0; k < 3; k++) {
			
			int holds = 1 + irandom(6);
			
			create_temp_ex(
				body->x + 8*xoffset + rand_sign() * irandom(3),
				body->y + 10 + rand_sign() * irandom(2),
				(float)rand_sign() * 0.25 * uniform(),
				-0.5 - 0.25 * uniform(), 
				ANIMATION_PARTICLE, PARTICLE_NFRAMES,
				holds,
				PARTICLE_NFRAMES * (1+holds), LAYER_SPR_2, 5,
				0.97, BIT1CD_COLORMODE_INVERT_SOURCE
			);
			
		}
		
		water_signal( water, body->x + rand_sign() * irandom(4), ( 1.5 + uniform() ), 0 );	

		self->destroy = 1;
		return;
	}
	
	if ( WATERFALL(self).ignore_blocks <= 0 )
	if ( body2d_place_meeting( body, body->x, body->y, OBJECT_TYPE_BLOCK ) ) { self->destroy = 1; }
}

static void _waterfall_draw( gamestate* gs, object* self, int layer ) {
	
	if (layer != LAYER_SPR_0) { return; }
	
	draw_sprite_relative( global.main_canvas,
		(int16_t)floorf( WATERFALL(self).body.x ) - gs->cam_x,
		(int16_t)floorf( WATERFALL(self).body.y ) - gs->cam_y,
		0,0,0, SPR_WATERFALL, 8
	);
	
}

void create_waterfall(float x, float y) {
	
	object* self = object_new( OBJECT_LIST_SUB );
	self->step = _waterfall_step;
	self->draw = _waterfall_draw;
	body2d_initialize( &WATERFALL(self).body, x,y, 16, 3, 8, 0);
	WATERFALL(self).TTL = 60 * 7;
	WATERFALL(self).ignore_blocks = 12;
}