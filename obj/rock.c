#include "rock.h"
#include "block.h"
#include "../gecko_global.h"
#include "../gecko_enemy.h"
#include "../gecko_sprite.h"
#include <math.h>

#define ROCK(self) (self->class.rock)

static void _rock_release( object* self ) {
	ROCK(self).block->destroy = 1;
}

static void _rock_step( gamestate* gs, object* self, int step ) {
	
	if (step != STEP_0) return;
	
	body2d* body = &ROCK(self).body;
	
	if ( body2d_place_meeting( body, body->x, body->y, OBJECT_TYPE_GECKOBALL ) ) {
		enemy_generic_death( gs, self, body->x + 4, body->y + 4 );
	}
	
}

static void _rock_draw( gamestate* gs, object* self, int layer ) {
	
	if (layer != LAYER_SPR_0) return;
	
	body2d* body = &ROCK(self).body;
	
	bit1cd_draw_sprite( global.main_canvas,
		(int16_t) floorf( body->x ) - gs->cam_x,
		(int16_t) floorf( body->y ) - gs->cam_y,
		0,0,0, SPR_ROCK
	);
	
}

object* create_rock(float x, float y) {
	object* self = object_new( OBJECT_LIST_MAIN );
	self->step = _rock_step;
	self->draw = _rock_draw;
	self->release = _rock_release;
	
	body2d_initialize( &ROCK(self).body, x, y, 8, 8, 0, 0 );
	ROCK(self).block = create_block(x,y,8,8);
	
	return self;
}