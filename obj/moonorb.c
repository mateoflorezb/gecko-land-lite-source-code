#include "moonorb.h"
#include "../gecko_draw.h"
#include "../gecko_global.h"
#include "../gecko_sprite.h"
#include "../gecko_magic.h"
#include "../gecko_misc.h"

#include <math.h>

static void* _moonorb_get_component( object* self, int component ) {
	switch(component) {
		
		case COMPONENT_BODY2D:    return &ORB(self).body;
		case COMPONENT_ENEMYDATA: return &ORB(self).enemy;
	}
	
	return NULL;
}

static void _moonorb_step( gamestate* gs, object* self, int step ) {
	
	if (step != STEP_0) return;
	body2d* body = &ORB(self).body;
	
	ORB(self).t++;
	
}

static void _moonorb_draw( gamestate* gs, object* self, int layer ) {
	
	if ( layer != LAYER_SPR_1    ) return;
	//if ( !ORB(self).enemy.active ) return;
	
	body2d* body = &ORB(self).body;
	
	bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_INVERT );
	
	int16_t xx = (int16_t)roundf(body->x) - gs->cam_x;
	int16_t yy = (int16_t)roundf(body->y) - gs->cam_y;
	
	for (int py = 0; py < 16; py++) {
		
		float theta = M_PI * (float)(( 4 * ORB(self).t + 10 * py ) % 360) / 180.0;
		int16_t xoffset = roundf( 2.0 * cosf( theta ) );
		
		bit1cd_draw_sprite_part( global.main_canvas, xx + xoffset, yy, 8, 8,
			0, py, 16, 1, SPR_CIRCLE16 );	
	}
	
	/*
	for (int py = 0; py < 16; py++) {
		
		float theta = M_PI * (float)(( 4 * ORB(self).t + 10 * py ) % 360) / 180.0;
		int16_t xoffset = roundf( 2.0 * cosf( theta ) );
		
		bit1cd_draw_sprite_part( global.main_canvas, xx + xoffset, yy, 8, 8,
			0, py, 16, 1, SPR_CIRCLE16 );	
	}
	*/
	
	bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_NORMAL );
	
}


object* create_moonorb(float x, float y) {
	object* self = object_new( OBJECT_LIST_MAIN );
	
	self->step = _moonorb_step;
	self->draw = _moonorb_draw;
	self->get_component = _moonorb_get_component;
	
	body2d_initialize(&ORB(self).body, x, y, 12, 12, 6, 6);
	enemy_initialize (&ORB(self).enemy, 1, 1, 0);
	ORB(self).t = 0;
	
	return self;
}