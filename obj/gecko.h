#ifndef _OBJ_GECKO_H
#define _OBJ_GECKO_H

#include "../gecko_object.h"

#define GECKO(self) ( self->class.gecko )

object* create_gecko(float x, float y);
object* find_gecko();
void draw_gecko_body( gamestate* gs, object* gecko );

#endif