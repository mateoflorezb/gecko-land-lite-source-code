#ifndef _OBJ_PORTAL_H
#define _OBJ_PORTAL_H

#include "../gecko_object.h"
#define PORTAL(self) (self->class.portal)

object* create_portal(float x, float y);

#endif