#include "geckoball.h"
#include "temp.h"
#include "../gecko_global.h"
#include "../gecko_sprite.h"
#include "../gecko_misc.h"
#include "../gecko_draw.h"
#include "../gecko_magic.h"
#include "../gecko_animation.h"
#include <math.h>
#include <string.h>

#define GBALL(self) (self->class.geckoball)

#define DAMAGE (1)

static void* _gball_component( object* self, int component ) {
	
	if ( component == COMPONENT_BODY2D ) 	
		return &GBALL(self).body;
	
	return NULL;
}

static void _gball_step( gamestate* gs, object* self, int step ) {
	
	body2d* body = &GBALL(self).body;
	
	switch(step) {
	
		case STEP_0:
		{
			//check if outside view. If so, self destruct 
			body2d camera_area;
			camera_area.x = gs->cam_x + 1;
			camera_area.y = gs->cam_y + 1;
			camera_area.w = SCREEN_W  - 2;
			camera_area.h = SCREEN_H  - 2;
			camera_area.xoffset = 0;
			camera_area.yoffset = 0;
			
			if ( !body2d_1on1_check( body, &camera_area ) ) {
				//self->destroy = 1;
				//return;
				GBALL(self).OOB_TTL--;
			}
	
			GBALL(self).TTL--;
			if ( GBALL(self).TTL <= 0 || GBALL(self).OOB_TTL <= 0 ) { self->destroy = 1; return; }
			
			int holds = irandom(1);
		
			float xspeed, yspeed;
			float theta = M_PI * (float)irandom(360) / 180.0;
			float spd = 0.5 * uniform() + 0.5;
			xspeed = spd*cosf(theta);
			yspeed = spd*sinf(theta);
			
			create_temp_ex(
				body->x, //+ irandom(1)*rand_sign(), 
				body->y, //+ irandom(1)*rand_sign(), 
				xspeed,yspeed,
				ANIMATION_PARTICLE, PARTICLE_NFRAMES, holds, ((PARTICLE_NFRAMES) * (holds+1)), LAYER_SPR_2,  5,
				0.85,
				BIT1CD_COLORMODE_NORMAL
				//BIT1CD_COLORMODE_INVERT
				//irandom(1) ? BIT1CD_COLORMODE_NORMAL : BIT1CD_COLORMODE_INVERT_SOURCE
			);
			
			clist_iterator it = clist_start( global.gs.main_object_list );
			while (it) {
				
				object* obj = clist_get(it, object*);
				if (obj->destroy) { it = clist_next(it); continue; } 
				
				enemydata* enemy = obj->get_component( obj, COMPONENT_ENEMYDATA );
				if (!enemy )		      { it = clist_next(it); continue; }
				if ( enemy->iframes > 0 ) { it = clist_next(it); continue; }
				
				body2d* enemy_body = obj->get_component( obj, COMPONENT_BODY2D );
				if ( !enemy_body ) { it = clist_next(it); continue; }
				
				//deth 
				if ( body2d_1on1_check( body, enemy_body ) ) {
					
					enemy->hp -= DAMAGE;
					self->destroy = 1;
					return;
				}
				
				it = clist_next(it);
			}
		}
		break;
		
		case STEP_1:
		{
			body2d block_body;
			memcpy( &block_body, body, sizeof(body2d) );
			block_body.h = 1;
			block_body.yoffset = 0;
			
			if ( body2d_place_meeting( &block_body, block_body.x, block_body.y, OBJECT_TYPE_BLOCK ) ) {
				self->destroy = 1;
			} 
			
			body->x += body->xspeed;
			body->y += body->yspeed;
		}
		break;
	}
}

static void _gball_draw( gamestate* gs, object* self, int layer ) {
	
	if (layer == LAYER_SPR_1) {
		body2d* body = &GBALL(self).body;
		int16_t xx = (int16_t)floorf ( body->x ) - gs->cam_x;
		int16_t yy = (int16_t)floorf ( body->y ) - gs->cam_y;
		draw_small_light( global.shadow_canvas, xx, yy );
	}
	
	if (layer == LAYER_SPR_3) {
	
		draw_sprite_relative( global.main_canvas,
			(int16_t)floorf( GBALL(self).body.x ) - gs->cam_x,
			(int16_t)floorf( GBALL(self).body.y ) - gs->cam_y,
			0,0,0, SPR_BALL0, 5
		);	
	}
}

object* create_geckoball(float x, float y, float xspeed) {
	
	object* self = object_new( OBJECT_LIST_MAIN );
	self->type = OBJECT_TYPE_GECKOBALL;
	self->step = _gball_step;
	self->draw = _gball_draw;
	self->get_component = _gball_component;
	
	body2d_initialize( &GBALL(self).body, x, y, 4, 6, 2, 3 );
	GBALL(self).body.xspeed = xspeed;
	GBALL(self).TTL = 60;
	GBALL(self).OOB_TTL = 6;
	
	return self;
}