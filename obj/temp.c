#include "temp.h"
#include "../gecko_global.h"
#include "../gecko_draw.h"
#include <math.h>

#define TEMP(self) (self->class.temp)	

static void _temp_step( gamestate* gs, object* self, int step ) {
	
	if (step != STEP_0) { return; }
	
	TEMP(self).TTL--;
	if (TEMP(self).TTL <= 0) { self->destroy = 1; return; }
	
	TEMP(self).x += TEMP(self).xspeed;
	TEMP(self).y += TEMP(self).yspeed;
	
	TEMP(self).xspeed *= TEMP(self).friction;
	TEMP(self).yspeed *= TEMP(self).friction;
	
	if ( TEMP(self).holds_left > 0 ) { TEMP(self).holds_left--; }
	else {
		TEMP(self).holds_left = TEMP(self).max_holds;
		TEMP(self).current_frame++;
		TEMP(self).current_frame = TEMP(self).current_frame % TEMP(self).nframes;
	}
}

static void _temp_draw( gamestate* gs, object* self, int layer ) {
	
	if ( layer != TEMP(self).layer ) { return; }
	
	bit1cd_set_colormode( global.main_canvas, TEMP(self).colormode );
	
	draw_sprite_relative( global.main_canvas,
		floorf(TEMP(self).x) - gs->cam_x,
		floorf(TEMP(self).y) - gs->cam_y,
		0,0,
		0,
		TEMP(self).sprite_array[ TEMP(self).current_frame ],
		TEMP(self).relative_offset
	);
	
	bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_NORMAL );
	
}

object* create_temp(float x, float y, float xspeed, float yspeed, 
	bit1cd_sprite** sprite_array, int nframes, int holds, int16_t TTL, int layer, int relative_offset) {
		
	object* self = object_new( OBJECT_LIST_SUB );
	self->step = _temp_step;
	self->draw = _temp_draw;
	
	TEMP(self).x = x;
	TEMP(self).y = y;
	TEMP(self).xspeed = xspeed;
	TEMP(self).yspeed = yspeed;
	TEMP(self).TTL = TTL;
	TEMP(self).friction = 1.0;
	TEMP(self).colormode = BIT1CD_COLORMODE_NORMAL;
	TEMP(self).nframes = nframes;
	TEMP(self).sprite_array = sprite_array;
	TEMP(self).current_frame = 0;
	TEMP(self).max_holds = holds;
	TEMP(self).holds_left = holds;
	TEMP(self).layer = layer;
	TEMP(self).relative_offset = relative_offset;
	
	return self;
	
}
	
object* create_temp_ex(float x, float y, float xspeed, float yspeed, 
	bit1cd_sprite** sprite_array, int nframes, int holds, int16_t TTL, int layer,  int relative_offset,
	float friction, uint8_t colormode) {
		
	object* self = create_temp(x, y, xspeed, yspeed, sprite_array, nframes, holds, TTL, layer, relative_offset);
	
	TEMP(self).friction = friction;
	TEMP(self).colormode = colormode;
	
	return self;
}




static void _temp_static_step( gamestate* gs, object* self, int step ) {
	
	if (step != STEP_0) { return; }
	
	TEMP(self).TTL--;
	if (TEMP(self).TTL <= 0) { self->destroy = 1; return; }
	
	TEMP(self).x += TEMP(self).xspeed;
	TEMP(self).y += TEMP(self).yspeed;
	
	TEMP(self).xspeed *= TEMP(self).friction;
	TEMP(self).yspeed *= TEMP(self).friction;
}

static void _temp_static_draw( gamestate* gs, object* self, int layer ) {
	
	if ( layer != TEMP(self).layer ) { return; }
	
	bit1cd_set_colormode( global.main_canvas, TEMP(self).colormode );
	
	draw_sprite_relative( global.main_canvas,
		floorf(TEMP(self).x) - gs->cam_x,
		floorf(TEMP(self).y) - gs->cam_y,
		0,0,
		0,
		TEMP(self).sprite,
		TEMP(self).relative_offset
	);
	
	bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_NORMAL );
	
}

object* create_temp_static(float x, float y, float xspeed, float yspeed, 
	bit1cd_sprite* sprite, int16_t TTL, int layer,  int relative_offset,
	float friction, uint8_t colormode) {

	object* self = object_new( OBJECT_LIST_SUB );
	self->step = _temp_static_step;
	self->draw = _temp_static_draw;
	
	TEMP(self).x = x;
	TEMP(self).y = y;
	TEMP(self).xspeed = xspeed;
	TEMP(self).yspeed = yspeed;
	TEMP(self).TTL = TTL;
	TEMP(self).friction = friction;
	TEMP(self).colormode = colormode;
	TEMP(self).sprite = sprite;
	TEMP(self).layer = layer;
	TEMP(self).relative_offset = relative_offset;
	
	return self;
}