#ifndef _OBJ_WATER_H
#define _OBJ_WATER_H

#include "../gecko_object.h"

#define MAX_SURFACE_YSPEED (4)

object* create_water(float x, float y, uint8_t w, float h, uint8_t node_data);
void water_signal(object* water_obj, float x, float yspeed, int sfx);

//checks if the body2d is on the surface of the water with enough yspeed. If so, triggers a signal.
//returns true if a signal was triggered 
int generic_water_interaction(body2d* body);

#endif