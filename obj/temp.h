#ifndef _OBJ_TEMP_H
#define _OBJ_TEMP_H

#include "../gecko_object.h"

object* create_temp(float x, float y, float xspeed, float yspeed, 
	bit1cd_sprite** sprite_array, int nframes, int holds, int16_t TTL, int layer, int relative_offset);
	
object* create_temp_ex(float x, float y, float xspeed, float yspeed, 
	bit1cd_sprite** sprite_array, int nframes, int holds, int16_t TTL, int layer,  int relative_offset,
	float friction, uint8_t colormode);
	
object* create_temp_static(float x, float y, float xspeed, float yspeed, 
	bit1cd_sprite* sprite, int16_t TTL, int layer,  int relative_offset,
	float friction, uint8_t colormode);

#endif 