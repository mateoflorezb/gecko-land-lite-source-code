#ifndef _OBJ_ROCK_H
#define _OBJ_ROCK_H

#include "../gecko_object.h"

object* create_rock(float x, float y);

#endif 