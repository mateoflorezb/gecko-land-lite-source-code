#include "water.h"
#include "../gecko_global.h"
#include "../gecko_misc.h"
#include "../gecko_sprite.h"
#include "waterdrop.h"
#include "temp.h"
#include <string.h>
#include <math.h>

#define WATER(self) ( self->class.water )

static void _water_release( object* self ) {
	
	pool_release( global.float_array_pool, WATER(self).surface_level      ); 
	pool_release( global.float_array_pool, WATER(self).surface_speed      ); 
	pool_release( global.float_array_pool, WATER(self).surface_wave_dir   ); 
	pool_release( global.float_array_pool, WATER(self).surface_wave_speed ); 
	pool_release( global.float_array_pool, WATER(self).surface_wave_dir2  ); 	
}

static void* _water_component( object* self, int component_type ) {
	
	if ( component_type == COMPONENT_BODY2D ) {
		return (void*)( &WATER(self).body );
	}
	
	return NULL;
}

static void _water_step( gamestate* gs, object* self, int step ) {
	
	if (step != STEP_2) { return; }
	
	if ( WATER(self).sound_delay > 0 ) { WATER(self).sound_delay--; }
	
	float slower = 20.0;

	for (int px = 0; px < WATER(self).npoints; px++) {
		
		float prevSign;
		prevSign = fsign( WATER(self).surface_level[px] );
		
		WATER(self).surface_level[px] += WATER(self).surface_speed[px];
		
		if ( prevSign != fsign( WATER(self).surface_level[px] ) ) {
			//if ( fabsf( WATER(self).surface_speed[px] ) < 0.5 )
			//	WATER(self).surface_speed[px] *= 0.25; 
			//else
				WATER(self).surface_speed[px] *= 0.8; 
		}
		
	}

	for (int px = 0; px < WATER(self).npoints; px++) {

		WATER(self).surface_speed[px] -= WATER(self).surface_level[px] / slower;
		WATER(self).surface_speed[px] = fclamp( WATER(self).surface_speed[px], -MAX_SURFACE_YSPEED, MAX_SURFACE_YSPEED );
		
		WATER(self).surface_level[px] = fclamp( WATER(self).surface_level[px], -8, 8 );
	}
		
	for (int px = 0; px < WATER(self).npoints; px++) {
		WATER(self).surface_wave_dir2 [px] = 0;
	}
	 
	float diminish_factor = 0.95;
	   
	for (int px = 1; px < WATER(self).npoints - 1; px++) {
		
		if ( WATER(self).surface_wave_dir [px] != 0 ) {
			
			int _dir = (int)WATER(self).surface_wave_dir[px];
			
			if ( WATER(self).surface_wave_dir2 [px + _dir] == 0 ) {
				WATER(self).surface_wave_dir2  [px + _dir] = _dir;
				WATER(self).surface_wave_speed [px + _dir] = WATER(self).surface_wave_speed [px] * diminish_factor;
			}
			else {
				int maxdir;
				if ( WATER(self).surface_wave_speed[px + _dir] > WATER(self).surface_wave_speed[px] )
				{ maxdir = (int)WATER(self).surface_wave_dir2 [px + _dir]; }
				else { maxdir = _dir; }
				float spd = ( WATER(self).surface_wave_speed[px + _dir] + WATER(self).surface_wave_speed[px] ) / 2;
				WATER(self).surface_wave_speed [px + _dir] = spd * diminish_factor;
				WATER(self).surface_wave_dir2  [px + _dir] = maxdir;
			}
			
			WATER(self).surface_wave_speed [px] = 0;
		}
	}

	for (int px = 0; px < WATER(self).npoints; px++) {
		WATER(self).surface_wave_dir[px] = WATER(self).surface_wave_dir2[px];
	}

	for (int px = 0; px < WATER(self).npoints; px++) {
		WATER(self).surface_speed[px] += WATER(self).surface_wave_speed[px];
	}

	WATER(self).surface_wave_speed[0] = 0;
	WATER(self).surface_wave_speed[ WATER(self).npoints - 1 ] = 0;
	/*
	//water particles 
	for (int k = 0; k < 2; k++)
	{
		int px = irandom( WATER(self).npoints - 1 );
		int xx = (int16_t)floorf( WATER(self).body.x ) + 2*px + irandom(1);
		int yy = WATER(self).body.y + fclamp(0.6 * WATER(self).surface_level[px] , -6,6) + 2;
		
		create_temp_static( xx, yy, 0, 0.5, 
			SPR_WATERDROP, 7 + irandom(7), LAYER_SPR_1,  5,
			1.0, BIT1CD_COLORMODE_NORMAL
		);
	}
	*/
	
}

static void _water_draw ( gamestate* gs, object* self, int layer ) {
	if (layer != LAYER_SPR_1) { return; }
	
	int16_t base_px = (int16_t)floorf( WATER(self).body.x ) - gs->cam_x;
	float   base_y = WATER(self).body.y - gs->cam_y;
	
	for (int px = 0; px < WATER(self).npoints; px++) {
		
		float surface_level; 
		const float ptg = 0.5;
		if (px > 0 && px < WATER(self).npoints - 1) {
			
			surface_level = ptg * WATER(self).surface_level[px    ] +
				(1 - ptg) * 0.5 * WATER(self).surface_level[px - 1] +
				(1 - ptg) * 0.5 * WATER(self).surface_level[px + 1];
		}
		else { surface_level = WATER(self).surface_level[px]; }
		
		bit1cd_draw_sprite( global.main_canvas,
			base_px + 2 * px,
			(int16_t)roundf(base_y  + fclamp(0.75 * surface_level, -6,6) ),
			0, 0,
			0, SPR_WATERTOP
		);
	}
}

object* create_water(float x, float y, uint8_t w, float h, uint8_t node_data) {
	
	object* self = object_new( OBJECT_LIST_MAIN );
	self->type = OBJECT_TYPE_WATER;
	
	//node data = bool inactive 
	if (!node_data) {
		self->step = _water_step;
		self->draw = _water_draw;
	}
	
	self->get_component = _water_component;
	self->release = _water_release;
	
	float fw = (float)w;
	
	body2d_initialize( &WATER(self).body, x, y, fw, h, 0, 0 );
	
    WATER(self).npoints = ((int)w) / 2;
    WATER(self).inactive = node_data;
    WATER(self).sound_delay = 0;
    
	WATER(self).surface_level      = pool_get( global.float_array_pool );
	WATER(self).surface_speed      = pool_get( global.float_array_pool );
	WATER(self).surface_wave_dir   = pool_get( global.float_array_pool );
	WATER(self).surface_wave_speed = pool_get( global.float_array_pool );
	WATER(self).surface_wave_dir2  = pool_get( global.float_array_pool ); 
	
    for (int px = 0; px < WATER(self).npoints; px++) {
        WATER(self).surface_level[px] = 0.0;
        WATER(self).surface_speed[px] = 0.0;
        WATER(self).surface_wave_dir  [px] = 0.0; 
        WATER(self).surface_wave_speed[px] = 0.0;
        WATER(self).surface_wave_dir2 [px] = 0.0; 
    }
	
	return self;	
}


void water_signal(object* water_obj, float x, float yspeed, int sfx) {
	
	if ( WATER(water_obj).inactive ) return;
	
	if (sfx && WATER(water_obj).sound_delay <= 0 )
	if ( water_obj->origin_chunk_id == ( global.gs.chunk_x + global.gs.chunk_y * WORLD_SCREENS_W ) ) {
		gamestate_play_sfx( &global.gs, SFX_WATER );
		WATER(water_obj).sound_delay = 10;
	}

	int pxbase;
	pxbase = (int)floorf(( x - WATER(water_obj).body.x ) / 2.0);

	float v = yspeed * 0.5;

	int range = 1;

	int start = pxbase - range;
	int target = pxbase + range;

	for (int px = start; px <= target; px++) {

		if ( px < 0 || px >= WATER(water_obj).npoints ) { continue; } //out of bounds signal
		
		for (int k = 0; k < 2; k++) {
			
			create_waterdrop(
				WATER(water_obj).body.x + px*2 + rand_sign() * irandom(2), 
				WATER(water_obj).body.y + rand_sign() * irandom(2)
			);
			
		}
		
		WATER(water_obj).surface_speed[px] = fclamp(WATER(water_obj).surface_speed[px] + v,
			-MAX_SURFACE_YSPEED, MAX_SURFACE_YSPEED );
		   
		if (px == start) {
			
			WATER(water_obj).surface_wave_dir  [px] = -1; 
			WATER(water_obj).surface_wave_speed[px] =  v;
		}    
		
		if (px == target) {
			
			WATER(water_obj).surface_wave_dir  [px] =  1; 
			WATER(water_obj).surface_wave_speed[px] =  v;
		}   
		
	}
	
}

int generic_water_interaction( body2d* body ) {
	
	object* water = body2d_object_at( body, body->x, body->y, OBJECT_TYPE_WATER );
	
	if (water) {
		body2d water_surface;
		memcpy( &water_surface, &water->class.water.body, sizeof(body2d) );
		water_surface.h = 4;
		
		if ( body2d_1on1_check( body, &water_surface ) ) {
			if ( fabsf(body->yspeed) >= 1.0 ) {
				water_signal(water, body->x, body->yspeed, 1);
				return 1;
			}
		}
	}
	
	return 0;
}