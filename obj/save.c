#include "save.h"
#include "../gecko_sprite.h"
#include "../gecko_global.h"
#include "../gecko_draw.h"
#include <math.h>

#define SAVE(self) (self->class.save)

#define ORIGIN_YOFFSET (20)

static void _save_step( gamestate* gs, object* self, int step ) {
	
	if ( step != STEP_0 ) return;
	
	//body2d* body = &SAVE(self).body;
	SAVE(self).t += 2;
	int deg = SAVE(self).t % 360;
	
	SAVE(self).body.y = SAVE(self).origin_y - ORIGIN_YOFFSET + 2.0 * sinf( M_PI * (float)deg / 180.0 ); 
	
}

static void _save_draw( gamestate* gs, object* self, int layer ) {
	
	switch(layer) {
		
		case LAYER_SPR_0:
		{
			draw_sprite_relative( global.main_canvas, 
				(int16_t)floorf( SAVE(self).origin_x ) - gs->cam_x,
				(int16_t)floorf( SAVE(self).origin_y ) - gs->cam_y,
				0,0,0, SPR_SAVE, 2
			);

		}
		break;
		
		case LAYER_SPR_3:
		{
			bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_INVERT);
				
			draw_sprite_relative( global.main_canvas, 
				(int16_t)floorf( SAVE(self).body.x ) - gs->cam_x,
				(int16_t)roundf( SAVE(self).body.y ) - gs->cam_y,
				0,0,0, SPR_CIRCLE16, 5
			);
			
			bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_NORMAL);

		}
		break;
		
	}
	
}

static void* _save_component( object* self, int component ) {
	
	if (component == COMPONENT_BODY2D) {
		return &SAVE(self).body;
	}
	
	return NULL;
}

object* create_save(float x, float y) {
	
	object* self = object_new( OBJECT_LIST_MAIN );
	self->type = OBJECT_TYPE_SAVE;
	self->step = _save_step;
	self->draw = _save_draw;
	self->get_component = _save_component;
			
	SAVE(self).origin_x = x;
	SAVE(self).origin_y = y;
	SAVE(self).t = 0;
	
	body2d_initialize( &SAVE(self).body, x, y - ORIGIN_YOFFSET, 16, 16, 8, 8);
	
	return self;
}