#include "moongazer.h"
#include "moonorb.h"
#include "enemyball.h"
#include "gecko.h"
#include "temp.h"
//#include "../gecko_input.h"
#include "../gecko_draw.h"
#include "../gecko_global.h"
#include "../gecko_sprite.h"
#include "../gecko_magic.h"
#include "../gecko_misc.h"
//#include "../gecko_animation.h"

#include <math.h>
#include <stdio.h>

#define MOON(self) (self->class.moongazer)

#define MAX_TIME_MOON_SINE (110)
#define MAX_TIME_MOON_VERTICAL (40)
#define MAX_TIME_MOON_BARRAGE (100)
#define MAX_TIME_MOON_AIM (80)
#define MAX_TIME_MOON_CENTER (160)

#define SECOND_PHASE_HEALTH (50)

#define STARTUP_SPEED (4.0)

enum _MOON_MODES {
	MOON_INTRO = 0,
	MOON_SINE_STARTUP,
	MOON_SINE, //goes top right and moves in a sine wave pattern of sorts
	MOON_VERTICAL_STARTUP,
	MOON_VERTICAL, //goes to the center bottom of the screen and then moves up while shooting a lot of bs 
	MOON_CENTER_STARTUP,
	MOON_CENTER, //bullet hell sort of deal 
	MOON_AIM_STARTUP,
	MOON_AIM, //top left, aims and shoots at the player for a while 
	MOON_BARRAGE_STARTUP,
	MOON_BARRAGE, //bottom left, shoots a barrage horizontally that starts angling up and then the same but starting vertically 
	MOON_RECOVERY, //just kinda vibin and waitin 
	MOON_DEATH
};

static void* _moon_component( object* self, int component ) {
	
	switch(component) {
		
		case COMPONENT_BODY2D:    return &MOON(self).body;
		case COMPONENT_ENEMYDATA: return &MOON(self).enemy;
	}
	
	return NULL;
}

//roll will only do center if phase is true 
static int _moon_roll_new_mode(int phase) {
	
	int range = 3;
	if (phase) range = 4;
	
	switch( irandom(range) ) {
		case 0: return MOON_SINE_STARTUP;
		case 1: return MOON_AIM_STARTUP;
		case 2: return MOON_BARRAGE_STARTUP;
		case 3: return MOON_VERTICAL_STARTUP;
		case 4: return MOON_CENTER_STARTUP;
	}
	
	return -1; //error lmao 
}

//if too far away from target, set state_left 
static void _moon_extend_state_by_distance( object* self, int state_left ) {
	
	body2d* body = &MOON(self).body;
	
	float dist = body2d_distance_from( body, MOON(self).target_x, MOON(self).target_y );
	if (dist > 0.4) {
		MOON(self).state_left = state_left;
	}
	
}

static void _moon_step( gamestate* gs, object* self, int step ) {
	
	if ( gs->chunk_x != 8 || gs->chunk_y != 0 ) { self->destroy = 1; return; }
	
	if (step != STEP_0) return;
	
	body2d* body = &MOON(self).body;
	MOON(self).t++;
	
	if ( MOON(self).flicker_left > 0 ) MOON(self).flicker_left--;
	
	if ( MOON(self).last_hp != MOON(self).enemy.hp ) {
		MOON(self).last_hp = MOON(self).enemy.hp;
		MOON(self).flicker_left = 8;
		gamestate_play_sfx( gs, SFX_ENEMYHIT );
	}
	
	//MOON(self).radius = 0.0; //by default the orbs are retracted 
	float radius_delta = -1.5;
	float max_radius = 24.0;
	MOON(self).xoffset = 0;
	MOON(self).sprite = SPR_MOONGAZER0;
	MOON(self).enemy.active = 0;
	ORB( MOON(self).orbs[0] ).enemy.active = 0;
	ORB( MOON(self).orbs[1] ).enemy.active = 0;
	
	if ( MOON(self).phase && MOON(self).mode != MOON_DEATH ) {
		MOON(self).light_px = (int8_t)iclamp( MOON(self).light_px + 1, 0, SCREEN_W/2 );
	}
	
	if ( MOON(self).state_left > 0 ) MOON(self).state_left--;
	else {
		
		if ( !MOON(self).phase && MOON(self).enemy.hp <= SECOND_PHASE_HEALTH ) {
			MOON(self).mode = MOON_CENTER_STARTUP;
		}
		else {
		
			MOON(self).mode = _moon_roll_new_mode( MOON(self).phase );
			if ( MOON(self).mode == MOON(self).last_mode ) {
				MOON(self).mode = _moon_roll_new_mode( MOON(self).phase );
			}
		}
		
		MOON(self).last_mode = MOON(self).mode;
		MOON(self).state_left = 10;
	}
	
	if ( MOON(self).enemy.hp <= 0 && MOON(self).mode != MOON_DEATH ) {
		MOON(self).mode = MOON_DEATH;
		MOON(self).state_left = 9999;
		gamestate_play_sfx( gs, SFX_FULLROAR );
		MOON(self).rng = 0;
	}
	
	switch( MOON(self).mode ) {
		
		case MOON_DEATH:
		{
			MOON(self).sprite = SPR_MOONGAZER1;
			MOON(self).xoffset = (MOON(self).state_left / 2) % 2;
			MOON(self).target_x = gs->cam_x + SCREEN_W / 2;
			MOON(self).target_y = gs->cam_y + SCREEN_H / 2;
			MOON(self).enemy.iframes = 1;
			
			
			if ( MOON(self).state_left % 60 == 0 || MOON(self).state_left % 142 == 0 ) {
				
				if ( MOON(self).fireworks_left > 0 ) {
				
					gamestate_play_sfx( gs, SFX_BREAK );
					gamestate_play_sfx( gs, SFX_ROAR );
					MOON(self).rng = rng_next( MOON(self).rng );
					MOON(self).fireworks_left--;
					float theta = M_PI * (float)( MOON(self).rng % 360) / 180.0;
					
					for (int k = 1; k <= 15; k++) {
						
						float spd = 3.0 * (float)k / 15.0;
						
						MOON(self).rng = rng_next( MOON(self).rng );
						bit1cd_sprite* spr = MOON(self).rng % 2 ? SPR_SBALL0 : SPR_BALL0;
						
						MOON(self).rng = rng_next( MOON(self).rng );
						uint8_t colormode = MOON(self).rng % 2 ? BIT1CD_COLORMODE_INVERT_SOURCE : BIT1CD_COLORMODE_NORMAL;
						
						float xspeed =  spd * cosf(theta) + ( uniform() * rand_sign() * 0.4 );
						float yspeed = -spd * sinf(theta) + ( uniform() * rand_sign() * 0.4 );
						
						object* p = create_temp_static( body->x, body->y, xspeed, yspeed,
							spr, 1200, LAYER_SPR_3,  5,
							0.975, colormode);
							
						p->ignore_pause = 1;
						
					}
				
				}	
			}
			
			if ( MOON(self).fireworks_left <= 0 ) {
				
				if ( MOON(self).light_px == SCREEN_W/2 ) {
					gamestate_play_sfx( gs, SFX_ORB );
				}
				
				MOON(self).light_px -= 8;
				
				if ( MOON(self).light_px <= 0 ) {
				
					gamestate_play_sfx( gs, SFX_BREAK );
					gamestate_play_sfx( gs, SFX_FULLROAR );
					
					self->destroy = 1;
					
					for (int k = 1; k <= 40; k++) {
						
						float spd = 1.0 + 3.0 * uniform();
						
						MOON(self).rng = rng_next( MOON(self).rng );
						bit1cd_sprite* spr = MOON(self).rng % 2 ? SPR_SBALL0 : SPR_BALL0;
						
						MOON(self).rng = rng_next( MOON(self).rng );
						uint8_t colormode = MOON(self).rng % 2 ? BIT1CD_COLORMODE_INVERT_SOURCE : BIT1CD_COLORMODE_NORMAL;
						
						float theta = 2.0 * M_PI * uniform();
						
						float xspeed =  spd * cosf(theta);
						float yspeed = -spd * sinf(theta);
						
						object* p = create_temp_static( body->x, body->y, xspeed, yspeed,
							spr, 1200, LAYER_SPR_3,  5,
							0.975, colormode);
							
						p->ignore_pause = 1;
						
					}		
				}
			}
		}
		break;
		
		case MOON_INTRO:
		{
			MOON(self).max_speed = 2.0;
			MOON(self).target_x = gs->cam_x + SCREEN_W / 2;
			MOON(self).target_y = gs->cam_y + SCREEN_H / 2;
			
			//for (int k = 0; k < 2; k++) {
			//	MOON(self).orb_target_x[k] = body->x;
			//	MOON(self).orb_target_y[k] = body->y;
			//}
			
			_moon_extend_state_by_distance( self, 30 );
			
			if ( MOON(self).state_left == 25 ) {
				gamestate_play_sfx(gs, SFX_ROAR);
			}
			
			
		}
		break;
		
		case MOON_SINE_STARTUP:
		{
			MOON(self).max_speed = STARTUP_SPEED;
			MOON(self).target_x = gs->cam_x + 8*16;
			MOON(self).target_y = gs->cam_y + 8*3;
			
			_moon_extend_state_by_distance( self, 4 );
			
			if ( MOON(self).state_left == 1 ) {
				MOON(self).mode = MOON_SINE;
				MOON(self).state_left = MAX_TIME_MOON_SINE;
				gamestate_play_sfx( gs, SFX_ORB );
			}
			
			//MOON(self).radius = 24.0;
			//radius_delta = 2;
			
		}
		break;
		
		case MOON_SINE:
		{
			int t = MAX_TIME_MOON_SINE - MOON(self).state_left;
			float theta = 2.0 * M_PI * (float)t / (float)MAX_TIME_MOON_SINE;
			MOON(self).max_speed = 3.0;
			MOON(self).target_x -= 2.7 * sinf( theta );
			MOON(self).target_y += 1.0;
			
			if ( MOON(self).state_left == 1 ) {
				MOON(self).mode = MOON_RECOVERY;
				MOON(self).state_left = 26;
			}
			
			radius_delta = 1.5;
			
			MOON(self).enemy.active = 1;
			ORB( MOON(self).orbs[0] ).enemy.active = 1;
			ORB( MOON(self).orbs[1] ).enemy.active = 1;
			MOON(self).sprite = SPR_MOONGAZER1;
			
		}
		break;
		
		case MOON_VERTICAL_STARTUP:
		{
			MOON(self).max_speed = STARTUP_SPEED;
			MOON(self).target_x = gs->cam_x + SCREEN_W/2;
			MOON(self).target_y = gs->cam_y + 8*16 + 4; //+4 so that first ball actually hits grounded players 
			
			_moon_extend_state_by_distance( self, 7 );
			
			if ( MOON(self).state_left == 1 ) {
				MOON(self).mode = MOON_VERTICAL;
				MOON(self).state_left = MAX_TIME_MOON_VERTICAL;
			}
		}
		break;
		
		case MOON_VERTICAL:
		{
			MOON(self).max_speed = 3.0;
			MOON(self).target_y -= 2.0;
			
			if ( MOON(self).state_left == 1 ) {
				MOON(self).mode = MOON_RECOVERY;
				MOON(self).state_left = 26;
			}
			
			if ( MOON(self).state_left % 5 == 0 ) {
				
				create_eball(body->x, body->y,  3.5, 0, 50);
				create_eball(body->x, body->y, -3.5, 0, 50);
				gamestate_play_sfx( gs, SFX_FIRE );
				
			}
		}
		break;
	
		case MOON_BARRAGE_STARTUP:
		{
			MOON(self).max_speed = STARTUP_SPEED;
			MOON(self).target_x = gs->cam_x + 8*4;
			MOON(self).target_y = gs->cam_y + 8*16;
			
			_moon_extend_state_by_distance( self, 7 );
			
			if ( MOON(self).state_left == 1 ) {
				MOON(self).mode = MOON_BARRAGE;
				MOON(self).state_left = MAX_TIME_MOON_BARRAGE;
			}
		}
		break;	
		
		case MOON_BARRAGE:
		{
			int t = MAX_TIME_MOON_BARRAGE - MOON(self).state_left;
			int interval = (MAX_TIME_MOON_BARRAGE / 2) - 15;
			
			if (t > interval && MOON(self).state_left > interval) break;
			
			float ptg;
			float theta;
			
			if (t <= interval) {
				ptg = (float)t / (float)interval;
				theta = 0.25 * M_PI * ptg - 0.1; // -0.1 to make low ones actually hit grounded players 
			}
			else {
				ptg = (float)MOON(self).state_left / (float)interval;
				ptg = 1.0 - ptg;
				theta =  (0.75 * M_PI) - 0.5 * M_PI * ptg;
			}
			
			if ( (t % 5) == 0 ) {
				float spd = 4.0;
				create_eball(body->x, body->y, spd * cosf(theta), -spd * sinf(theta), 60);
				gamestate_play_sfx( gs, SFX_FIRE );
			}
			
			if ( MOON(self).state_left == 1 ) {
				MOON(self).mode = MOON_RECOVERY;
				MOON(self).state_left = 30;
			}
			
		}
		break;
		
		case MOON_AIM_STARTUP:
		{
			MOON(self).max_speed = STARTUP_SPEED;
			MOON(self).target_x = gs->cam_x + 8*4;
			MOON(self).target_y = gs->cam_y + 8*2;
			
			_moon_extend_state_by_distance( self, 7 );
			
			if ( MOON(self).state_left == 1 ) {
				MOON(self).mode = MOON_AIM;
				MOON(self).state_left = MAX_TIME_MOON_AIM;
			}
		}
		break;
		
		case MOON_AIM:
		{
			if ( (MOON(self).state_left % 18) == 0 ) {

				object* gecko = find_gecko();
				if (!gecko) break;
				
				float spd = 3.0;
				float ux,uy;
				body2d_unit_vector_at( body, GECKO(gecko).body.x, GECKO(gecko).body.y - 4, &ux, &uy );
				
				create_eball(body->x, body->y, ux * spd, uy * spd, 55);
				gamestate_play_sfx( gs, SFX_FIRE );
			}
			
			if ( MOON(self).state_left == 1 ) {
				MOON(self).mode = MOON_RECOVERY;
				MOON(self).state_left = 20;
			}
			
		}
		break;
		
		case MOON_CENTER_STARTUP:
		{
			MOON(self).max_speed = STARTUP_SPEED;
			MOON(self).target_x = gs->cam_x + SCREEN_W/2;
			MOON(self).target_y = gs->cam_y + 8*2;
			
			_moon_extend_state_by_distance( self, 7 );
			
			if ( MOON(self).state_left == 1 ) {
				MOON(self).mode = MOON_CENTER;
				MOON(self).state_left = MAX_TIME_MOON_CENTER;
				MOON(self).rng = irandom(1) ? 341 : 42; //2 possible seeds 
			}
		}
		break;
		
		case MOON_CENTER:
		{
			MOON(self).sprite = SPR_MOONGAZER1;
			MOON(self).xoffset = (MOON(self).state_left / 2) % 2;
			
			if ( (MOON(self).state_left % 8) == 0 ) {
				
				for (int k = 0; k < 2; k++) {
				
					MOON(self).rng = rng_next( MOON(self).rng );	
					int range = 60;
					uint8_t rvalue = (MOON(self).rng) % range;
					float deadzone = 0.15;
					float rdeadzone = 1 - deadzone;
					float theta = M_PI + M_PI*deadzone + ( M_PI * rdeadzone ) * (float)( rvalue ) / (float)range;
					
					float spd = 2.0;
					
					create_eball(body->x, body->y, spd * cosf(theta), -spd * sinf(theta), 70);
				}
				
				gamestate_play_sfx( gs, SFX_FIRE );
			}
			
			if ( MOON(self).state_left == 1 ) {
				MOON(self).mode = MOON_RECOVERY;
				MOON(self).state_left = 15;
			}
			
			if ( !MOON(self).phase && MOON(self).enemy.hp <= SECOND_PHASE_HEALTH ) {
				MOON(self).phase = 1;
				gamestate_play_sfx(gs, SFX_ROAR);
				gamestate_play_sfx(gs, SFX_ORB);
			}
			
		}
		break;
	}
	
	MOON(self).radius = fclamp( MOON(self).radius + radius_delta, 0, max_radius );
	
	for (int k = 0; k < 2; k++) {
		float r = MOON(self).radius;
		//if (k > 0 && !MOON(self).phase) r = 0.0;
		
		float theta = M_PI * (float)( (4 * MOON(self).t) % 360 ) / 180.0;
		if (k > 0) theta += M_PI;
		MOON(self).orb_target_x[k] = body->x + r * cosf( theta );
		MOON(self).orb_target_y[k] = body->y - r * sinf( theta );
	}
	
	float a = 10.0;
	float dist = body2d_distance_from( body, MOON(self).target_x, MOON(self).target_y );
	float ux, uy;
	body2d_unit_vector_at( body, MOON(self).target_x, MOON(self).target_y, &ux, &uy );
	
	body->xspeed = fclamp( ux * dist / a, -MOON(self).max_speed, MOON(self).max_speed );
	body->yspeed = fclamp( uy * dist / a, -MOON(self).max_speed, MOON(self).max_speed );
	
	body->x += body->xspeed;
	body->y += body->yspeed;
	
	if (dist < 0.5) {
		body->x = MOON(self).target_x;
		body->y = MOON(self).target_y;	
	}
	
	for (int k = 0; k < 2; k++) {
		body2d* orb_body = &ORB( MOON(self).orbs[k] ).body;
		
		float factor = 1.0;
		//if (MOON(self).radius < 0.5) factor = 1.5;
		
		orb_body->x += ( MOON(self).orb_target_x[k] - orb_body->x ) / factor;
		orb_body->y += ( MOON(self).orb_target_y[k] - orb_body->y ) / factor;
	}
	
}

static void _moon_draw( gamestate* gs, object* self, int layer ) {
	
	if ( layer == LAYER_PRE_TILES ) {
		int16_t w = MOON(self).light_px * 2;
		bit1cd_draw_rect( global.main_canvas, SCREEN_W/2 - w/2, 0, w, SCREEN_H, BIT1CD_COLOR_LIGHT );
	}
	
	if ( layer == LAYER_SPR_2 ) {
		
		if ( ( MOON(self).flicker_left / 2 ) % 2 > 0 ) return;
		
		body2d* body = &MOON(self).body;
		
		draw_sprite_relative( global.main_canvas,
			(int16_t)roundf( body->x ) - gs->cam_x + MOON(self).xoffset,
			(int16_t)roundf( body->y ) - gs->cam_y,
			0,0,0,
			MOON(self).sprite, 5
		);
	}
}


object* create_moongazer(float x, float y) {
	
	object* self = object_new( OBJECT_LIST_MAIN );
	
	self->type = OBJECT_TYPE_MOONGAZER;
	self->step = _moon_step;
	self->draw = _moon_draw;
	self->get_component = _moon_component;
	
	body2d_initialize(&MOON(self).body, x, y, 12, 12, 6, 6);
	enemy_initialize (&MOON(self).enemy, 100, 0, 0);
	MOON(self).sprite = SPR_MOONGAZER0;
	MOON(self).mode = MOON_INTRO;
	MOON(self).last_mode = MOON_INTRO;
	MOON(self).state_left = 10;
	MOON(self).t = 0;
	MOON(self).radius = 0.0;
	MOON(self).phase = 0;
	MOON(self).xoffset = 0;
	MOON(self).last_hp = MOON(self).enemy.hp;
	MOON(self).flicker_left = 0;
	MOON(self).light_px = 0;
	MOON(self).fireworks_left = 5;
	
	MOON(self).orbs[0] = create_moonorb(x, y);
	MOON(self).orbs[1] = create_moonorb(x, y);
	
	ORB( MOON(self).orbs[1] ).t = 240;
	
	return self;
}