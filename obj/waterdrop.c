#include "waterdrop.h"
#include "../gecko_object.h"
#include "../gecko_misc.h"
#include "../gecko_sprite.h"
#include "../gecko_global.h"
#include <math.h>

#define WATERDROP(self) (self->class.waterdrop)

static void _waterdrop_step( gamestate* gs, object* self, int step ) {
	
	if (step != STEP_0) return;
	
	WATERDROP(self).TTL--;
	if (WATERDROP(self).TTL <= 0) { self->destroy = 1; }
	
	if ( body2d_place_meeting( &WATERDROP(self).body,
		WATERDROP(self).body.x,
		WATERDROP(self).body.y,
		OBJECT_TYPE_BLOCK ) 
	) { self->destroy = 1; }
	
	if ( body2d_place_meeting( &WATERDROP(self).body,
		WATERDROP(self).body.x,
		WATERDROP(self).body.y,
		OBJECT_TYPE_WATER ) 
		&&
		WATERDROP(self).body.yspeed > 0
	) { self->destroy = 1; }

	WATERDROP(self).body.yspeed += 0.1;
	
	WATERDROP(self).body.x += WATERDROP(self).body.xspeed;
	WATERDROP(self).body.y += WATERDROP(self).body.yspeed;
}

static void _waterdrop_draw( gamestate* gs, object* self, int layer ) {
	if (layer != LAYER_SPR_1) { return; }
	
	bit1cd_draw_sprite( global.main_canvas,
		(int16_t)floorf( WATERDROP(self).body.x ) - gs->cam_x,
		(int16_t)floorf( WATERDROP(self).body.y ) - gs->cam_y,
		0, 1,
		0, SPR_WATERDROP
	);
}

void create_waterdrop(float x, float y) {
	
	object* self = object_new( OBJECT_LIST_SUB );
	self->step = _waterdrop_step;
	self->draw = _waterdrop_draw;
	body2d_initialize( &WATERDROP(self).body, x,y, 1,1, 0,0 );
	
	WATERDROP(self).TTL = 240;
	WATERDROP(self).body.xspeed = rand_sign() * uniform();
	WATERDROP(self).body.yspeed = -2.0 * uniform();
	
}