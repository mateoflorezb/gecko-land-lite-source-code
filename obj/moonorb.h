#ifndef _OBJ_MOON_ORB_H
#define _OBJ_MOON_ORB_H

#include "../gecko_object.h"

#define ORB(self) (self->class.moonorb)

object* create_moonorb(float x, float y);

#endif