#ifndef _OBJ_ENEMYBALL_H
#define _OBJ_ENEMYBALL_H

#include "../gecko_object.h"

object* create_eball(float x, float y, float xspeed, float yspeed, int16_t TTL);

#endif