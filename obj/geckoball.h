#ifndef _OBJ_GECKOBALL_H
#define _OBJ_GECKOBALL_H

#include "../gecko_object.h"

object* create_geckoball(float x, float y, float xspeed);

#endif