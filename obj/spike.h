#ifndef _OBJ_SPIKE_H
#define _OBJ_SPIKE_H

#include "../gecko_object.h"

object* create_spike(float x, float y, uint8_t node_data);

#endif