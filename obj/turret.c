#include "turret.h"
#include "enemyball.h"
#include "../gecko_sprite.h"
#include "../gecko_global.h"
#include "../gecko_draw.h"
#include "../gecko_misc.h"
#include "../gecko_magic.h"
#include <math.h>
#include <string.h>

#define TURRET(self) (self->class.turret)

static void* _turret_component( object* self, int component ) {
	
	switch(component) {
		
		case COMPONENT_BODY2D:    return &TURRET(self).body;
		case COMPONENT_ENEMYDATA: return &TURRET(self).enemy;
	}
	
	return NULL;
}

static void _turret_step( gamestate* gs, object* self, int step ) {
	
	if ( step != STEP_0 ) return;
	
	if ( self->origin_chunk_id == ( gs->chunk_x + gs->chunk_y * WORLD_SCREENS_W ) )
	if ( (gs->tickcount % 60) == 0 && (!gs->midnight) ) {
		//spawn shot here 
		create_eball(
			TURRET(self).body.x,
			TURRET(self).body.y,
			TURRET(self).shot_xspeed,
			TURRET(self).shot_yspeed,
			120
		);
		
		gamestate_play_sfx(gs, SFX_FIRE);
	}
	
}

static void _turret_draw( gamestate* gs, object* self, int layer ) {
	
	if (layer != LAYER_SPR_2) return;
	
	draw_sprite_relative( global.main_canvas,
		(int16_t) floorf( TURRET(self).body.x ) - gs->cam_x, 
		(int16_t) floorf( TURRET(self).body.y ) - gs->cam_y, 
		0,0,0, SPR_TURRET, 5
	);
	
}

object* create_turret(float x, float y, uint8_t node_data) {

	object* self = object_new( OBJECT_LIST_MAIN );
	self->step = _turret_step;
	self->draw = _turret_draw;
	self->get_component = _turret_component;
	
	body2d_initialize( &TURRET(self).body, x, y, 8, 8, 4, 4);
	enemy_initialize ( &TURRET(self).enemy, 1, 1, 1);
	
	const int8_t spd = 2;
	
	int8_t shot_xspeed = spd;
	int8_t shot_yspeed = 0;
	
	switch(node_data) {
		
		case 1:
			shot_xspeed =  0;
			shot_yspeed = -spd;
		break;
		
		case 2:
			shot_xspeed = -spd;
			shot_yspeed = 0;
		break;
		
		case 3:
			shot_xspeed =  0;
			shot_yspeed =  spd;
		break;
		
	} 
	
	TURRET(self).shot_xspeed = shot_xspeed;
	TURRET(self).shot_yspeed = shot_yspeed;
	
	return self;
}