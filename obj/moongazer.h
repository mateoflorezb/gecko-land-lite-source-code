#ifndef _OBJ_MOONGAZER_H
#define _OBJ_MOONGAZER_H

#include "../gecko_object.h"

object* create_moongazer(float x, float y);

#endif 