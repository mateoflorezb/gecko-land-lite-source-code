#include "pipe.h"
#include "waterfall.h"
#include "../gecko_draw.h"
#include "../gecko_sprite.h"
#include "../gecko_global.h"
#include <math.h>

#define PIPE(self) (self->class.pipe)

static void _pipe_step( gamestate* gs, object* self, int step ) {
	
	if (step != STEP_0) return;
	
	if ((gs->tickcount % 7) == 0) {
		create_waterfall( PIPE(self).x, PIPE(self).y );
	}
	
}

static void _pipe_draw( gamestate* gs, object* self, int layer ) {
	
	if (layer != LAYER_SPR_1) return;
	
	draw_sprite_relative( global.main_canvas,
		(int16_t) floorf(PIPE(self).x) - gs->cam_x,
		(int16_t) floorf(PIPE(self).y) - gs->cam_y,
		0,0,0, SPR_PIPE, 8 
	);
	
}

object* create_pipe(float x, float y) {
	
	object* self = object_new( OBJECT_LIST_MAIN );
	self->type = OBJECT_TYPE_PIPE;
	self->step = _pipe_step;
	self->draw = _pipe_draw;
	
	PIPE(self).x = x;
	PIPE(self).y = y;
	
	return self;
	
}