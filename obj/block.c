#include "block.h"

#include "../gecko_global.h"
#include <math.h>

static void _block_draw( gamestate* gs, object* self, int layer ) {
	
	if ( layer == LAYER_SPR_2 ) {
		
		body2d* body = &self->class.block.body;
		
		bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_INVERT );
		
		bit1cd_draw_rect( global.main_canvas,
		(int16_t) floorf( body->x ) - gs->cam_x,
		(int16_t) floorf( body->y ) - gs->cam_y,
		(int16_t) body->w, (int16_t) body->h, BIT1CD_COLOR_DARK );
	
		bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_NORMAL );
	}
	
}


static void* _block_component( object* self, int component_type ) {
	
	if ( component_type == COMPONENT_BODY2D ) {
		return (void*)( &self->class.block.body );
	}
	
	return NULL;
}

object* create_block(float x, float y, float w, float h) {
	
	object* self = object_new( OBJECT_LIST_MAIN );
	self->type = OBJECT_TYPE_BLOCK;
	//self->draw = _block_draw;
	self->get_component = _block_component;
	
	body2d_initialize(&self->class.block.body, x, y, w, h, 0, 0);
	
	return self;
	
}


object* create_platform(float x, float y, float w, float h) {
	
	object* plat = create_block(x,y,w,h);
	plat->type = OBJECT_TYPE_PLATFORM;
	return plat;
}