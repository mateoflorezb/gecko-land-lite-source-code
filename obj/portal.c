#include "portal.h"
#include "temp.h"
#include "gecko.h"
#include "../gecko_draw.h"
#include "../gecko_global.h"
#include "../gecko_sprite.h"
//#include "../gecko_magic.h"
#include "../gecko_misc.h"
#include "../gecko_animation.h"

#include <math.h>

#define CRYSTAL_OFFSET (24)

static void* _portal_component( object* self, int component_type ) {
	
	if ( component_type == COMPONENT_BODY2D ) {
		return (void*)( &PORTAL(self).body );
	}
	
	return NULL;
}

static void _portal_step( gamestate* gs, object* self, int step ) {
	
	if ( step != STEP_0 ) return;
	
	PORTAL(self).t++;
	body2d* body = &PORTAL(self).body;
	
	if ( self->ignore_pause ) {
		
		object* gecko = find_gecko();
		if (gecko) {
			body2d* gecko_body = gecko->get_component( gecko, COMPONENT_BODY2D );
			
			gecko_body->x += ( body->x   - gecko_body->x ) / 7.0;
			gecko_body->y += ( body->y+5 - gecko_body->y ) / 7.0;
		}
		
	}
	
	if (PORTAL(self).id == 0) {
		
		if (gs->midnight) { PORTAL(self).active = 1; }
	
		for (int crystal = UNLOCKED_CRYSTAL_0; crystal <= UNLOCKED_CRYSTAL_2; crystal++) {
			
			int16_t dx = 0;
			int16_t dy = 0;
			
			switch(crystal) {
				
				case UNLOCKED_CRYSTAL_0: dx = -CRYSTAL_OFFSET; break;
				case UNLOCKED_CRYSTAL_1: dy = -CRYSTAL_OFFSET; break;
				case UNLOCKED_CRYSTAL_2: dx =  CRYSTAL_OFFSET; break;
				
			}
			
			if ( !gs->unlocks[ crystal ] ) continue; 
			
			if (( gs->tickcount % 3 ) == 0 ) {
				
				float spd = 0.5 + 0.5*uniform();
				float angle = M_PI * (float)irandom(360) / 180.0;
				float xspeed = spd * cosf(angle);
				float yspeed = spd * sinf(angle);
				
				create_temp_static(
					body->x + rand_sign() * irandom(2) + dx,
					body->y + rand_sign() * irandom(2) + dy,
					xspeed, yspeed, 
					irandom(1) ? SPR_SBALL0 : SPR_TBALL0,
					18 + irandom(6), LAYER_SPR_0, 5,
					0.96, BIT1CD_COLORMODE_NORMAL
				);
			}
			
		}
	}
	else {
		PORTAL(self).active = 1;
	}
}

static void _portal_draw( gamestate* gs, object* self, int layer ) {
	
	body2d* body = &PORTAL(self).body;
	
	//crystals progress 
	if (PORTAL(self).id == 0) 
	if ( layer == LAYER_SPR_1 ) {
		
		for (int crystal = UNLOCKED_CRYSTAL_0; crystal <= UNLOCKED_CRYSTAL_2; crystal++) {
			
			int16_t dx = 0;
			int16_t dy = 0;
			
			switch(crystal) {
				
				case UNLOCKED_CRYSTAL_0: dx = -CRYSTAL_OFFSET; break;
				case UNLOCKED_CRYSTAL_1: dy = -CRYSTAL_OFFSET; break;
				case UNLOCKED_CRYSTAL_2: dx =  CRYSTAL_OFFSET; break;
				
			}
			
			bit1cd_sprite* spr = ( gs->unlocks[ crystal ] ) ? SPR_CRYSTAL0 : SPR_CRYSTAL1;
			
			draw_sprite_relative( global.main_canvas,
				(int16_t)floorf(body->x) - gs->cam_x + dx,
				(int16_t)floorf(body->y) - gs->cam_y + dy,
				0,0,0, spr, 5
			);
		}	
	}
	
	if ( layer == LAYER_SPR_3 ) {
		
		if ( !PORTAL(self).active ) return;
		
		bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_INVERT );
		
		for (int py = 0; py < 16; py++) {
			
			float dir = (py % 2) ? -1.0 : 1.0;
			
			float theta = M_PI * (float)(( 4 * PORTAL(self).t + 10 * py ) % 360) / 180.0;
			int16_t xoffset = roundf( 2.0 * cosf( theta ) * dir );
			
			int16_t xx = (int16_t)floorf(body->x) - gs->cam_x;
			int16_t yy = (int16_t)floorf(body->y) - gs->cam_y;
			
			bit1cd_draw_sprite_part( global.main_canvas, xx + xoffset, yy, 8, 8,
				0, py, 16, 1, SPR_CIRCLE16 );	
		}
		
		bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_NORMAL );
	}
}


object* create_portal(float x, float y) {
	
	object* self = object_new( OBJECT_LIST_MAIN );
	self->type = OBJECT_TYPE_PORTAL;
	self->step = _portal_step;
	self->draw = _portal_draw;
	self->get_component = _portal_component;
	
	body2d_initialize(&PORTAL(self).body, x, y, 12, 12, 6, 6);
	
	PORTAL(self).t = 0;
	PORTAL(self).id = 0;
	PORTAL(self).active = 0;
	
	return self;
}