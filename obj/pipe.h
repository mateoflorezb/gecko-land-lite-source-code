#ifndef _OBJ_PIPE_H
#define _OBJ_PIPE_H

#include "../gecko_object.h"

object* create_pipe(float x, float y);

#endif