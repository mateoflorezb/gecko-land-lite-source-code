#include "gecko.h"
#include "../gecko_input.h"
#include "../gecko_draw.h"
#include "../gecko_global.h"
#include "../gecko_sprite.h"
#include "../gecko_magic.h"
#include "../gecko_misc.h"
#include "../gecko_animation.h"

#include "water.h"
#include "temp.h"
#include "geckoball.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

object* find_gecko() {
	
	clist_iterator it = clist_start( global.gs.main_object_list );
	while (it != NULL) {
		
		object* obj = clist_get(it, object*);
		if (obj->destroy) { it = clist_next(it); continue; }
		if (obj->type == OBJECT_TYPE_GECKO) {
			
			return obj;
		}
		
		it = clist_next(it);
	}
	
	return NULL;
}

static void _geckoGOD_step( gamestate* gs, object* self, int step ) {
	
	body2d* body = &GECKO(self).body;
	
	switch( step ) {
		
		case STEP_0:
		{
			float spd = 3.0;
			
			if ( input_held( INPUT_R ) ) { body->x += spd; }
			if ( input_held( INPUT_L ) ) { body->x -= spd; }
			if ( input_held( INPUT_U ) ) { body->y -= spd; }
			if ( input_held( INPUT_D ) ) { body->y += spd; }
			
		}
		break;
		
	}
}

static void _gecko_death( gamestate* gs, object* self ) {
	
	gamestate_play_sfx(gs, SFX_DEATH);
	
	body2d* body = &GECKO(self).body;
	
	body->y -= 4;

	for (int k = 0; k < 30; k++) {
		
		int holds = 2 + irandom(10);
		
		object* p = create_temp_ex( body->x, body->y, 1.5 * uniform(), 1.5 * uniform(), 
			ANIMATION_PARTICLE, PARTICLE_NFRAMES, holds, PARTICLE_NFRAMES * (1+holds), LAYER_SPR_3,  5,
			0.95, BIT1CD_COLORMODE_NORMAL
		);
		
		p->ignore_pause = 1;
	}

	for (int k = 0; k < 30; k++) {
		
		float xspeed, yspeed;
		float angle = 2 * M_PI * uniform();
		float spd = 0.5 + 3 * uniform();
		xspeed = spd * cosf(angle);
		yspeed = spd * sinf(angle);

		bit1cd_sprite* spr = ( irandom(1) == 0 ) ? SPR_BALL0 : SPR_SBALL0;
		uint8_t colormode = ( irandom(1) == 0 ) ? BIT1CD_COLORMODE_NORMAL : BIT1CD_COLORMODE_INVERT_SOURCE;
		
		object* p = create_temp_static( body->x, body->y, xspeed, yspeed, 
			spr, 60 * 60, LAYER_SPR_3,  5,
			0.95, colormode
		);
		
		p->ignore_pause = 1;
	}
		
	self->destroy = 1;
}


static void _gecko_step( gamestate* gs, object* self, int step ) {
	
	body2d* body = &GECKO(self).body;
	
	switch( step ) {
		
		case STEP_0:
		{
			
			if ( GECKO(self).down_buffer        > 0 ) { GECKO(self).down_buffer--; } 
			if ( GECKO(self).platform_delay     > 0 ) { GECKO(self).platform_delay--; } 
			if ( GECKO(self).platform_ignore    > 0 ) { GECKO(self).platform_ignore--; }
			if ( GECKO(self).jump_buffer        > 0 ) { GECKO(self).jump_buffer--; }
			if ( GECKO(self).water_signal_delay > 0 ) { GECKO(self).water_signal_delay--; }
			
			//signal water 
			if ( GECKO(self).water_signal_delay <= 0 ) {
				if ( generic_water_interaction(body) ) { GECKO(self).water_signal_delay = 5; }
			}
			
			//find hazards and deth 
			body2d hurtbox;
			memcpy( &hurtbox, body, sizeof(body2d));
			
			//more lenient hurtbox when airborne 
			if (
				!body2d_place_meeting( body, body->x, body->y + 1.0, OBJECT_TYPE_BLOCK ) && 
				!body2d_place_meeting( body, body->x, body->y + 1.0, OBJECT_TYPE_PLATFORM ) 
			) {
				hurtbox.w = 4;
				hurtbox.h = 4;
				hurtbox.xoffset = 2;
				hurtbox.yoffset = 5; //yeah, 5 is right 
			}
			
			clist_iterator it = clist_start( global.gs.main_object_list );
			while (it != NULL) {
				
				object* obj = clist_get(it, object*);
				
				if (obj->destroy) { it = clist_next(it); continue; }
				
				enemydata* enemy = obj->get_component( obj, COMPONENT_ENEMYDATA );
				if (!enemy)		    { it = clist_next(it); continue; }
				if (!enemy->active) { it = clist_next(it); continue; }
				
				body2d* enemy_body = obj->get_component( obj, COMPONENT_BODY2D );
				if (!enemy_body) { it = clist_next(it); continue; }
				
				//deth 
				if ( body2d_1on1_check( &hurtbox, enemy_body ) ) {
					_gecko_death( gs, self );
					return;
				}
				
				it = clist_next(it);
			}
			
			//find save orbs 
			if ( !GECKO(self).orb_ignore ) {
				object* save = body2d_object_at( body, body->x, body->y, OBJECT_TYPE_SAVE );
				if (save) {
					
					if ( !GECKO(self).orb_floating ) {
						
						GECKO(self).orb_floating = 1;
						gs->checkpoint_x = save->origin_chunk_id % WORLD_SCREENS_W;
						gs->checkpoint_y = save->origin_chunk_id / WORLD_SCREENS_W;
						
						memcpy( gs->checkpoint_unlocks, gs->unlocks, sizeof( gs->unlocks ) );
						gs->checkpoint_midnight = gs->midnight;
						
						gamestate_play_sfx(gs, SFX_SAVE);
					}
					
					body2d* orb_body = save->get_component(save, COMPONENT_BODY2D);
					if (orb_body) {
						float a = 8;
						body->x += (orb_body->x - body->x)     / a; 
						if (fabsf(body->x - orb_body->x) < 0.5) { body->x = orb_body->x; } 
						body->y += (orb_body->y - body->y + 5) / a; 
					}
					
				}
			}
			
		}
		break;
		
		case STEP_1:
		{
			
			if (GECKO(self).orb_floating) {
				
				GECKO(self).sprite = SPR_PLAYER_JUMP1;
				GECKO(self).air_jump = 1;
				body->xspeed = 0;
				body->yspeed = 0;
				
				if ( input_pressed( INPUT_A ) ) {
					GECKO(self).orb_floating = 0;
					GECKO(self).orb_ignore   = 1;
				}
				else {
					return;
				}
				
			}
			
			int inAir = 1;
			float grav = 0.18;
			float walkSpeed = 1.5;
			float jumpSpeed = 3;
			int   max_air_time = 8;
			float max_vspeed = 4.5;
			int inWater = 0;
			
			if (
				 body2d_place_meeting( body, body->x, body->y + 1.0, OBJECT_TYPE_BLOCK    ) ||
				(
					 GECKO(self).platform_ignore <= 0 &&
					 body->yspeed == 0 &&
					 body2d_place_meeting( body, body->x, body->y + 1.0, OBJECT_TYPE_PLATFORM ) &&
					!body2d_place_meeting( body, body->x, body->y,       OBJECT_TYPE_PLATFORM )
				)
			) {
				inAir = 0;
			}
			
			if ( !inAir ) {
				if ( gs->unlocks[ UNLOCKED_FEATHER ] ) GECKO(self).air_jump = 1;
				GECKO(self).orb_ignore = 0; 
			}
			
			if ( body2d_place_meeting( body, body->x, body->y, OBJECT_TYPE_WATER ) ) {
				grav /= 2;
				max_vspeed = 1.5;
				jumpSpeed = 2.6;
				if ( gs->unlocks[ UNLOCKED_FEATHER ] ) GECKO(self).air_jump = 1;
				inWater = 1;
				walkSpeed = 1;
			}
			
			
			if ( inAir ) {
				body->yspeed += grav;
			}
			
			body->yspeed = fclamp( body->yspeed, -99.0, max_vspeed );
			
			//GECKO(self).air_jump = 1;    //temp ////////////////////////////////////////////////////////////////////////////////////////////

			int walking = 0;

			if (
				 ( input_held( INPUT_L ) || input_held( INPUT_R ) ) &&
				!( input_held( INPUT_L ) && input_held( INPUT_R ) ) 
			) {
				
				if ( input_held( INPUT_L ) )
				{ GECKO(self).xdir = -1; }
				else
				{ GECKO(self).xdir =  1; }

				walking = 1;
			}

			if (walking) {
				if (( fabsf(body->xspeed) < walkSpeed) || fsign(body->xspeed) != ((float) GECKO(self).xdir)) {
					body->xspeed += (walkSpeed/3) * GECKO(self).xdir;
					
					if ( fabsf( body->xspeed ) > walkSpeed ) {
						body->xspeed = walkSpeed * GECKO(self).xdir;
					}
				}
			}
			else { body->xspeed = 0; }
			
			if ( input_pressed( INPUT_A ) ) { GECKO(self).jump_buffer = 3; }
			
			if (( !inAir || GECKO(self).air_jump || inWater ) && ( GECKO(self).jump_buffer > 0 ) ) {
				GECKO(self).air_time = max_air_time;
				GECKO(self).jump_buffer = 0;
				GECKO(self).can_release_jump = 1;
				
				int sound = SFX_JUMP1;
				
				if (inAir && !inWater) {
					GECKO(self).air_jump = 0;
					sound = SFX_JUMP2;
				}
				
				gamestate_play_sfx(gs, sound);
				
			}
			
			if ( !input_held( INPUT_A ) ) { GECKO(self).air_time = 0; }

			if ( GECKO(self).air_time > 0 && input_held( INPUT_A ) ) {
				GECKO(self).air_time -= 1;
				body->yspeed = -jumpSpeed;
			}

			if ( inAir && !input_held( INPUT_A ) && GECKO(self).can_release_jump && body->yspeed < 0 ) {
				GECKO(self).can_release_jump = 0;
				body->yspeed *= 0.5;
			}
			
			int shooting = 0;
			
			if ( gs->unlocks[ UNLOCKED_FIRE ] ) {
			
				
				if ( input_held( INPUT_B ) || GECKO(self).shot_delay > 0 ) { shooting = 1; } 

				if ( input_pressed( INPUT_B ) || ( input_held( INPUT_B ) && GECKO(self).shot_delay <= 0 ) ) {
					
					//var p;
					//p = instance_create(x,y-5, obj_projectile)
					//p.hspeed = 3*xdir
					create_geckoball( body->x, body->y - 5, 4.0 * GECKO(self).xdir );
					GECKO(self).shot_delay = 8;
					gamestate_play_sfx(gs, SFX_SHOOT);
				}

				if ( GECKO(self).shot_delay > 0 ) { GECKO(self).shot_delay--; }
			}
			
			if ( input_pressed( INPUT_D ) ) { GECKO(self).down_buffer = 3; } 
			
			if ( GECKO(self).down_buffer > 0 ) {
				
				if ( body->yspeed == 0 && body2d_place_meeting( body, body->x, body->y + 1.0, OBJECT_TYPE_PLATFORM ) ) {
					if ( GECKO(self).platform_delay <= 0 ) { GECKO(self).platform_delay = 2; } 
				}
				
			}
			
			if ( GECKO(self).platform_delay == 1 ) { GECKO(self).platform_ignore = 3; }
			
			

			/////////// Animation
			
			if ( !inAir ) {

				if (walking) { GECKO(self).walk_counter++; }
				
				if (shooting) { GECKO(self).sprite = ( (GECKO(self).walk_counter % 10) < 5 ) ? SPR_PLAYER_WALKSHOOT0 : SPR_PLAYER_WALKSHOOT1; }
				else          { GECKO(self).sprite = ( (GECKO(self).walk_counter % 10) < 5 ) ? SPR_PLAYER_WALK0      : SPR_PLAYER_WALK1     ; }
			}
			else {	

				GECKO(self).walk_counter = 0;
				
				if (shooting) { GECKO(self).sprite = ( body->yspeed < 0) ? SPR_PLAYER_JUMPSHOOT0 : SPR_PLAYER_JUMPSHOOT1; }
				else          { GECKO(self).sprite = ( body->yspeed < 0) ? SPR_PLAYER_JUMP0      : SPR_PLAYER_JUMP1     ; }
			}
			
		}
		break;
		
		case STEP_2:
		{
			
			float prev_yspeed = body->yspeed;
			
			body2d_solid_col(body); 
			if ( GECKO(self).platform_ignore <= 0 ) body2d_platform_col(body);
			
			if (body->yspeed == 0 && prev_yspeed >= 3.9) {
				gamestate_play_sfx(gs, SFX_TAP);
			}
			
			body->x += body->xspeed;
			body->y += body->yspeed;	
		}
		break;
	}
}

void draw_gecko_body( gamestate* gs, object* gecko ) {
	body2d* body = &GECKO(gecko).body;
	int16_t xx = (int16_t)roundf ( body->x ) - gs->cam_x;
	int16_t yy = (int16_t)roundf ( body->y ) - gs->cam_y;
	
	uint8_t flip = ( GECKO(gecko).xdir > 0 ) ? 0 : BIT1CD_FLIP_HORIZONTAL;
	
	draw_sprite_relative( global.main_canvas, xx, yy, 0, 0,
		flip, GECKO(gecko).sprite, 2
	);
}

static void _gecko_draw( gamestate* gs, object* self, int layer ) {
	
	switch( layer ) {
		
		case LAYER_SPR_1:
		{
			if ( gs->unlocks[ UNLOCKED_LANTERN ] ) {
			
				body2d* body = &GECKO(self).body;
				int16_t xx = (int16_t)floorf ( body->x ) - gs->cam_x;
				int16_t yy = (int16_t)floorf ( body->y ) - gs->cam_y;
				draw_big_light( global.shadow_canvas, xx, yy );
			}
		}
		break;
		
		case LAYER_SPR_2:
		{
			draw_gecko_body( gs, self );
		}
		break;	
	}	
}

static void* _gecko_component( object* self, int component_type ) {
	
	if ( component_type == COMPONENT_BODY2D ) {
		return (void*)( &GECKO(self).body );
	}
	
	return NULL;
}

object* create_gecko(float x, float y) {
	
	object* self = object_new( OBJECT_LIST_MAIN );
	self->type = OBJECT_TYPE_GECKO;
	self->origin_chunk_id = -1;
	self->step = _gecko_step;
	self->draw = _gecko_draw;
	self->get_component = _gecko_component;
	
	body2d_initialize(&GECKO(self).body, x, y, 6.0, 6.0, 3, 6);
	
	GECKO(self).xdir = (int8_t)rand_sign();
	GECKO(self).can_release_jump = 0;
	GECKO(self).shot_delay = 0;
	GECKO(self).air_jump = 0; 
	GECKO(self).air_time = 0; 
	GECKO(self).jump_buffer = 0; 
	GECKO(self).water_signal_delay = 0; 
	GECKO(self).walk_counter = 0;
	GECKO(self).down_buffer = 0; 
	GECKO(self).platform_delay = 0; 
	GECKO(self).platform_ignore = 0; 
	GECKO(self).orb_ignore = 0; 
	GECKO(self).orb_floating = 0;
	GECKO(self).sprite = SPR_PLAYER_JUMP1;
	
	object* save = body2d_object_at( &GECKO(self).body, GECKO(self).body.x, GECKO(self).body.y, OBJECT_TYPE_SAVE );
	if (save) {
		GECKO(self).orb_floating = 1;
	}
	
	return self;
}