#include "fly.h"
#include "water.h"
#include "../gecko_sprite.h"
#include "../gecko_global.h"
#include "../gecko_draw.h"
#include "../gecko_animation.h"
#include "../gecko_misc.h"
#include <math.h>

#define FLY(self) (self->class.fly)


static void* _fly_component( object* self, int component ) {
	
	switch(component) {
		
		case COMPONENT_BODY2D:    return &FLY(self).body;
		case COMPONENT_ENEMYDATA: return &FLY(self).enemy;
	}
	
	return NULL;
}

static void _fly_step( gamestate* gs, object* self, int step ) {
	
	if ( step != STEP_0 ) return;
	
	if (gs->midnight) { self->destroy = 1; return; }
	
	body2d* body = &FLY(self).body;
	
	if (FLY(self).enemy.hp <= 0) { enemy_generic_death( gs, self, body->x, body->y ); }
	
	if (FLY(self).water_signal_delay > 0) { FLY(self).water_signal_delay--; }
	
	//signal water 
	if ( FLY(self).water_signal_delay <= 0 ) {
		if ( generic_water_interaction(body) ) { FLY(self).water_signal_delay = 5; }
	}
	
	if ( body2d_place_meeting( body,
		body->x + body->xspeed,
		body->y + body->yspeed, 
		OBJECT_TYPE_BLOCK )
	) {
		body->xspeed *= -1;
		body->yspeed *= -1;
	}		
	
	body->x += body->xspeed;
	body->y += body->yspeed;
	
	FLY(self).sprite = (((gs->tickcount + FLY(self).t_offset) % 16) < 8 ) ? SPR_FLY0 : SPR_FLY1;
	
}

static void _fly_draw( gamestate* gs, object* self, int layer ) {
	
	if (layer == LAYER_SPR_1) {
		draw_small_light( global.shadow_canvas,
			(int16_t)floorf ( FLY(self).body.x ) - gs->cam_x,
			(int16_t)floorf ( FLY(self).body.y ) - gs->cam_y
		);
	}
	
	if (layer == LAYER_SPR_2) {
	
		draw_sprite_relative( global.main_canvas,
			(int16_t) floorf( FLY(self).body.x ) - gs->cam_x,
			(int16_t) floorf( FLY(self).body.y ) - gs->cam_y,
			0,0,0, FLY(self).sprite, 5
		);
	
	}
	
}

object* create_fly(float x, float y, uint8_t node_data) {

	object* self = object_new( OBJECT_LIST_MAIN );
	self->step = _fly_step;
	self->draw = _fly_draw;
	self->get_component = _fly_component;
	
	
	body2d_initialize( &FLY(self).body, x, y, 8, 8, 4, 4);
	enemy_initialize ( &FLY(self).enemy, 1, 0, 1);
	FLY(self).t_offset = irandom(200);
	FLY(self).sprite = SPR_FLY0;
	FLY(self).water_signal_delay = 0;
	
	const float spd = 1;
	
	float xspeed = spd;
	float yspeed = 0;
	
	switch(node_data) {
		
		case 1:
			xspeed = 0;
			yspeed = -spd;
		break;
		
		case 2:
			xspeed = -spd;
			yspeed = 0;
		break;
		
		case 3:
			xspeed = 0;
			yspeed = spd;
		break;
	}
	
	FLY(self).body.xspeed = xspeed;
	FLY(self).body.yspeed = yspeed;
	
	return self;
}