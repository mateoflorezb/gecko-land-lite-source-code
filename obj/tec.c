#include "tec.h"
#include "../gecko_object.h"
#include "../gecko_global.h"
#include "../gecko_misc.h"
#include "../gecko_magic.h"
#include "../gecko_input.h"
#include "../gecko_sprite.h"
#include "../gecko_draw.h"
#include "../gecko_world.h"
#include "gecko.h"
#include "portal.h"
#include "temp.h"

#include <math.h>
#include <stdio.h>
#include <string.h>

#define BLACKBAR_W ( SCREEN_W + (8*8) )

#define MESSAGE_W_MAX ( SCREEN_W - 8 * 2 )
#define MESSAGE_H_MAX ( 9 * 5 + 4 ) //3 rows and 2 pixels offset top and bottom 


#define TEC(self) (self->class.tec)

static void _draw_rect_outline(bit1cd_canvas* cv, int16_t x, int16_t y, int16_t w, int16_t h) {
	
	bit1cd_draw_rect( cv, x-1, y-1, w+2, h+2, BIT1CD_COLOR_LIGHT );
	bit1cd_draw_rect( cv, x  , y  , w  , h  , BIT1CD_COLOR_DARK  );
	
}

enum _PAUSE_REASONS {
	PAUSE_REASON_NONE = 0,
	PAUSE_REASON_TITLE_SCREEN,
	PAUSE_REASON_PLAYER_PAUSE,
	PAUSE_REASON_CAMERA_TRANSITION,
	PAUSE_REASON_MESSAGE_DELAY,
	PAUSE_REASON_MESSAGE,
	PAUSE_REASON_MESSAGE_RECOVERY,
	PAUSE_REASON_TELEPORT,
	PAUSE_REASON_BOSS_CUTSCENE
};

static void _tec_release( object* self ) {
	
	printf("Warning: tec was released you dummy\n");
}

static void _tec_check_pause( gamestate* gs, object* self ) {
	
	if ( gs->pause_state ) { return; }
	
	object* player = find_gecko();
	
	if (!player && !TEC(self).transition_spawn_gecko) {
		return;
	}
	
	if (!player) {
		
		TEC(self).pause_reason = PAUSE_REASON_CAMERA_TRANSITION;
		TEC(self).transition_delay = 60; //frames left 
		TEC(self).blackbar_position = SCREEN_W; //top left corner of the big square that covers the whole screen in transitions 
		gamestate_pause( gs, 1 ); 
		TEC(self).transition_load_inventory = 1;
		TEC(self).transition_x = gs->checkpoint_x;
		TEC(self).transition_y = gs->checkpoint_y;
		gamestate_music( gs,  MUSIC_PAUSE );
		if ( gs->chunk_x >= 8 ) gamestate_music( gs,  MUSIC_PLAY_OVERWORLD );
		//TEC(self).transition_spawn_gecko = 1;
		return;
	}
	
	body2d* player_body = player->get_component(player, COMPONENT_BODY2D);
	object* portal = body2d_object_at( player_body, player_body->x, player_body->y, OBJECT_TYPE_PORTAL ); 
	
	if ( portal ) 
	if ( PORTAL(portal).active && !player->destroy ) {
		gamestate_pause( gs, 1 ); 
		TEC(self).pause_reason = PAUSE_REASON_TELEPORT;
		gamestate_play_sfx( gs, SFX_TELEPORT );
		portal->ignore_pause = 1;
		TEC(self).teleport_left = 45;
		TEC(self).last_portal_id = PORTAL(portal).id;
		gamestate_music( gs,  MUSIC_PAUSE );
		gamestate_music( gs,  MUSIC_PLAY_BOSS );
		return;
	}
	
	object* moon = object_find( gs, OBJECT_TYPE_MOONGAZER );
	
	if (moon) {
		enemydata* moon_enemy = moon->get_component( moon, COMPONENT_ENEMYDATA );
		if (moon_enemy->hp <= 0) {
			
			TEC(self).pause_reason = PAUSE_REASON_BOSS_CUTSCENE;
			TEC(self).boss_cutscene_left = 60*6;
			moon->ignore_pause = 1;
			gamestate_pause( gs, 1 ); 
			gamestate_music( gs,  MUSIC_PAUSE );
			return;
		}
	}
	
	if ( gs->queued_message ) {
		
		gs->queued_message = 0;
		gamestate_pause( gs, 1 ); 
		TEC(self).pause_reason = PAUSE_REASON_MESSAGE_DELAY;
		gamestate_music( gs,  MUSIC_PAUSE );
		return;
	}
	
	if ( input_pressed( INPUT_START ) && player != NULL && !gs->credits ) {
		gamestate_pause( gs, 1 ); 
		TEC(self).pause_reason = PAUSE_REASON_PLAYER_PAUSE;
		gamestate_play_sfx(gs, SFX_MENU);
		return;
	}
}

static void _tec_process_pause( gamestate* gs, object* self ) {
	
	switch(TEC(self).pause_reason) {
		
		case PAUSE_REASON_CAMERA_TRANSITION:
		{
			if ( TEC(self).transition_delay > 0 ) { TEC(self).transition_delay--; }
			else {
				
				const int speed = 8; // BLACKBAR_W needs to be a multiple of this 
				
				TEC(self).blackbar_position -= speed;
				
				if ( TEC(self).blackbar_position == 0 ) {
					gamestate_nuke_objects( gs );
					
					if ( TEC(self).transition_load_inventory ) {
						memcpy( gs->unlocks, gs->checkpoint_unlocks, sizeof( gs->unlocks ) );
						gs->midnight = gs->checkpoint_midnight;
					}
				}
				
				if ( TEC(self).blackbar_position == -speed ) {
				
					global.gs.chunk_x = -10;
					global.gs.chunk_y = -10;
					
					gamestate_set_camera(gs,
						TEC(self).transition_x * SCREEN_W, //gs->checkpoint_x * SCREEN_W,
						TEC(self).transition_y * SCREEN_H  //gs->checkpoint_y * SCREEN_H 
					);
						
					//gamestate_set_chunk( gs, gs->checkpoint_x, gs->checkpoint_y );
					gamestate_set_chunk( gs, TEC(self).transition_x, TEC(self).transition_y );
				}
				
				if ( TEC(self).blackbar_position == - 2 * speed ) {
					
					//int id = gs->checkpoint_x + WORLD_SCREENS_W * gs->checkpoint_y;	
					//float player_spawn_x = gs->checkpoint_x * SCREEN_W + SCREEN_W/2;
					//float player_spawn_y = gs->checkpoint_y * SCREEN_H + SCREEN_H/2;
					
					int id = TEC(self).transition_x + WORLD_SCREENS_W * TEC(self).transition_y;
					float player_spawn_x = TEC(self).transition_x * SCREEN_W + SCREEN_W/2;
					float player_spawn_y = TEC(self).transition_y * SCREEN_H + SCREEN_H/2;
					
					//here should search for an object of type save to define spawn position
					clist_iterator it = clist_start( gs->main_object_list );
					while (it != NULL) {
						
						object* obj = clist_get(it, object*);
						if (obj->destroy) { it = clist_next(it); continue; }
						if (obj->type == OBJECT_TYPE_SAVE && obj->origin_chunk_id == id) {
							body2d* orb_body = obj->get_component(obj, COMPONENT_BODY2D);
							if (orb_body) {
								player_spawn_x = orb_body->x;
								player_spawn_y = orb_body->y + 5;
							}
						}
						
						it = clist_next(it);
						
					}
					
					if ( TEC(self).transition_spawn_gecko )
					create_gecko( player_spawn_x, player_spawn_y );
					
				}
				
				if ( TEC(self).blackbar_position <= -BLACKBAR_W ) {
					gamestate_pause( gs, 0 );
					TEC(self).pause_reason = PAUSE_REASON_NONE;
					gs->music_vdelta = 5;
					
					
					
				}
				
			}
		}
		break;
		
		case PAUSE_REASON_TITLE_SCREEN:
		{
			if ( input_pressed( INPUT_START ) ) {
				gamestate_pause( gs, 0 );
				TEC(self).pause_reason = PAUSE_REASON_NONE;
				create_gecko( 2.5 * SCREEN_W, 2 * SCREEN_H - 8 * 3 );
				gamestate_play_sfx( gs, SFX_START);
			}
		}
		break;
		
		case PAUSE_REASON_PLAYER_PAUSE:
		{
			if ( input_pressed( INPUT_START ) ) {
				gamestate_pause( gs, 0 ); 
				TEC(self).pause_reason = PAUSE_REASON_NONE;
			}
			
			//play sfx here 
			
			int8_t prev_settings = TEC(self).settings_cursor;
			
			if ( input_pressed( INPUT_U ) ) { TEC(self).settings_cursor--; }
			if ( input_pressed( INPUT_D ) ) { TEC(self).settings_cursor++; }
			
			TEC(self).settings_cursor = (int8_t) iclamp( TEC(self).settings_cursor, 0, 2 ); 
			
			if ( prev_settings != TEC(self).settings_cursor ) {
				gamestate_play_sfx( gs, SFX_MENU_SELECT );
			}
			
			int* target_setting;
			int  target_range;
			
			if ( TEC(self).settings_cursor == 0 ) {
				target_setting = &gs->settings.music_envelope;
				target_range = VOLUME_ENVELOPE_RANGE;
			}
			
			if ( TEC(self).settings_cursor == 1 ) {
				target_setting = &gs->settings.sfx_envelope;
				target_range = VOLUME_ENVELOPE_RANGE;
			}
			
			if ( TEC(self).settings_cursor == 2 ) {
				target_setting = &gs->settings.heat_envelope;
				target_range = HEAT_ENVELOPE_RANGE;
			}
			
			int prev = *target_setting;
			
			if ( input_pressed( INPUT_L ) ) { (*target_setting)--; }
			if ( input_pressed( INPUT_R ) ) { (*target_setting)++; }
			
			(*target_setting) = iclamp( *target_setting, 0, target_range );
			
			if ( prev > *target_setting ) { gamestate_play_sfx( gs, SFX_JUMP1 ); }
			if ( prev < *target_setting ) { gamestate_play_sfx( gs, SFX_JUMP2 ); }
			
		}
		break;
		
		case PAUSE_REASON_MESSAGE_DELAY:
		{
			gs->message_delay--;
			if ( gs->message_delay <= 0 ) {
				TEC(self).pause_reason = PAUSE_REASON_MESSAGE;
				TEC(self).message_w = 0;
				TEC(self).message_h = 0;
				
			} 
		}
		break;
		
		case PAUSE_REASON_MESSAGE:
		{
			
			
			if ( TEC(self).message_w < MESSAGE_W_MAX ) {
				TEC(self).message_w += 8;
				if ( TEC(self).message_w > MESSAGE_W_MAX ) { TEC(self).message_w = MESSAGE_W_MAX; }
			}
			else {
				TEC(self).message_h += 2;
				if ( TEC(self).message_h > MESSAGE_H_MAX ) { TEC(self).message_h = MESSAGE_H_MAX; }
			}
			
			if ( TEC(self).message_h >= MESSAGE_H_MAX ) {
				
				if ( input_pressed( INPUT_START ) || input_pressed( INPUT_A ) || input_pressed( INPUT_B ) ) {
					//gamestate_pause( gs, 0 ); 
					TEC(self).pause_reason = PAUSE_REASON_MESSAGE_RECOVERY;
				}
				
			}
		}
		break;
		
		case PAUSE_REASON_MESSAGE_RECOVERY:
		{
			TEC(self).message_w *= 0.85;
			TEC(self).message_w -= 2;
			
			if ( TEC(self).message_w <= 0 ) {
				gamestate_pause( gs, 0 ); 
				TEC(self).pause_reason = PAUSE_REASON_NONE;
				gs->music_vdelta = 5;
			}
		}
		break;
		
		case PAUSE_REASON_TELEPORT:
		{
			TEC(self).teleport_left--;
			
			object* player = find_gecko();
			
			if ( player ) {
				
				player->class.gecko.sprite = SPR_PLAYER_JUMP1;
				
				body2d* player_body = player->get_component( player, COMPONENT_BODY2D );
				
				float xspeed, yspeed;
				float angle = 2 * M_PI * uniform();
				float spd = 0.5 + 3 * uniform();
				xspeed = spd * cosf(angle);
				yspeed = spd * sinf(angle);

				bit1cd_sprite* spr = ( irandom(1) == 0 ) ? SPR_BALL0 : SPR_SBALL0;
				//uint8_t colormode = ( irandom(1) == 0 ) ? BIT1CD_COLORMODE_NORMAL : BIT1CD_COLORMODE_INVERT_SOURCE;
				uint8_t colormode = BIT1CD_COLORMODE_NORMAL;
				
				object* p = create_temp_static( player_body->x, player_body->y - 4, xspeed, yspeed, 
					spr, 60 * 60, LAYER_SPR_3,  5,
					0.95, colormode
				);
				
				p->ignore_pause = 1;
			}
			
			if ( TEC(self).teleport_left <= 0 ) {
				
				switch( TEC(self).last_portal_id ) {
					case 0: //boss tp 
						TEC(self).transition_load_inventory = 0;
						TEC(self).transition_x = 8;
						TEC(self).transition_y = 0;
					break;
					
					case 1: //"the end"
						//TEC(self).transition_load_inventory = 0;
						TEC(self).transition_x = 8;
						TEC(self).transition_y = 4;
						
						TEC(self).transition_spawn_gecko = 0;
						
						/*
						gs->checkpoint_x = 2; //initial screen 
						gs->checkpoint_y = 1;
						
						gs->credits = 0;
						
						memset( gs->unlocks, 0, sizeof( gs->unlocks ) );
						memset( gs->checkpoint_unlocks, 0, sizeof( gs->checkpoint_unlocks ) );
						*/
					break;
				}
				
				//gamestate_pause( gs, 0 ); 
				//object* player = find_gecko();
				if ( player ) player->destroy = 1;
				TEC(self).pause_reason = PAUSE_REASON_CAMERA_TRANSITION;
				

				TEC(self).transition_delay = 40;
				//TEC(self).transition_spawn_gecko = 1;
				TEC(self).blackbar_position = SCREEN_W;
				
				
			}
			
		}
		break;
		
		case PAUSE_REASON_BOSS_CUTSCENE:
		{
			
			TEC(self).boss_cutscene_left--;
			if ( TEC(self).boss_cutscene_left <= 0 ) {
				
				TEC(self).pause_reason = PAUSE_REASON_CAMERA_TRANSITION;
				TEC(self).transition_load_inventory = 0;
				TEC(self).transition_x = 8;
				TEC(self).transition_y = 2;
				TEC(self).transition_delay = 2;
				//TEC(self).transition_spawn_gecko = 0;
				TEC(self).blackbar_position = SCREEN_W;
				gs->credits = 1;
			}
			
			//object* moon = object_find( gs, OBJECT_TYPE_MOONGAZER );
			//if (moon) {}
		}
		break;
		
	}
}

static void _tec_step( gamestate* gs, object* self, int step ) {
	
	if (step == STEP_0) {
		
		if ( gs->real_tickcount == 1 ) {
			gamestate_music( gs,  MUSIC_PLAY_OVERWORLD );
		}
		
		if ( gs->credits )                     { gamestate_music(gs, MUSIC_PAUSE ); }
		if ( gs->midnight && gs->chunk_x < 8 ) { gamestate_music(gs, MUSIC_PAUSE ); }
		
		if (
			gs->unlocks[ UNLOCKED_CRYSTAL_0 ] &&
			gs->unlocks[ UNLOCKED_CRYSTAL_1 ] &&
			gs->unlocks[ UNLOCKED_CRYSTAL_2 ] 
		)    { gs->midnight = 1; }
		else { gs->midnight = 0; }
		
		if (TEC(self).pause_reason != PAUSE_REASON_TITLE_SCREEN) {
			if (TEC(self).title_left > 0 && !gs->pause_state) { TEC(self).title_left--; }
		}
		
		int16_t chunk_id = gs->chunk_x + gs->chunk_y * WORLD_SCREENS_W;
		clist_iterator it = clist_start(gs->main_object_list);
		int pipe_in_view = 0;
		while (it != NULL) {
			
			object* obj = clist_get(it, object*);
				
			if ( obj->origin_chunk_id == chunk_id && obj->type == OBJECT_TYPE_PIPE ) {
				pipe_in_view = 1;
				break;
			}
			
			it = clist_next(it);
		}
		
		if (pipe_in_view) { gs->app_signal.waterfall_volume += 0.07; }
		else              { gs->app_signal.waterfall_volume -= 0.07; }
		
		gs->app_signal.waterfall_volume = fclamp( gs->app_signal.waterfall_volume, 0, 1);
		//printf("waterfall volume: %f\n", (float)gs->app_signal.waterfall_volume );
		
		_tec_process_pause( gs, self );
		_tec_check_pause  ( gs, self );

		if (gs->pause_state) return;
		
		object* player = find_gecko();
		body2d* player_body = NULL;
		
		if (player) { player_body = player->get_component( player, COMPONENT_BODY2D ); }
		
		if (player_body) {
			
			int8_t chunk_x = (int8_t)floorf(player_body->x / SCREEN_W);
			int8_t chunk_y = (int8_t)floorf(player_body->y / SCREEN_H);
			
			int16_t target_cam_x = chunk_x * SCREEN_W;
			int16_t target_cam_y = chunk_y * SCREEN_H;
			
			float slower = 10;
			
			float cam_dx = (float)( target_cam_x - global.gs.cam_x ) / slower;
			float cam_dy = (float)( target_cam_y - global.gs.cam_y ) / slower;

			if ( cam_dx != 0 && fabsf(cam_dx) < 2.0 ) { cam_dx = 2.0 * fsign(cam_dx); }
			if ( cam_dy != 0 && fabsf(cam_dy) < 2.0 ) { cam_dy = 2.0 * fsign(cam_dy); }
			
			int16_t new_cam_x = global.gs.cam_x + cam_dx;
			int16_t new_cam_y = global.gs.cam_y + cam_dy;
			
			//global.gs.cam_x += cam_dx;
			//global.gs.cam_y += cam_dy;
			
			//if camera was overshot, set to position 
			float new_dx = fsign( (float)( target_cam_x - new_cam_x ) );
			if (new_dx != fsign(cam_dx)) { new_cam_x = target_cam_x; }
			
			float new_dy = fsign( (float)( target_cam_y - new_cam_y ) );
			if (new_dy != fsign(cam_dy)) { new_cam_y = target_cam_y; }
			
			gamestate_set_camera(gs,
				new_cam_x, new_cam_y );
				
			gamestate_set_chunk( gs, chunk_x, chunk_y );
		}
	}
}

static void _draw_title( gamestate* gs, int16_t x, int16_t y ) {
	
	draw_sprite_relative( global.main_canvas,
		x - gs->cam_x, y - gs->cam_y,
		0,0,0, SPR_TITLE, 8 
	);		
	
	bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_INVERT );
	
	draw_text( global.main_canvas, x + SCREEN_W/2 - 50 - gs->cam_x, y - 8 + (SCREEN_H - 8 * 8) + 1 - gs->cam_y , "LITE" );	
}

static void _tec_draw( gamestate* gs, object* self, int layer ) {
	
	switch(layer) {
		
		case LAYER_SPR_0:
		{
			if (
				( gs->chunk_x == 8 ) && 
				( gs->chunk_y == 2 || gs->chunk_y == 3 ) 
			) {
				
				for (int k = 0; k < 2; k++) {
				
					int16_t xx, yy;
					const char* str;
					const char* str1 = "THANK YOU FOR\nPLAYING";
					const char* str2 = "A GAME BY\nMATEO F. B.";
					
					if ( k > 0 ) {
						str = str1;
						xx = 8 * SCREEN_W + 12 - gs->cam_x;
						yy = 2 * SCREEN_H + 28 - gs->cam_y;
					}
					else {
						str = str2;
						xx = 8 * SCREEN_W + 12 - gs->cam_x;
						yy = 3 * SCREEN_H + 28 - gs->cam_y;
					}
					
					bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_INVERT );
					draw_text( global.main_canvas, xx, yy, str );
					bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_NORMAL );
				}
			}
			
			if (
				( gs->chunk_x == 8 ) && 
				( gs->chunk_y == 4 ) 
			) {
				
				int16_t xx = SCREEN_W * 8 + SCREEN_W / 2;
				int16_t yy = SCREEN_H * 4 + 8;
				
				_draw_title( gs, xx, yy );
				
				bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_INVERT );
				draw_text_centered  ( global.main_canvas, xx - gs->cam_x, yy + 100 - gs->cam_y, "THE END" );
				bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_NORMAL );
			}
		}
		break;
		
		case LAYER_SPR_2:
		{
			
			if (TEC(self).title_left > 0) {
				
				/*
				draw_sprite_relative( global.main_canvas,
					SCREEN_W/2, 8 - gs->cam_y,
					0,0,0, SPR_TITLE, 8 
				);		
				
				bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_INVERT );
				
				draw_text( global.main_canvas, SCREEN_W - 50, (SCREEN_H - 8 * 8) - gs->cam_y , "LITE" );
				*/
				
				_draw_title( gs, SCREEN_W * 2 + SCREEN_W/2, 8 );
				
				draw_text_centered(
					global.main_canvas,
					SCREEN_W / 2,
					(SCREEN_H - 8 * 6) - gs->cam_y  + 1 * (( global.gs.real_tickcount / 25) % 2 )  ,
					"PRESS START"
				);
				
				bit1cd_set_colormode( global.main_canvas, BIT1CD_COLORMODE_NORMAL );
				
			}
		}
		break;
		
		
		case LAYER_TEC_0:
		{
			bit1cd_draw_sprite( global.main_canvas, 0,0,0,0,
				0, &global.shadow_canvas->buffer );
		}
		break;
		
		
		case LAYER_TEC_1:
		{
			if ( TEC(self).pause_reason == PAUSE_REASON_CAMERA_TRANSITION ) {
				
				bit1cd_draw_rect( 
					global.main_canvas,
					TEC(self).blackbar_position, 0, BLACKBAR_W, SCREEN_H, BIT1CD_COLOR_DARK
				);
			}
			
			//apply heat effect before hud 
			float yheat_envelope = 1.0;
			float xheat_envelope = 1.0;
			float heat = gs->heat_intensity;
			
			if ( gs->settings.heat_envelope < 2 ) { yheat_envelope = 0.0; }
			if ( gs->settings.heat_envelope < 1 ) { xheat_envelope = 0.0; }
			
			if (heat > 0 && (!gs->midnight) ) {
			
				for (int scanline = 0; scanline < SCREEN_H; scanline++) {
					
					int16_t base_t = global.gs.tickcount + global.gs.cam_y;
					
					int64_t t1 = (scanline * 4) + ( base_t * 1);
					int64_t t2 = base_t + scanline;
					
					float angle1 = M_PI * (float)( ( t1     ) % 360) / 180.0;
					float angle2 = M_PI * (float)( ( t2 * 3 ) % 360) / 180.0;
					
					int xoffset = xheat_envelope * roundf(heat * 1.0 * cosf(angle1));
					int yoffset = yheat_envelope * roundf(heat * 1.0 * sinf(angle2));
					
					bit1cd_draw_sprite_part( global.heat_canvas, xoffset, 0, 0, yoffset,
					0, iclamp(scanline + yoffset, 0, SCREEN_H - 1), SCREEN_W, 1, &global.main_canvas->buffer );
					
				}
				
				//bit1cd_draw_sprite_part(bit1cd_canvas* cv, int16_t x, int16_t y, int16_t xoffset, int16_t yoffset,
				//int16_t xsection, int16_t ysection, int16_t wsection, int16_t hsection, bit1cd_sprite* sprite);
				
				//bit1cd_draw_rect( global.main_canvas, 0,0, SCREEN_W, SCREEN_H, BIT1CD_COLOR_DARK );
				
				bit1cd_draw_sprite( global.main_canvas, 0,0,0,0,
					0, &global.heat_canvas->buffer );
			}
				
			if ( !gs->unlocks[ UNLOCKED_LANTERN ] && gs->centered_in_shadow ) {
				
				object* player = find_gecko();
				body2d* player_body = NULL;
				
				if (player) { player_body = player->get_component( player, COMPONENT_BODY2D ); }
				
				if (player_body) {
					draw_gecko_body( gs, player );
				}
			}
				
			//hud should go here 
			
			if ( TEC(self).pause_reason == PAUSE_REASON_PLAYER_PAUSE ) {
				
				char txt[200];
				strcpy(txt, "PAUSED");
				int16_t w = strlen(txt)*8 + 4;
				int16_t y = 8;
				int16_t h = 12;
				
				_draw_rect_outline( 
					global.main_canvas,
					SCREEN_W/2 - w/2,
					y - 2,
					w, h
				);
				
				draw_text_centered( global.main_canvas, SCREEN_W/2, y, txt );
				
				int16_t y2 = 40;
				
				int16_t relic_center_x = SCREEN_W / 4;
				strcpy(txt, "RELICS");
				w = strlen(txt)*8 + 4;
				y = y2 - 14;
				
				_draw_rect_outline( 
					global.main_canvas,
					relic_center_x - w/2,
					y - 2,
					w, h
				);
				
				draw_text_centered( global.main_canvas, relic_center_x, y, txt );
				
				int16_t w2 = (8+4) * 6 + 4;
				int16_t relic_y = y2 + 0;
				int16_t relic_h = 14;
				
				_draw_rect_outline( 
					global.main_canvas,
					relic_center_x - w2/2,
					relic_y,
					w2, relic_h
				);
				
				bit1cd_sprite* _ITEM_SPRITES[] = {
					SPR_CRYSTAL0,
					SPR_CRYSTAL0,
					SPR_CRYSTAL0,
					SPR_FEATHER , 
					SPR_FIRE    ,    
					SPR_LANTERN   
				};
				
				for (int16_t item = UNLOCKED_CRYSTAL_0; item < UNLOCKED_MAX; item++) {
					
					if (!gs->unlocks[item]) continue;
					
					draw_sprite_relative( global.main_canvas,
						relic_center_x - w2/2 + 2 + 12*item,
						relic_y + 2,
						0,0,0, _ITEM_SPRITES[item], 7 
					);
					
				}

				int16_t map_center_x = 3 * SCREEN_W / 4;
				strcpy(txt, "LOCATION");
				w = strlen(txt)*8 + 4;				
				
				_draw_rect_outline( 
					global.main_canvas,
					map_center_x - w/2,
					y - 2,
					w, h
				);
				
				draw_text_centered( global.main_canvas, map_center_x, y, txt );
				
				draw_sprite_relative( global.main_canvas, map_center_x, y2, 0,0,0, SPR_MINIMAP, 8 );
				int16_t map_corner_x = map_center_x - 8*4;
				
				for (int cx = 0; cx < WORLD_SCREENS_W - 1; cx++)
				for (int cy = 0; cy < WORLD_SCREENS_H    ; cy++) {
					int cid = cx + WORLD_SCREENS_W * cy;
					if ( !gs->known_mapchunks[ cid ] ) {
						bit1cd_draw_rect( 
							global.main_canvas,
							map_corner_x + 8*cx,
							y2           + 8*cy, 
							8,8, BIT1CD_COLOR_DARK
						);
					}
					
					int has_checkpoint = gs->checkpoint_locations[ cid ];
					int is_center = (cx == gs->chunk_x && cy == gs->chunk_y) ? 1 : 0;
					if (is_center) has_checkpoint = 0; //flicker overwrites static checkpoint mark 
					
					if ( has_checkpoint || ( is_center && (gs->real_tickcount % 20) < 10 )  ) {
						
						bit1cd_draw_rect( 
							global.main_canvas,
							map_corner_x + 8*cx + 3,
							y2           + 8*cy + 3, 
							2,2, BIT1CD_COLOR_DARK
						);
						
					}
				}
				
				
				int16_t settings_center_x = SCREEN_W / 4;
				strcpy(txt, "SETTINGS");
				w = strlen(txt)*8 + 4;
				y = y2 + 8*4;
				
				_draw_rect_outline( 
					global.main_canvas,
					settings_center_x - w/2,
					y - 2,
					w, h
				);
				
				draw_text_centered( global.main_canvas, settings_center_x, y, txt );
				
				//settings black bg
				
				int16_t bg_w = 8*9;
				int16_t bg_h = 8*7;
				
				_draw_rect_outline( 
					global.main_canvas,
					settings_center_x - bg_w/2,
					y + 14,
					bg_w, bg_h
				);
				
				const char* _SETTINGS_NAMES[] = {
					"MUSIC",
					"SFX",
					"HEAT"
				};
				
				int16_t settings_ystart = y + 21;
				int16_t settings_xleft = 12;
				int16_t settings_xright = settings_xleft + 8*7 - 2;
				int16_t settings_yoffset = 17;
				
				for (int k = 0; k < 3; k++) {
					strcpy( txt, _SETTINGS_NAMES[k] );
					
					draw_text( global.main_canvas,
						settings_xleft, settings_ystart + settings_yoffset * k, txt);
						
					if ( k == TEC(self).settings_cursor ) {
						bit1cd_draw_sprite( global.main_canvas,
							settings_xleft - 8, settings_ystart + settings_yoffset * k,
							0,0,0, SPR_MARKER
						);
					}
					
					int v;
					if (k == 0) v =	gs->settings.music_envelope; 
					if (k == 1)	v = gs->settings.sfx_envelope;
					if (k == 2) v = gs->settings.heat_envelope;
					
					sprintf(txt, "%d", v);
					draw_text_centered( global.main_canvas, 
						settings_xright, settings_ystart + settings_yoffset * k, txt );
					
				}
			}
			
			if ( TEC(self).pause_reason == PAUSE_REASON_MESSAGE || TEC(self).pause_reason == PAUSE_REASON_MESSAGE_RECOVERY ) {
				
				int16_t rect_x = SCREEN_W/2 - TEC(self).message_w / 2;
				int16_t rect_y = SCREEN_H/2 - MESSAGE_H_MAX / 2;
				
				for (int k = 0; k < 2; k++) {
				
					int dx = (k == 0) ? -1:0;
					int dy = (k == 0) ? -1:0;
					uint8_t color = (k == 0) ? BIT1CD_COLOR_LIGHT : BIT1CD_COLOR_DARK;
					int16_t extra_w = (k == 0) ? 2 : 0;
					int16_t extra_h = (k == 0) ? 2 : 0;
				
					bit1cd_draw_rect( 
						global.main_canvas,
						rect_x + dx, rect_y + dy,
						TEC(self).message_w + extra_w,
						MESSAGE_H_MAX + extra_h,
						color
					);
				}
				
				if ( TEC(self).pause_reason == PAUSE_REASON_MESSAGE ) {
				
					if ( TEC(self).message_w >= MESSAGE_W_MAX ) {
						draw_text_centered( global.main_canvas, SCREEN_W/2, SCREEN_H/2 - 20, gs->message_title );
						draw_text( global.main_canvas, 8 + 4, SCREEN_H/2 - 6, gs->message );
					}
					
					//if (0)
					bit1cd_draw_rect( 
						global.main_canvas,
						rect_x, rect_y + TEC(self).message_h,
						TEC(self).message_w,
						iclamp(MESSAGE_H_MAX - TEC(self).message_h, 0, 9999),
						BIT1CD_COLOR_DARK
					);
				}
			}
			
		}
		break;
		
	}
}

void create_tec() {
	
	object* self = object_new( OBJECT_LIST_SUB );
	self->type = OBJECT_TYPE_TEC;
	self->step = _tec_step;
	self->draw = _tec_draw;
	self->ignore_pause = 1;
	TEC(self).pause_reason = PAUSE_REASON_TITLE_SCREEN;
	TEC(self).title_left = 20;
	TEC(self).teleport_left = 0;
	TEC(self).transition_spawn_gecko = 1;
	TEC(self).settings_cursor = 0;
}