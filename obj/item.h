#ifndef _OBJ_ITEM_H
#define _OBJ_ITEM_H

#include "../gecko_object.h"

object* create_item(float x, float y, uint8_t node_data);

#endif