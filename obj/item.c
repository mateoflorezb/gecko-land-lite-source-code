#include "item.h"
#include "temp.h"
#include "../gecko_sprite.h"
#include "../gecko_global.h"
#include "../gecko_draw.h"
#include "../gecko_misc.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

#define ITEM(self) (self->class.item)

static void* _item_component( object* self, int component ) {
	
	if ( component == COMPONENT_BODY2D ) 
		return &ITEM(self).body;
	
	return NULL;
}

static void _item_step( gamestate* gs, object* self, int step ) {
	
	if ( step != STEP_0 ) return;
	
	body2d* body = &ITEM(self).body;
	
	float yoffset = 2.0*sinf( M_PI * (float)( ( 3*gs->tickcount ) % 360) / 180.0 );
	yoffset = fclamp(yoffset, -1, 1);
	body->y = roundf(ITEM(self).origin_y + yoffset);
	
	if ( ITEM(self).destroy_next_frame ) { self->destroy = 1; return; }
	
	if ( body2d_place_meeting( body, body->x, body->y, OBJECT_TYPE_GECKO ) ) {
		
		ITEM(self).destroy_next_frame = 1;
		gs->unlocks[ ITEM(self).unlocks ] = 1;
		
		char title[200];
		char message[200];
		
		
		switch( ITEM(self).unlocks ) {
			
			case UNLOCKED_CRYSTAL_0:
			case UNLOCKED_CRYSTAL_1:
			case UNLOCKED_CRYSTAL_2:
			{
				strcpy(title,"CRYSTAL");
				int ncrystals = 0;
				for (int k = UNLOCKED_CRYSTAL_0; k <= UNLOCKED_CRYSTAL_2; k++) {
					if ( gs->unlocks[ k ] ) { ncrystals++; }
				}
				
				if (ncrystals < 3) {
					sprintf(message, "%d out of 3", ncrystals);
				}
				else {
					strcpy(message,"THE MOON GAZER\nAWAITS...");
				}
			}	
			break;
			
			case UNLOCKED_FEATHER:
				strcpy(title, "SPIRIT FEATHER");
				strcpy(message, "ALLOWS TO JUMP IN\nMID AIR.");
			break;
			
			case UNLOCKED_FIRE:
				strcpy(title, "FIREBALL");
				strcpy(message, "PRESS X TO SHOOT.\nDESTROYS ROCKS\nAND ENEMIES.");
			break;
			
			case UNLOCKED_LANTERN:
				strcpy(title, "MAGIC LANTERN");
				strcpy(message, "ILLUMINATES DARK\nPLACES.");
			break;
			
		}
		
		//gamestate_set_message( gs, title, message, 60 );
		gamestate_set_message( gs, title, message, 10 );
		gamestate_play_sfx(gs, SFX_ITEMGET);
		
		return;
	}
	
	if (( gs->tickcount % 3 ) == 0 ) {
		
		float spd = 0.5 + 0.5*uniform();
		float angle = M_PI * (float)irandom(360) / 180.0;
		float xspeed = spd * cosf(angle);
		float yspeed = spd * sinf(angle);
		
		create_temp_static(
			body->x + rand_sign() * irandom(2),
			body->y + rand_sign() * irandom(2),
			xspeed, yspeed, 
			irandom(1) ? SPR_SBALL0 : SPR_TBALL0,
			18 + irandom(6), LAYER_SPR_0, 5,
			0.96, BIT1CD_COLORMODE_NORMAL
		);
	}
}

static void _item_draw( gamestate* gs, object* self, int layer ) {
	
	if (layer != LAYER_SPR_1) return;
	
	int16_t xx = (int16_t) floorf( ITEM(self).body.x ) - gs->cam_x;
	int16_t yy = (int16_t) floorf( ITEM(self).body.y ) - gs->cam_y;
	draw_big_light( global.shadow_canvas, xx, yy );
	
	draw_sprite_relative( global.main_canvas,
		xx,yy,
		0,0,0, ITEM(self).sprite, 5
	);
	
}

object* create_item(float x, float y, uint8_t node_data) {
		
	node_data = (uint8_t) iclamp( (int)node_data, UNLOCKED_CRYSTAL_0, UNLOCKED_MAX - 1 );
		
	if ( global.gs.unlocks[ node_data ] ) return NULL;

	object* self = object_new( OBJECT_LIST_MAIN );
	self->type = OBJECT_TYPE_ITEM;
	self->step = _item_step;
	self->draw = _item_draw;
	self->get_component = _item_component;

	switch(node_data) {
		
		case UNLOCKED_CRYSTAL_0:
		case UNLOCKED_CRYSTAL_1:
		case UNLOCKED_CRYSTAL_2:
			ITEM(self).sprite = SPR_CRYSTAL0;
		break;
		
		case UNLOCKED_FEATHER: ITEM(self).sprite = SPR_FEATHER; break;
		case UNLOCKED_FIRE   : ITEM(self).sprite = SPR_FIRE   ; break;
		case UNLOCKED_LANTERN: ITEM(self).sprite = SPR_LANTERN; break;
		
	}
	
	ITEM(self).origin_y = y;
	ITEM(self).destroy_next_frame = 0;
	body2d_initialize( &ITEM(self).body, x, y, 8,8,4,4 );
	
	ITEM(self).unlocks = node_data;
	
	return self;
}