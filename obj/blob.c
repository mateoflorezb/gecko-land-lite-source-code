#include "blob.h"
#include "water.h"
#include "../gecko_sprite.h"
#include "../gecko_global.h"
#include "../gecko_draw.h"
#include "../gecko_animation.h"
#include "../gecko_misc.h"
#include "../gecko_magic.h"
#include <math.h>
#include <string.h>

#define BLOB(self) (self->class.blob)

static void* _blob_component( object* self, int component ) {
	
	switch(component) {
		
		case COMPONENT_BODY2D:    return &BLOB(self).body;
		case COMPONENT_ENEMYDATA: return &BLOB(self).enemy;
	}
	
	return NULL;
}

static void _blob_step( gamestate* gs, object* self, int step ) {
	
	body2d* body = &BLOB(self).body;
	
	switch( step ) {
		
		case STEP_1: 
		{
			
			if (BLOB(self).enemy.hp <= 0) { enemy_generic_death( gs, self, body->x, body->y - 4 ); }
			if (gs->midnight) { self->destroy = 1; return; }
			
			body2d camera_area;
			camera_area.x = gs->cam_x + 1;
			camera_area.y = gs->cam_y + 1;
			camera_area.w = SCREEN_W  - 2;
			camera_area.h = SCREEN_H  - 2;
			camera_area.xoffset = 0;
			camera_area.yoffset = 0;
			
			body2d spawn_area;
			memcpy( &spawn_area, body, sizeof(body2d) );
			spawn_area.x = BLOB(self).origin_x;
			spawn_area.y = BLOB(self).origin_y;
			
			if ( !body2d_1on1_check( &spawn_area, &camera_area ) ) {
				
				BLOB(self).jump_delay = 30 + irandom(30);
				BLOB(self).water_signal_delay = 0;
				body->x = BLOB(self).origin_x;
				body->y = BLOB(self).origin_y;
				body->xspeed = 0;
				body->yspeed = 0;
				
				return;
				
			}
			
			if (BLOB(self).water_signal_delay > 0) { BLOB(self).water_signal_delay--; }
			
			//signal water 
			if ( BLOB(self).water_signal_delay <= 0 ) {
				if ( generic_water_interaction(body) ) { BLOB(self).water_signal_delay = 5; }
			}
			
			int inAir = 1;
			float grav = 0.05;
			float max_vspeed = 1;
			
			if ( body2d_place_meeting( body, body->x, body->y, OBJECT_TYPE_WATER ) ) {
				grav /= 2;
				max_vspeed = 0.5;
			}

			if (
				 body2d_place_meeting( body, body->x, body->y + 1.0, OBJECT_TYPE_BLOCK ) ||
				(
					 body->yspeed == 0 &&
					 body2d_place_meeting( body, body->x, body->y + 1.0, OBJECT_TYPE_PLATFORM ) &&
					!body2d_place_meeting( body, body->x, body->y,       OBJECT_TYPE_PLATFORM )
				)
			) {
				inAir = 0;
			}
			
			if ( inAir ) {
				body->yspeed += grav;
			}
			else {
				body->xspeed = 0;
			}
			
			if ( !inAir ) {
				if (BLOB(self).jump_delay > 0) { BLOB(self).jump_delay--; }
			}
			
			if ( !inAir && BLOB(self).jump_delay <= 0 ) {
				body->yspeed = -1.5;
				body->xspeed = rand_sign() * (0.5 + 0.5 * uniform() );
				BLOB(self).jump_delay = 50 + irandom(20);
				gamestate_play_sfx(gs, SFX_BLOBJUMP);
				
			}
			
			BLOB(self).sprite = ( body->yspeed < 0 ) ? SPR_BLOB0 : SPR_BLOB1;
			
		}
		break;
		
		case STEP_2: 
		{
			body2d_solid_col(body); 
			body2d_platform_col(body);
			
			body->x += body->xspeed;
			body->y += body->yspeed;
		}
		break;
		
	}
}

static void _blob_draw( gamestate* gs, object* self, int layer ) {
	
	if (layer == LAYER_SPR_1) {

		draw_small_light( global.shadow_canvas,
			(int16_t)floorf ( BLOB(self).body.x ) - gs->cam_x,
			(int16_t)floorf ( BLOB(self).body.y ) - gs->cam_y - 4
		);
	}
	
	if (layer == LAYER_SPR_2) {
	
		draw_sprite_relative( global.main_canvas,
			(int16_t) floorf( BLOB(self).body.x ) - gs->cam_x,
			(int16_t) floorf( BLOB(self).body.y ) - gs->cam_y,
			0,0,0, BLOB(self).sprite, 2
		);
		
	}
}

object* create_blob(float x, float y) {
	
	object* self = object_new( OBJECT_LIST_MAIN );
	self->step = _blob_step;
	self->draw = _blob_draw;
	self->get_component = _blob_component;
	
	body2d_initialize( &BLOB(self).body, x, y, 8, 8, 4, 8);
	enemy_initialize ( &BLOB(self).enemy, 1, 0, 1);
	
	BLOB(self).sprite = SPR_BLOB1;
	BLOB(self).jump_delay = 30 + irandom(30);
	BLOB(self).origin_x = x;
	BLOB(self).origin_y = y;
	BLOB(self).water_signal_delay = 0;
	
	return self;
}