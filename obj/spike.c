#include "spike.h"
#include "../gecko_sprite.h"
#include "../gecko_global.h"
#include "../gecko_draw.h"
#include <math.h>

#define SPIKE(self) (self->class.spike)

static void* _spike_component( object* self, int component ) {
	
	switch(component) {
		
		case COMPONENT_BODY2D:    return &SPIKE(self).body;
		case COMPONENT_ENEMYDATA: return &SPIKE(self).enemy;
	}
	
	return NULL;
}

static void _spike_draw( gamestate* gs, object* self, int layer ) {
	
	if ( layer == LAYER_SPR_1 || layer == LAYER_SPR_0 ) {
	
		bit1cd_sprite* spr = SPIKE(self).sprite;
		
		if ( layer == LAYER_SPR_0 ) {
			spr = (spr == SPR_SPIKE_H) ? SPR_SPIKE_H_OUTLINE : SPR_SPIKE_V_OUTLINE;
		}
	
		draw_sprite_relative( global.main_canvas,
			(int16_t)floorf( SPIKE(self).body.x ) - gs->cam_x,
			(int16_t)floorf( SPIKE(self).body.y ) - gs->cam_y,
			0,0, SPIKE(self).flip_flags, spr, 5
		);
	}
	
}

object* create_spike(float x, float y, uint8_t node_data) {
	
	object* self = object_new( OBJECT_LIST_MAIN );
	self->draw = _spike_draw;
	self->get_component = _spike_component;
	
	SPIKE(self).flip_flags = 0;
	
	float w,h;
	int8_t xoffset, yoffset;
	
	w = 8;
	h = 6;
	xoffset = 4;
	yoffset = 3;
	
	//horizontal spike 
	if (node_data == 0 || node_data == 2) {
		
		if (node_data == 2) { SPIKE(self).flip_flags = BIT1CD_FLIP_HORIZONTAL; }
		SPIKE(self).sprite = SPR_SPIKE_H;
	}
	//vertical spike 
	else {
		
		if (node_data == 1) { SPIKE(self).flip_flags = BIT1CD_FLIP_VERTICAL; }
		SPIKE(self).sprite = SPR_SPIKE_V;
	}
	
	body2d_initialize( &SPIKE(self).body, x + 4, y + 4, w, h, xoffset, yoffset);
	enemy_initialize ( &SPIKE(self).enemy, 1, 1, 1);
	
	return self;
}