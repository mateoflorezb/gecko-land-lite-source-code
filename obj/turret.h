#ifndef _OBJ_TURRET_H
#define _OBJ_TURRET_H

#include "../gecko_object.h"

object* create_turret(float x, float y, uint8_t node_data);

#endif 