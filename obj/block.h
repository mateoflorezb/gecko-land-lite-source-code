#ifndef _OBJ_BLOCK_H
#define _OBJ_BLOCK_H

#include "../gecko_object.h"

object* create_block   (float x, float y, float w, float h);
object* create_platform(float x, float y, float w, float h);

#endif 