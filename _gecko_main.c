#include "bit1cd.h"
#include "dhex_window_app.h"
#include "bit_to_dhex.h"
#include "gecko_global.h"
#include "gecko_magic.h"
#include "gecko_misc.h"
#include "gecko_sprite.h"
#include "gecko_input.h"
#include "gecko_gamestate.h"
#include "gecko_world.h"
#include "gecko_enemy.h"
#include "gecko_animation.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

//temp 
//#include "obj/gecko.h"
#include <math.h>

#define MICROS_PER_FRAME (16667*1)
#define MICROS_BEFORE_REFRESH ((3 * MICROS_PER_FRAME) / 5)

static dhex_rgba LIGHT_COLORS[ PALETTE_MAX ] = {
	
	(dhex_rgba) {155, 188,  15, 255}, //base
	(dhex_rgba) {  0, 202, 152, 255}, //water
	(dhex_rgba) {255, 233,   0, 255}, //fire
	(dhex_rgba) {170, 120,  90, 255}, //cave 
	(dhex_rgba) {200, 200, 165, 255}  //end 
	//(dhex_rgba) {  0, 230, 210, 255}  //end 

};

static dhex_rgba DARK_COLORS[ PALETTE_MAX ] = {
	
	(dhex_rgba) { 15,  56,  15, 255}, //base  
	(dhex_rgba) {  0,  60,  41, 255}, //water
	(dhex_rgba) { 50,   0,  95, 255}, //fire
	(dhex_rgba) {  0,   0,   0, 255}, //cave 
	(dhex_rgba) { 24,  24,  24, 255}  //end 
	//(dhex_rgba) {  0,   0,   0, 255}  //end 

};

static dhex_rgba current_light;
static dhex_rgba current_dark;

static inline uint8_t approach_byte( uint8_t current, uint8_t target ) {
	
	if ( current == target ) return target;
	
	int difference = (int)target - (int)current;
	int delta = difference / 12;
	if ( delta == 0 ) {
		delta = isign(difference) * 2;
	} 
	
	uint8_t new = (uint8_t)iclamp ( (int)current + delta, 0, 255 ); 
	
	int new_difference = (int)target - (int)new;
	
	if (isign(new_difference) != isign(difference)) new = target;
	
	return new;
	
}

static dhex_rgba approach_color(dhex_rgba current, dhex_rgba target) {
	
	current.r = approach_byte( current.r, target.r );
	current.g = approach_byte( current.g, target.g );
	current.b = approach_byte( current.b, target.b );
	
	return current;
	
}

static void process_sound(void** sfx_list, void* waterfall) {
	
	for (int k = 0; k < global.gs.app_signal.n_sfx; k++) {
		
		int sound_id = global.gs.app_signal.queued_sfx[k];
		if (sound_id < 0) continue;
		void* sfx = sfx_list[ sound_id ];
		if (!sfx) continue;
		
		dhex_sfx_play( -1, sfx, 0);
		
	}
	
	dhex_sfx_volume( waterfall, global.gs.app_signal.waterfall_volume * 127 );
	
	float volume_ptg = (float) global.gs.settings.sfx_envelope / (float) VOLUME_ENVELOPE_RANGE;
	volume_ptg *= 0.7; //sfx are too damn loud lmao 
	int volume = iclamp( volume_ptg * 127, 0, 127 );
	
	dhex_sfx_channel_volume( -1, volume); //channel from 0 to 7, volume from 0 to 128. if less than 0, volume won't be set
}

static void process_music(void* overworld, void* battle ) {
	
	switch( global.gs.app_signal.music_action ) {
		
		case MUSIC_NOP: break;
		case MUSIC_PLAY_OVERWORLD: dhex_music_play( overworld, -1 ); break;
		case MUSIC_PLAY_BOSS:      dhex_music_play( battle,    -1 ); break;
	}
	
	float volume_ptg = (float) global.gs.settings.music_envelope / (float) VOLUME_ENVELOPE_RANGE;
	volume_ptg *= 1.1;
	int volume = iclamp( global.gs.app_signal.music_volume * volume_ptg, 0, 128 );
	dhex_music_volume(volume);
}

int main () {
	
	srand( time(NULL) );
	
	init_sprites();
	init_animation();
	init_world();
	init_global();
	input_init();
	
	current_light = LIGHT_COLORS[ global.gs.app_signal.current_palette ];
	current_dark  =  DARK_COLORS[ global.gs.app_signal.current_palette ];
	
	
	void* sfx_list[ SFX_MAX ] = { NULL };
	sfx_list[ SFX_JUMP1       ] = dhex_load_sfx("snd/jump1.ogg");
	sfx_list[ SFX_JUMP2       ] = dhex_load_sfx("snd/jump2.ogg");
	sfx_list[ SFX_BREAK       ] = dhex_load_sfx("snd/break.ogg");
	sfx_list[ SFX_SHOOT       ] = dhex_load_sfx("snd/shoot.ogg");
	sfx_list[ SFX_BLOBJUMP    ] = dhex_load_sfx("snd/blobjump.ogg");
	sfx_list[ SFX_DEATH       ] = dhex_load_sfx("snd/death.ogg");
	sfx_list[ SFX_ROAR        ] = dhex_load_sfx("snd/roar.ogg");
	sfx_list[ SFX_FULLROAR    ] = dhex_load_sfx("snd/fullroar.ogg");
	sfx_list[ SFX_ITEMGET     ] = dhex_load_sfx("snd/itemget.ogg");
	sfx_list[ SFX_TELEPORT    ] = dhex_load_sfx("snd/teleport.ogg");
	sfx_list[ SFX_WATER       ] = dhex_load_sfx("snd/water.ogg");
	sfx_list[ SFX_SAVE        ] = dhex_load_sfx("snd/save.ogg");
	sfx_list[ SFX_TAP         ] = dhex_load_sfx("snd/tap.ogg");
	sfx_list[ SFX_FIRE        ] = dhex_load_sfx("snd/fire.ogg");
	sfx_list[ SFX_MENU        ] = dhex_load_sfx("snd/menu.ogg");
	sfx_list[ SFX_START       ] = dhex_load_sfx("snd/start.ogg");
	sfx_list[ SFX_ORB         ] = dhex_load_sfx("snd/orb.ogg");
	sfx_list[ SFX_ENEMYHIT    ] = dhex_load_sfx("snd/enemyhit.ogg");
	sfx_list[ SFX_MENU_SELECT ] = dhex_load_sfx("snd/menuselect.ogg");
	
	void* waterfall = dhex_load_sfx("snd/waterfall.ogg");
	
	void* overworld = dhex_load_music("snd/overworld.ogg");
	void* battle    = dhex_load_music("snd/battle.ogg");
	
	dhex_sfx_loop(0, waterfall, -1);
	
	//OG GB
	//dhex_rgba  dark_color = (dhex_rgba) { 15,  56, 15, 255};
	//dhex_rgba light_color = (dhex_rgba) {155, 188, 15, 255};
	
	//blue white 
	//dhex_rgba  dark_color = (dhex_rgba) { 40,  40, 180, 255};
	//dhex_rgba light_color = (dhex_rgba) {200, 200, 220, 255};
	
	//gb light 
	//dhex_rgba  dark_color = (dhex_rgba) {  0,  60,  41, 255};
	//dhex_rgba light_color = (dhex_rgba) {  0, 202, 152, 255};
	
	//gb pocket 
	//dhex_rgba  dark_color = (dhex_rgba) { 24,  24,  24, 255};
	//dhex_rgba light_color = (dhex_rgba) {200, 200, 165, 255};
	
	//strong green
	//dhex_rgba  dark_color = (dhex_rgba) { 30,  30,  30, 255};
	//dhex_rgba light_color = (dhex_rgba) {  0, 200,  80, 255};
	
	//create_gecko(300,20);
	
	//float t = 0;
	
	while (global.app->quit == 0) {
		dhex_set_timer();
		dhex_refresh_app(global.app);
		
		/*
		t += 0.05;
		dhex_rgba light_color = (dhex_rgba) {
			127 + 127 * fabs(sin(t / 10.0)),
			127 + 127 * cos(t / 10.0),
			127 + 127 * fabs(sin(t / 20.0)),
			255
		};
		*/
		
		gamestate_step( &global.gs );
		gamestate_draw( &global.gs );
		
		current_light = approach_color ( current_light, LIGHT_COLORS[ global.gs.app_signal.current_palette ] );
		current_dark  = approach_color ( current_dark,   DARK_COLORS[ global.gs.app_signal.current_palette ] );
		
		bit_to_dhex( current_dark, current_light, &global.main_canvas->buffer, &global.app->main_canvas->pixelbuffer );
		
		process_sound( sfx_list, waterfall );
		process_music( overworld, battle );
		
		//TEMP SCREENSHOTS
		/*
		char ssname[200];
		if ( global.app->input.mouse_pressed ) {
			sprintf( ssname, "frame%d.png", (int)global.gs.tickcount );
			dhex_dump_image_png( ssname, &global.app->main_canvas->pixelbuffer );
		}
		*/
		
		dhex_wait_timer( MICROS_BEFORE_REFRESH );
		dhex_refresh_window( global.app );
		dhex_wait_timer( MICROS_PER_FRAME );
	}
	
	dhex_free_app(global.app);
	return 0;
}
	
