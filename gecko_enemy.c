#include "gecko_enemy.h"
#include "gecko_sprite.h"
#include "gecko_animation.h"
#include "gecko_object.h"
#include "gecko_gamestate.h"
#include "obj/temp.h"

void enemy_initialize(enemydata* enemy, int16_t hp, int8_t iframes, uint8_t active) {
	
	enemy->hp = hp;
	enemy->iframes = iframes;
	enemy->active = active;
}

void enemy_generic_death( void* gs, void* _self, float x, float y ) {
	
	gamestate_play_sfx((gamestate*)gs, SFX_BREAK);
	
	object* self = _self;
	self->destroy = 1;
	
	create_temp(x, y, 0,0, 
		ANIMATION_DEATH_PARTICLE,
		DEATH_PARTICLE_NFRAMES, 1, 2 * DEATH_PARTICLE_NFRAMES, LAYER_SPR_3, 5
	);
	
}