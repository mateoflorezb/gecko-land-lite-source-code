#include "bit_to_dhex.h"
#include "gecko_magic.h"
#include "gecko_sprite.h"
#include "gecko_world.h"
#include "gecko_draw.h"

#include <stdio.h>

static int CHUNK_PALETTE[ WORLD_SCREENS_W * WORLD_SCREENS_H ] = {
	4,0,0,0,1,1,1,1,  4,
	0,0,0,0,1,1,1,1,  0,
	0,0,0,0,1,1,1,1,  4,
	2,2,2,0,0,0,1,1,  4,
	2,2,2,0,0,0,3,3,  4,
	2,2,2,0,0,3,3,3,  0,
	2,2,2,0,3,3,3,3,  0,
	0,0,0,0,3,3,3,3,  0
};

static dhex_rgba LIGHT_COLORS[] = {
	
	(dhex_rgba) {155, 188,  15, 255}, //base
	(dhex_rgba) {  0, 202, 152, 255}, //water
	(dhex_rgba) {255, 233,   0, 255}, //fire
	(dhex_rgba) {170, 120,  90, 255}, //cave 
	(dhex_rgba) {200, 200, 165, 255}  //end 
	//(dhex_rgba) {  0, 230, 210, 255}  //end 

};

static dhex_rgba DARK_COLORS[] = {
	
	(dhex_rgba) { 15,  56,  15, 255}, //base  
	(dhex_rgba) {  0,  60,  41, 255}, //water
	(dhex_rgba) { 50,   0,  95, 255}, //fire
	(dhex_rgba) {  0,   0,   0, 255}, //cave 
	(dhex_rgba) { 24,  24,  24, 255}  //end 
	//(dhex_rgba) {  0,   0,   0, 255}  //end 

};

static void _draw_chunk_extended(bit1cd_canvas* cv, int16_t x, int16_t y, mapchunk* mc) {
	mapchunk_draw( cv, x, y, mc );
	
	//nodes 
	{
		clist_iterator it = clist_start( mc->node_list );
		while (it) {
			
			//printf("Drawing node with mc=%p ; ", mc);
			
			#define NODE ( clist_get( it, mapchunk_node ) )
			
			switch (NODE.type) {
				
				case NODE_SAVE:
					draw_sprite_relative( cv, x + NODE.x, y + NODE.y, 0,0,0, SPR_SAVE, 2 );
					
					bit1cd_set_colormode( cv, BIT1CD_COLORMODE_INVERT );
					draw_sprite_relative( cv, x + NODE.x, y + NODE.y - 16, 0,0,0, SPR_CIRCLE16, 5 );
					bit1cd_set_colormode( cv, BIT1CD_COLORMODE_NORMAL );
				break;
				
				case NODE_FLY:
					draw_sprite_relative( cv, x + NODE.x, y + NODE.y, 0,0,0, SPR_FLY0, 5 );
				break;
				
				case NODE_BLOB:
					draw_sprite_relative( cv, x + NODE.x, y + NODE.y, 0,0,0, SPR_BLOB1, 2 );
				break;
				
				case NODE_TURRET:
					draw_sprite_relative( cv, x + NODE.x, y + NODE.y, 0,0,0, SPR_TURRET, 5 );
				break;
				
				case NODE_PIPE:
					draw_sprite_relative( cv, x + NODE.x, y + NODE.y, 0,0,0, SPR_PIPE, 8 );
				break;
				
				case NODE_SPIKE:
				{
					
					bit1cd_sprite* spr = NULL;
					bit1cd_sprite* outline = NULL;
					
					uint8_t flip_flags = 0;
					
					//horizontal spike 
					if (NODE.data == 0 || NODE.data == 2) {
						
						if (NODE.data == 2) { flip_flags = BIT1CD_FLIP_HORIZONTAL; }
						spr = SPR_SPIKE_H;
						outline = SPR_SPIKE_H_OUTLINE;
					}
					//vertical spike 
					else {
						
						if (NODE.data == 1) { flip_flags = BIT1CD_FLIP_VERTICAL; }
						spr = SPR_SPIKE_V;
						outline = SPR_SPIKE_V_OUTLINE;
					}
					
					draw_sprite_relative( cv, x + NODE.x +4, y + NODE.y +4, 0,0, flip_flags, outline, 5 );
					draw_sprite_relative( cv, x + NODE.x +4, y + NODE.y +4, 0,0, flip_flags, spr, 5 );
				}
				break;
				
				case NODE_ROCK:
					draw_sprite_relative( cv, x + NODE.x, y + NODE.y, 0,0,0, SPR_ROCK, 7 );
				break;
				
			}
			
			#undef NODE 
			
			it = clist_next(it);
		}
	}
	
	
	//nodes 
	{
		clist_iterator it = clist_start( mc->area_list );
		while (it) {
			
			#define AREA ( clist_get( it, mapchunk_area ) )
			
			if (AREA.type == AREA_WATER && AREA.data == 0) {
				
				for (int px = 0; px < AREA.w/2; px++)
				draw_sprite_relative( cv, x + AREA.x + 2*px, y + AREA.y, 0,0,0, SPR_WATERTOP, 7 );
			}
			
			#undef AREA
			
			it = clist_next(it);
		}
	}
	
}

int main() {
	
	init_sprites();
	init_world();
	
	bit1cd_canvas* bit_screen[ 8*8 ];
	dhex_canvas*   rgb_screen[ 8*8 ];
	
	for (int k = 0; k < 8*8; k++) {
		bit_screen[k] = bit1cd_create_canvas( SCREEN_W, SCREEN_H );
		rgb_screen[k] =   dhex_create_canvas( SCREEN_W, SCREEN_H );
		
		bit1cd_draw_rect( bit_screen[k], 0, 0, SCREEN_W, SCREEN_H, BIT1CD_COLOR_LIGHT );
	}
	
	dhex_canvas* rgbworld = dhex_create_canvas( SCREEN_W * 8, SCREEN_H * 8 );
	dhex_clear_canvas( rgbworld );
	
	for (int cy = 0; cy < 8; cy++)
	for (int cx = 0; cx < 8; cx++) {
		int chunk_id = cx + WORLD_SCREENS_W * cy;
		int screen_id = cx + 8*cy;
		mapchunk* mc = CHUNK_ARRAY[ chunk_id ];
		if (mc) {
			_draw_chunk_extended( bit_screen[screen_id], 0,0, mc );
			
			dhex_rgba light = LIGHT_COLORS[ CHUNK_PALETTE[ chunk_id ] ];
			dhex_rgba dark  =  DARK_COLORS[ CHUNK_PALETTE[ chunk_id ] ];
			
			bit_to_dhex( dark, light, &bit_screen[screen_id]->buffer, &rgb_screen[screen_id]->pixelbuffer );
			
			dhex_draw_image_memcpy( &rgb_screen[screen_id]->pixelbuffer, rgbworld, cx * SCREEN_W, cy * SCREEN_H, 0, 0);
		}
	}
	
	dhex_render_canvas( rgbworld ); 
	
	if ( dhex_dump_image_png("GeckoLandLite_world.png", &rgbworld->pixelbuffer) < 0 ) {
		printf("Couldn't do shit \n");
	}
	
	return 0;
}