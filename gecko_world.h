#ifndef _GECKO_WORLD_H
#define _GECKO_WORLD_H

#include "gecko_mapchunk.h"

#define WORLD_SCREENS_W (9)
#define WORLD_SCREENS_H (8)

extern mapchunk* CHUNK_ARRAY[ WORLD_SCREENS_W * WORLD_SCREENS_H ];

void init_world();

#endif