#ifndef _GECKO_GAMESTATE_H
#define _GECKO_GAMESTATE_H

#include "clist.h"
#include "gecko_world.h"

//how many SFX can be queued on one frame 
#define SFX_PER_FRAME (8) 
#define MAX_VOLUME (128)

#define VOLUME_ENVELOPE_RANGE (15)
#define HEAT_ENVELOPE_RANGE (2)

enum _PALETTES {
	PALETTE_BASE  = 0,
	PALETTE_WATER = 1,
	PALETTE_FIRE  = 2,
	PALETTE_CAVE  = 3,
	PALETTE_END   = 4,
	PALETTE_MAX   = 5
};

enum _UNLOCKS {
	UNLOCKED_CRYSTAL_0 = 0,
	UNLOCKED_CRYSTAL_1 = 1,
	UNLOCKED_CRYSTAL_2 = 2,
	UNLOCKED_FEATHER   = 3,
	UNLOCKED_FIRE      = 4,
	UNLOCKED_LANTERN   = 5,
	UNLOCKED_MAX       = 6
};

enum _SFX {
	SFX_NONE = -1,
	SFX_JUMP1 = 0,
	SFX_JUMP2,
	SFX_BREAK,
	SFX_SHOOT,
	SFX_BLOBJUMP,
	SFX_DEATH,
	SFX_ROAR,
	SFX_FULLROAR,
	SFX_ITEMGET,
	SFX_TELEPORT,
	SFX_WATER,
	SFX_SAVE,
	SFX_TAP,
	SFX_FIRE,
	SFX_MENU,
	SFX_START,
	SFX_ORB,
	SFX_ENEMYHIT,
	SFX_MENU_SELECT,
	SFX_MAX
};

enum _MUSIC {
	MUSIC_NOP = 0, // do nothing
	MUSIC_PAUSE,
	MUSIC_RESUME,
	MUSIC_PLAY_OVERWORLD,
	MUSIC_PLAY_BOSS
};

//access with chunk id. Stores what palette is in what chunk 
extern int CHUNK_PALETTE[ WORLD_SCREENS_W * WORLD_SCREENS_H ];

typedef int32_t object_id;

typedef struct {
	
	int64_t tickcount;
	int64_t real_tickcount;
	uint8_t pause_state;
	uint8_t queued_pause_state;
	object_id id_counter;
	float heat_intensity; //from 0 to 1. Defines how much the screen should be distorted 
	
	clist* main_object_queue; //objects get added here before being pushed to the main list 
	clist* main_object_list ;
	clist*  sub_object_queue;
	clist*  sub_object_list ; //for particles 
	
	int16_t cam_x, cam_y;
	
	int8_t chunk_x, chunk_y; //current center chunk, used to decide what chunk data to draw 
	int8_t checkpoint_x, checkpoint_y; //in chunk coords 
	
	int8_t unlocks[ UNLOCKED_MAX ];
	int8_t midnight; //bool 
	
	int8_t checkpoint_unlocks[ UNLOCKED_MAX ];
	int8_t checkpoint_midnight;
	
	uint8_t centered_in_shadow; //bool 
	
	uint8_t queued_message; //if true, message data is to be displayed by tec 
	int16_t message_delay; //how many frames to wait in a pause state before starting the message animation 
	char message_title[128];
	char message[256];
	
	//for the minimap 
	int8_t known_mapchunks[ WORLD_SCREENS_W * WORLD_SCREENS_H ];
	
	uint8_t credits; //if true, can't pause 
	
	struct {
		int current_palette;
		
		int n_sfx;
		int queued_sfx[ SFX_PER_FRAME ];
		
		int music_volume;
		int music_action; //one of _MUSIC enum 
		
		float waterfall_volume; //from 0 to 1 
		
	} app_signal; //used to tell the app some misc stuff 
	
	int music_vdelta;
	
	struct {
		int heat_envelope; //from 0(none) to 2(max)
		int music_envelope; //volume, from 0 to 15
		int sfx_envelope; //volume, from 0 to 15
	} settings;
	
	uint8_t checkpoint_locations[ WORLD_SCREENS_W * WORLD_SCREENS_H ]; //2d array of bools. Used to know where to put lil dots in the map 
	
} gamestate;

void gamestate_initialize(gamestate* gs);
void gamestate_spawn_chunk(gamestate* gs, mapchunk* mc, int16_t x, int16_t y, int16_t origin_chunk_id ); //x,y is the top left corner of the chunk in world space  
void gamestate_set_camera(gamestate* gs, int16_t cam_x, int16_t cam_y);
void gamestate_nuke_objects(gamestate* gs); //only tec will survive 
void gamestate_pause(gamestate* gs, uint8_t pause);

void gamestate_step(gamestate* gs);
void gamestate_draw(gamestate* gs);

//will delete objects with body2d outside of the loaded area and call spawn_chunk internally 
void gamestate_set_chunk(gamestate* gs, int8_t chunk_x, int8_t chunk_y);

void gamestate_set_message( gamestate* gs, const char* title, const char* message, int16_t delay );

void gamestate_music(gamestate* gs, int action); //pass one of _MUSIC values 
void gamestate_music_volume(gamestate* gs, int v); //from 0 to 128
void gamestate_play_sfx(gamestate* gs, int sfx); //pass one of _SFX values 

#endif