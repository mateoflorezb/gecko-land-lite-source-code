#ifndef _GECKO_SPRITE_H
#define _GECKO_SPRITE_H

#include "bit1cd.h"

void init_sprites();

extern bit1cd_sprite* SPR_BALL0;
extern bit1cd_sprite* SPR_BALL1;
extern bit1cd_sprite* SPR_BLOB0;
extern bit1cd_sprite* SPR_BLOB1;
extern bit1cd_sprite* SPR_DEATH0;
extern bit1cd_sprite* SPR_DEATH1;
extern bit1cd_sprite* SPR_DEATH2;
extern bit1cd_sprite* SPR_DEATH3;
extern bit1cd_sprite* SPR_FEATHER;
extern bit1cd_sprite* SPR_FIRE;
extern bit1cd_sprite* SPR_LANTERN;
extern bit1cd_sprite* SPR_CRYSTAL0;
extern bit1cd_sprite* SPR_CRYSTAL1;
extern bit1cd_sprite* SPR_FLY0;
extern bit1cd_sprite* SPR_FLY1;
extern bit1cd_sprite* SPR_TURRET;
extern bit1cd_sprite* SPR_SPIKE_H;
extern bit1cd_sprite* SPR_SPIKE_V;
extern bit1cd_sprite* SPR_SPIKE_H_OUTLINE;
extern bit1cd_sprite* SPR_SPIKE_V_OUTLINE;
extern bit1cd_sprite* SPR_PLAYER_JUMP0;
extern bit1cd_sprite* SPR_PLAYER_JUMP1;
extern bit1cd_sprite* SPR_PLAYER_JUMPSHOOT0;
extern bit1cd_sprite* SPR_PLAYER_JUMPSHOOT1;
extern bit1cd_sprite* SPR_PLAYER_WALK0;
extern bit1cd_sprite* SPR_PLAYER_WALK1;
extern bit1cd_sprite* SPR_PLAYER_WALKSHOOT0;
extern bit1cd_sprite* SPR_PLAYER_WALKSHOOT1;
extern bit1cd_sprite* SPR_PARTICLE0;
extern bit1cd_sprite* SPR_PARTICLE1;
extern bit1cd_sprite* SPR_PARTICLE2;
extern bit1cd_sprite* SPR_PARTICLE3;
extern bit1cd_sprite* SPR_PARTICLE4;
extern bit1cd_sprite* SPR_SBALL0;
extern bit1cd_sprite* SPR_SBALL1;
extern bit1cd_sprite* SPR_TBALL0;
extern bit1cd_sprite* SPR_SIGN;
extern bit1cd_sprite* SPR_ROCK;
extern bit1cd_sprite* SPR_CIRCLE16;
extern bit1cd_sprite* SPR_SAVE;
extern bit1cd_sprite* SPR_PIPE;
extern bit1cd_sprite* SPR_WATERDROP;
extern bit1cd_sprite* SPR_WATERTOP;
extern bit1cd_sprite* SPR_WATERFALL;
extern bit1cd_sprite* SPR_MOONGAZER0;
extern bit1cd_sprite* SPR_MOONGAZER1;
extern bit1cd_sprite* SPR_MARKER;
extern bit1cd_sprite* SPR_TITLE;
extern bit1cd_sprite* SPR_TILESET;
extern bit1cd_sprite* SPR_MINIMAP;
extern bit1cd_sprite* SPR_BIG_CIRCLE;

extern bit1cd_sprite* SPR_FONT_0;
extern bit1cd_sprite* SPR_FONT_1;
extern bit1cd_sprite* SPR_FONT_2;
extern bit1cd_sprite* SPR_FONT_3;
extern bit1cd_sprite* SPR_FONT_4;
extern bit1cd_sprite* SPR_FONT_5;
extern bit1cd_sprite* SPR_FONT_6;
extern bit1cd_sprite* SPR_FONT_7;
extern bit1cd_sprite* SPR_FONT_8;
extern bit1cd_sprite* SPR_FONT_9;
extern bit1cd_sprite* SPR_FONT_A;
extern bit1cd_sprite* SPR_FONT_B;
extern bit1cd_sprite* SPR_FONT_C;
extern bit1cd_sprite* SPR_FONT_D;
extern bit1cd_sprite* SPR_FONT_E;
extern bit1cd_sprite* SPR_FONT_F;
extern bit1cd_sprite* SPR_FONT_G;
extern bit1cd_sprite* SPR_FONT_H;
extern bit1cd_sprite* SPR_FONT_I;
extern bit1cd_sprite* SPR_FONT_J;
extern bit1cd_sprite* SPR_FONT_K;
extern bit1cd_sprite* SPR_FONT_L;
extern bit1cd_sprite* SPR_FONT_M;
extern bit1cd_sprite* SPR_FONT_N;
extern bit1cd_sprite* SPR_FONT_O;
extern bit1cd_sprite* SPR_FONT_P;
extern bit1cd_sprite* SPR_FONT_Q;
extern bit1cd_sprite* SPR_FONT_R;
extern bit1cd_sprite* SPR_FONT_S;
extern bit1cd_sprite* SPR_FONT_T;
extern bit1cd_sprite* SPR_FONT_U;
extern bit1cd_sprite* SPR_FONT_V;
extern bit1cd_sprite* SPR_FONT_W;
extern bit1cd_sprite* SPR_FONT_X;
extern bit1cd_sprite* SPR_FONT_Y;
extern bit1cd_sprite* SPR_FONT_Z;

extern bit1cd_sprite* SPR_FONT_APOS;
extern bit1cd_sprite* SPR_FONT_MINUS;
extern bit1cd_sprite* SPR_FONT_COMMA;
extern bit1cd_sprite* SPR_FONT_DOT;
extern bit1cd_sprite* SPR_FONT_QMARK;
extern bit1cd_sprite* SPR_FONT_NULL;

#endif