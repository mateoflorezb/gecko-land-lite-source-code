#ifndef _GECKO_GLOBAL_H
#define _GECKO_GLOBAL_H

#include "dhex_window_app.h"
#include "bit1cd.h"
#include "gecko_gamestate.h"
#include "pool.h"

typedef struct {
	dhex_app* app;
	bit1cd_canvas* main_canvas;
	bit1cd_canvas* heat_canvas;
	bit1cd_canvas* shadow_canvas;
	gamestate gs;
	
	pool* object_pool;
	pool* float_array_pool;
	
} _global;

extern _global global;

void init_global();

#endif