#include "bit1cd.h"
#include <stdio.h>
#include <stdlib.h>


int main( int argc, char* argv[] ) {

	if (argc == 1) {
		printf(
			"Usage:" "\n"
			"\n"
			"sprdump.exe <png files...>" "\n\n"
		);
	}
	
	char buffer [200];
	char buffer2[200];
	char buffer3[400];
	
	//dump as binary file 
	for (int k = 1; k < argc; k++) {
		
		bit1cd_sprite* spr = bit1cd_load_sprite_png( argv[k] );
		if (!spr) {
			printf("Failed to load %s\n", argv[k] );
			continue;
		}
		
		sprintf(buffer, "%s.bin", argv[k] );
		
		if ( !bit1cd_dump_sprite_bin(spr, buffer) ) {
			printf("Failed to dump %s\n", buffer);
		}
	}
	
	//now call rescomp 
	for (int k = 1; k < argc; k++) {
		sprintf(buffer,  "%s.bin", argv[k] );
		sprintf(buffer2, "%s.txt", argv[k] );
		sprintf(buffer3, "rescomp %s %s", buffer, buffer2);
		system(buffer3);
	}
}