#include "bit1cd.h"
#include "dhex_window_app.h"
#include "bit_to_dhex.h"
#include "gecko_magic.h"
#include "gecko_misc.h"
#include "gecko_sprite.h"
#include "gecko_draw.h"

#include <stdio.h>

#define MICROS_PER_FRAME (16667*2)

//copy pasted from gecko_input.c lol
#define DHEX_INPUT_A     DHEX_INPUT_0 
#define DHEX_INPUT_B     DHEX_INPUT_1
#define DHEX_INPUT_R     DHEX_INPUT_2 
#define DHEX_INPUT_L     DHEX_INPUT_3 
#define DHEX_INPUT_U     DHEX_INPUT_4 
#define DHEX_INPUT_D     DHEX_INPUT_5 
#define DHEX_INPUT_START DHEX_INPUT_6 

const char* _BUTTON_NAMES[] = {
	"JUMP",
	"ACTION",
	"RIGHT",
	"LEFT",
	"UP",
	"DOWN",
	"START"
};

const int NBUTTONS = 7;

int main () {
	//Initialize some resources here...
	
	init_sprites();
	
	dhex_app* app = dhex_create_app("Gecko Land Lite button config", SCREEN_W, SCREEN_H, (10.0 / 9.0), 1, 0);
	
	dhex_set_window_size( app, SCREEN_W * 3, SCREEN_H * 3);
	dhex_window_center( app );
	
	bit1cd_canvas* cv = bit1cd_create_canvas( SCREEN_W, SCREEN_H );
	
	int delay = -1; //stops at -1, acts at 0 
	int current_button = 0;
	
	char str[256];
	
	while (app->quit == 0) {
		dhex_refresh_app(app);
		
		if (delay >= 0) delay--;
		
		if (delay == 0) {
			current_button++;
			if (current_button >= NBUTTONS) {
				dhex_input_config_save( app, BUTTON_CONFIG_FILE );
				return 0;
			}
		}
		
		bit1cd_draw_rect( cv, 0,0, SCREEN_W, SCREEN_H, BIT1CD_COLOR_DARK);
		
		sprintf( str, "TAP INPUT FOR %s", _BUTTON_NAMES[current_button] );
		
		draw_text_centered( cv, SCREEN_W / 2, SCREEN_H / 2, str );
		
		if (delay < 0)
		if (app->input.any_vk) {
			dhex_input_config_set( app, current_button, 0, app->input.last_vk);
			delay = 6;
		}
		
		bit_to_dhex( (dhex_rgba){0,0,0,255}, (dhex_rgba){255,255,255,255},
			&cv->buffer, &app->main_canvas->pixelbuffer );

		dhex_refresh_window(app); 
		dhex_microsleep(MICROS_PER_FRAME);
	}
	
	dhex_free_app(app);
	return 0;

}