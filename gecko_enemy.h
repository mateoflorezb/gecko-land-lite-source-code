#ifndef _GECKO_ENEMY_H
#define _GECKO_ENEMY_H

#include "bit1cd.h"

//#define DEATH_PARTICLE_NFRAMES (4)

typedef struct {
	
	int8_t hp;
	int8_t iframes; //if > 0 can't be hit. Objects are free to implement if this is frames left or what 
	uint8_t active; //if false, can't damage player on hit 
	
} enemydata;

void enemy_initialize(enemydata* enemy, int16_t hp, int8_t iframes, uint8_t active);

//creates particle and destroys self 
//self is actually object* 
//gs is gamestate*
void enemy_generic_death( void* gs, void* _self, float x, float y );


#endif 