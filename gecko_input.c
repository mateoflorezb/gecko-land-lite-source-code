#include "gecko_input.h"
#include "gecko_global.h"
#include "gecko_magic.h"
#include "dhex_window_app_keycodes.h"
#include <string.h>
#include <stdio.h>

#define DHEX_INPUT_A     DHEX_INPUT_0 
#define DHEX_INPUT_B     DHEX_INPUT_1
#define DHEX_INPUT_R     DHEX_INPUT_2 
#define DHEX_INPUT_L     DHEX_INPUT_3 
#define DHEX_INPUT_U     DHEX_INPUT_4 
#define DHEX_INPUT_D     DHEX_INPUT_5 
#define DHEX_INPUT_START DHEX_INPUT_6 

void input_init() {
	
	FILE* f = fopen( BUTTON_CONFIG_FILE, "rb" );
	if (f) {
		fclose(f);
		dhex_input_config_load( global.app, BUTTON_CONFIG_FILE );
		return;
	}
	
	dhex_input_config_set(global.app, DHEX_INPUT_A, 0, (dhex_vk) { .type=DHEX_INPUT_HW_KEYBOARD, .hw.keyboard.key=DHEX_KEY_Z } );
	dhex_input_config_set(global.app, DHEX_INPUT_B, 0, (dhex_vk) { .type=DHEX_INPUT_HW_KEYBOARD, .hw.keyboard.key=DHEX_KEY_X } );
	
	dhex_input_config_set(global.app, DHEX_INPUT_R, 0, (dhex_vk) { .type=DHEX_INPUT_HW_KEYBOARD, .hw.keyboard.key=DHEX_KEY_RIGHT } );
	dhex_input_config_set(global.app, DHEX_INPUT_L, 0, (dhex_vk) { .type=DHEX_INPUT_HW_KEYBOARD, .hw.keyboard.key=DHEX_KEY_LEFT  } );
	dhex_input_config_set(global.app, DHEX_INPUT_U, 0, (dhex_vk) { .type=DHEX_INPUT_HW_KEYBOARD, .hw.keyboard.key=DHEX_KEY_UP    } );
	dhex_input_config_set(global.app, DHEX_INPUT_D, 0, (dhex_vk) { .type=DHEX_INPUT_HW_KEYBOARD, .hw.keyboard.key=DHEX_KEY_DOWN  } );
	
	dhex_input_config_set(global.app, DHEX_INPUT_START, 0, (dhex_vk) { .type=DHEX_INPUT_HW_KEYBOARD, .hw.keyboard.key=DHEX_KEY_RETURN } );
}

int input_held    (int input) { return dhex_input_check         ( global.app, input, 0 ); }
int input_pressed (int input) { return dhex_input_check_pressed ( global.app, input, 0 ); }
int input_released(int input) { return dhex_input_check_released( global.app, input, 0 ); }