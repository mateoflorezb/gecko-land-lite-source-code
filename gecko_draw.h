//Enhanced draw functions 

#ifndef _GECKO_DRAW_H
#define _GECKO_DRAW_H

#include "bit1cd.h"

//relative offset is numpad notation as always 
void draw_sprite_relative(bit1cd_canvas* cv, int16_t x, int16_t y, int16_t xoffset, int16_t yoffset,
	uint8_t flip_flags, bit1cd_sprite* sprite, int relative_offset);
	
void draw_big_light  ( bit1cd_canvas* cv, int16_t x, int16_t y );
void draw_small_light( bit1cd_canvas* cv, int16_t x, int16_t y );

void draw_text_centered( bit1cd_canvas* cv, int16_t x, int16_t y, const char* txt );

//pass \n in the string to break the line 
void draw_text( bit1cd_canvas* cv, int16_t x, int16_t y, const char* txt);

#endif 