/*
	Usage:
	
	/// Create new pool allocator ///
	pool* P = pool_create(sizeof(type), N);
	
	/// Get a new element ///
	type* new = pool_get(P); //will return NULL if the whole memblock is taken 
	
	/// Release element ///
	// passing an element not allocated by this pool is undefined behaviour
	// releasing the same element more than once is undefined behaviour
	pool_release(P, new); 
	
	/// Free pool from memory ///
	pool_free(P);
	
*/

#ifndef _POOL_ALLOC_H
#define _POOL_ALLOC_H

#include <stdlib.h>
#include <stdint.h>

typedef struct {
	
	uint16_t nelements;
	void* memblock;
	void** stack;
	int stack_pointer;
	
} pool;

pool* pool_create(uint16_t element_size, uint16_t nelements);
void* pool_get(pool* P);
void  pool_release(pool* P, void* a);
void  pool_free(pool* P);

#endif