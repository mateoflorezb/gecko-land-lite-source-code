/*
Pixels are 2 bits.

-first bit -> color (1 -> dark pixel, 0 -> light pixel)

-second bit -> alpha (0 for invisible, 1 for visible. Alpha is used for support canvases)
this format was chosen so that memset() with a bunch of zeros would leave the screen
with no alpha and with light pixels 

Multiple pixels will be stored in the same byte

*/

#ifndef _BIT1CD_H
#define _BIT1CD_H

#include <stdint.h>

typedef struct {
	uint8_t w, h;
	int32_t nbytes; //only initialized for canvases, not sprites 
	uint8_t* data;
} bit1cd_pixelbuffer;

typedef bit1cd_pixelbuffer bit1cd_sprite;

enum _BIT1CD_COLORFORMAT_ {
	BIT1CD_COLORFORMAT_COLOR = 0b00000001, //if this flag is raised, the color is dark 
	BIT1CD_COLORFORMAT_ALPHA = 0b00000010  //if this flag is raised, the color is opaque 
};

enum _BIT1CD_COLORS {
	BIT1CD_COLOR_NONE  = 0b00000000,
	BIT1CD_COLOR_LIGHT = 0b00000010,
	BIT1CD_COLOR_DARK  = 0b00000011
};

enum _BIT1CD_COLORMODES {
	BIT1CD_COLORMODE_NORMAL = 0,
	BIT1CD_COLORMODE_SUB_ALPHA = 1, //ignore actual color. If the source pixel is opaque, make the destination pixel transparent
	BIT1CD_COLORMODE_INVERT = 2, //ignore source color. If the source pixel is opaque, swap the destination pixel color 
	BIT1CD_COLORMODE_INVERT_SOURCE = 3 //draw sprite with opposite source colors. Useful for saving on rescomping more shit T-T
};

enum _BIT1CD_FLIP_FLAGS {
	BIT1CD_FLIP_HORIZONTAL = 0x1,
	BIT1CD_FLIP_VERTICAL   = 0x2
};

typedef struct {
	bit1cd_pixelbuffer buffer;
	uint8_t current_colormode;
} bit1cd_canvas;

bit1cd_canvas* bit1cd_create_canvas(int16_t w, int16_t h);
void bit1cd_free_canvas(bit1cd_canvas* cv);

//if the color's alpha is < 128, interpreted as *not* having BIT1CD_COLORFORMAT_ALPHA
//if the color's red is < 128, interpreted as having BIT1CD_COLORFORMAT_COLOR
bit1cd_sprite* bit1cd_load_sprite_png(const char* filename);
bit1cd_sprite* bit1cd_load_sprite_memory(const uint8_t* rescomp_file); //address to a rescomp'd block of memory from a bin dump 

//needed for the rescomp. Format is w,h (uint8_t each) then 1 byte per pixel in x+wy order 
//returns false if failed to dump.
int bit1cd_dump_sprite_bin(bit1cd_sprite* spr, const char* filename); 



void bit1cd_set_colormode(bit1cd_canvas* cv, uint8_t colormode);

//inlined for speed 
static inline uint8_t bit1cd_read_pixelvalue(bit1cd_pixelbuffer* buffer, int x, int y) {
	int nth_pixel = x + ((int)buffer->w) * y;
	int nth_byte  = nth_pixel / 4;
	int pixel_offset = nth_pixel % 4;
	return ( buffer->data[nth_byte] >> ( (3 - pixel_offset) * 2 ) ) & 0b00000011;
}

void bit1cd_clear_canvas(bit1cd_canvas* cv);

void bit1cd_draw_rect(bit1cd_canvas* cv, int16_t x, int16_t y, int16_t w, int16_t h, uint8_t color);

void bit1cd_draw_sprite(bit1cd_canvas* cv, int16_t x, int16_t y, int16_t xoffset, int16_t yoffset,
	uint8_t flip_flags, bit1cd_sprite* sprite);
	
void bit1cd_draw_sprite_ex(bit1cd_canvas* cv, int16_t x, int16_t y, int16_t xoffset, int16_t yoffset,
	float xscale, float yscale, bit1cd_sprite* sprite);
	
void bit1cd_draw_sprite_part(bit1cd_canvas* cv, int16_t x, int16_t y, int16_t xoffset, int16_t yoffset,
	int16_t xsection, int16_t ysection, int16_t wsection, int16_t hsection, bit1cd_sprite* sprite);

#endif
