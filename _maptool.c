#include "gecko_magic.h"
#include "gecko_mapchunk.h"
#include "bit1cd.h"
#include "dhex_window_app.h"
#include "dhex_window_app_keycodes.h"
#include "dhex_font.h"
#include "gecko_sprite.h"
#include "bit_to_dhex.h"
#include "gecko_misc.h"
#include "gecko_draw.h"

#include <stdio.h>
#include <string.h>


#define MICROS_PER_FRAME (16667*1)

#define RESOLUTION_W ((SCREEN_W * 2) + 30)
#define RESOLUTION_H (SCREEN_H + 70)

#define DHEX_INPUT_R     DHEX_INPUT_0 
#define DHEX_INPUT_L     DHEX_INPUT_1 
#define DHEX_INPUT_U     DHEX_INPUT_2 
#define DHEX_INPUT_D     DHEX_INPUT_3 
#define DHEX_INPUT_SPACE DHEX_INPUT_4
#define DHEX_INPUT_X     DHEX_INPUT_5

#define DEFAULT_OUTPUT_FILE "out.map"

#define BUTTON_H 14

enum _MODES {
	MODE_TILES = 0,
	MODE_NODES,
	MODE_AREAS
};

static void draw_wire_rect(dhex_rgba color, dhex_canvas* cv, int x, int y, int w, int h) {
	dhex_draw_rect(color, cv, x, y, w, 1);
	dhex_draw_rect(color, cv, x, y, 1, h);
	dhex_draw_rect(color, cv, x, y + h, w, 1);
	dhex_draw_rect(color, cv, x + w, y, 1, h);
}

static dhex_palette pal_txt;
static dhex_app* app; 
static char output_file[128];
static int mode; //one of enum _MODES  


int mouse_in_area(int x, int y, int w, int h) {
	if (
		app->input.mouse_x > x     &&
		app->input.mouse_x < x + w &&
		app->input.mouse_y > y     &&
		app->input.mouse_y < y + h 
	) { return 1; }
	
	return 0;
}

int BUTTON(const char* caption, int x, int y, int w, int h) {
	
	dhex_rgba base = (dhex_rgba){255,255,255,255};
	
	int mouse_in = mouse_in_area(x,y,w,h);
	
	if ( mouse_in && app->input.mouse_held ) {
		base = (dhex_rgba){180,180,200,255};
	}
	
	dhex_draw_rect( base, app->main_canvas, x, y, w, h );
	
	dhex_font_draw_text( &DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, caption, DHEX_FONT_ALIGN_CENTERED,
		x + w/2, y + h/2, 1,1);
		
	if ( mouse_in && app->input.mouse_pressed ) { return 1; }
	
	return 0;
}

int main ( int argc, char* argv[] ) {
	
	printf(
		"\nUsage:\n"
		"New chunk: maptool\n"
		"Edit chunk: maptool <.map file>\n"	
		"\nUse the arrow keys to select another tile type.\n"
		"Hold space + mb left to place multiple tiles.\n"
		"Hold X + mb left to remove tiles.\n"
		"Click the map area to move the current node/area.\n"
		"Press X to resize the current area to the mouse's position.\n"
		"Red areas are blocks. Yellow are platforms. Blue are water.\n\n"
	);
	
	//Initialize some resources here...
	app = dhex_create_app("Gecko Land Lite maptool", RESOLUTION_W, RESOLUTION_H, 4.0/3.0, 1, 0);
	dhex_set_window_size(app, RESOLUTION_W * 2, RESOLUTION_H * 2);
	dhex_font_init();
	init_sprites();
	
	pal_txt.ncolors = 1;
	pal_txt.colors[0] = (dhex_rgba){0,0,0,0};
	pal_txt.colors[1] = (dhex_rgba){0,0,0,255};
	
	strcpy( output_file, DEFAULT_OUTPUT_FILE );
	
	mode = MODE_TILES;
	
	dhex_rgba  dark_color = (dhex_rgba) { 15,  56, 15, 255};
	dhex_rgba light_color = (dhex_rgba) {155, 188, 15, 255};
	
	bit1cd_canvas* bit_canvas = bit1cd_create_canvas( SCREEN_W, SCREEN_H );
	dhex_canvas* chunk_showcase = dhex_create_canvas( SCREEN_W, SCREEN_H );
	dhex_canvas* tile_showcase_rgb = dhex_create_canvas( 64, 64 );
	bit_to_dhex( dark_color, light_color, SPR_TILESET, &tile_showcase_rgb->pixelbuffer );
	
	mapchunk* working_chunk;
	
	if (argc > 1) {
		working_chunk = mapchunk_load_from_file( argv[1] );
		if (strlen( argv[1] ) < 120 ) {
			strcpy( output_file, argv[1] );
		} 
	}
	else {
		working_chunk = mapchunk_new();
	}
	
	dhex_input_config_set(app, DHEX_INPUT_R, 0, (dhex_vk) { .type=DHEX_INPUT_HW_KEYBOARD, .hw.keyboard.key=DHEX_KEY_RIGHT } );
	dhex_input_config_set(app, DHEX_INPUT_L, 0, (dhex_vk) { .type=DHEX_INPUT_HW_KEYBOARD, .hw.keyboard.key=DHEX_KEY_LEFT  } );
	dhex_input_config_set(app, DHEX_INPUT_U, 0, (dhex_vk) { .type=DHEX_INPUT_HW_KEYBOARD, .hw.keyboard.key=DHEX_KEY_UP    } );
	dhex_input_config_set(app, DHEX_INPUT_D, 0, (dhex_vk) { .type=DHEX_INPUT_HW_KEYBOARD, .hw.keyboard.key=DHEX_KEY_DOWN  } );
	dhex_input_config_set(app, DHEX_INPUT_X, 0, (dhex_vk) { .type=DHEX_INPUT_HW_KEYBOARD, .hw.keyboard.key=DHEX_KEY_X     } );
	dhex_input_config_set(app, DHEX_INPUT_SPACE, 0, (dhex_vk) { .type=DHEX_INPUT_HW_KEYBOARD, .hw.keyboard.key=DHEX_KEY_SPACE  } );
	
	char str[256];
	
	int tickcount = 0;
	
	//currently selected tile type 
	int tile_type_x = 0;
	int tile_type_y = 0;
	
	//indexed from 0 
	int selected_node = 0; 
	int selected_area = 0; 
	
	while (app->quit == 0) {
		
		dhex_refresh_app(app);
		
		tickcount++;
		
		//background color 
		dhex_draw_rect( (dhex_rgba){127,127,127,255}, app->main_canvas,0,0, RESOLUTION_W, RESOLUTION_H);
		
		//chunk area 
		//dhex_draw_rect( (dhex_rgba){255,255,255,255}, app->main_canvas,0,0, SCREEN_W, SCREEN_H );
		bit1cd_clear_canvas( bit_canvas );
		mapchunk_draw( bit_canvas, 0,0, working_chunk );
		
		//current mode text 
		switch(mode) {
			case MODE_TILES: { strcpy(str, "MODE %l TILES %r"); } break;
			case MODE_NODES: { strcpy(str, "MODE %l NODES %r"); } break;
			case MODE_AREAS: { strcpy(str, "MODE %l AREAS %r"); } break;
			default:		 { strcpy(str, "MODE %l ERROR %r"); } break;
		}
		
		dhex_font_draw_text( &DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, str, DHEX_FONT_ALIGN_LEFT,
			SCREEN_W + 5, 6, 1,1 );
			
		//mode switch buttons 
		if ( BUTTON("TILES", SCREEN_W + 5, 20, 50, BUTTON_H) ) {
			mode = MODE_TILES;
		}
		
		if ( BUTTON("NODES", SCREEN_W + 5 + 55, 20, 50, BUTTON_H) ) {
			mode = MODE_NODES;
		}
		
		if ( BUTTON("AREAS", SCREEN_W + 5 + 110, 20, 50, BUTTON_H) ) {
			mode = MODE_AREAS;
		}
		
		if ( BUTTON("Export map", 10, 190, 100, BUTTON_H) ) {
			mapchunk_dump_file( working_chunk, output_file );
		}
		
		if ( BUTTON("BG COLOR", 10, 160, 100, BUTTON_H) ) {
			working_chunk->bg_color = ( working_chunk->bg_color ) ? 0 : 1;
		}
		

		//////////
		if ( working_chunk->heat ) {
			sprintf(str, "HEAT ON");
		}
		else {
			sprintf(str, "HEAT OFF");
		}
		
		if ( BUTTON( str, 130, 190, 100, BUTTON_H) ) {
			working_chunk->heat = ( working_chunk->heat ) ? 0 : 1;
		}
		
		//////////
		if ( working_chunk->shadow ) {
			sprintf(str, "SHADOW ON");
		}
		else {
			sprintf(str, "SHADOW OFF");
		}
		
		if ( BUTTON( str, 240, 190, 100, BUTTON_H) ) {
			working_chunk->shadow = ( working_chunk->shadow ) ? 0 : 1;
		}
		
		
		//mouse position in tiles relative to chunk top left 
		int selected_tile_x = 0;
		int selected_tile_y = 0;
		
		selected_tile_x = iclamp( app->input.mouse_x / 8, 0, MAPCHUNK_TILE_W - 1 );
		selected_tile_y = iclamp( app->input.mouse_y / 8, 0, MAPCHUNK_TILE_H - 1 );
		
		//////////////////// TILES ///////////////////////
		if (mode == MODE_TILES) {
			
			//Tileset preview
			{
				
				if ( dhex_input_check_pressed ( app, DHEX_INPUT_R, 0 ) ) { tile_type_x++; }
				if ( dhex_input_check_pressed ( app, DHEX_INPUT_L, 0 ) ) { tile_type_x--; }
				if ( dhex_input_check_pressed ( app, DHEX_INPUT_U, 0 ) ) { tile_type_y--; }
				if ( dhex_input_check_pressed ( app, DHEX_INPUT_D, 0 ) ) { tile_type_y++; }
				
				tile_type_x = iclamp( tile_type_x, 0, 6);
				tile_type_y = iclamp( tile_type_y, 0, 6);
				
				int preview_xbase = SCREEN_W + 10;
				int preview_ybase = 40;
				dhex_draw_image(&tile_showcase_rgb->pixelbuffer, app->main_canvas,
					preview_xbase , preview_ybase, 0,0,1,1);
					
				//red square showing selected thingy 
				if ((tickcount % 20) < 10) {
					dhex_draw_rect( (dhex_rgba){255,0,0,255}, app->main_canvas,
						preview_xbase + 3 + 9 * tile_type_x,
						preview_ybase + 3 + 9 * tile_type_y,
						4, 4);
				}
				
			}
			
			//place tiles
			{
				if (mouse_in_area(0,0,SCREEN_W, SCREEN_H)) {
					
					int selected_tile_id = selected_tile_x + selected_tile_y * MAPCHUNK_TILE_W;
					
					uint8_t selected_tile_type_id = (uint8_t)(tile_type_x + tile_type_y * 7 );
					
					if (
						  app->input.mouse_pressed ||
						( app->input.mouse_held && dhex_input_check ( app, DHEX_INPUT_SPACE, 0 ) )
					) {
						working_chunk->tiledata[selected_tile_id] = selected_tile_type_id;
					}
					
					if ( app->input.mouse_held && dhex_input_check ( app, DHEX_INPUT_X, 0 ) ) {
						working_chunk->tiledata[selected_tile_id] = MAPCHUNK_NULL_TILE;
					}
				}
			}
			
		}
		
		//////////////////// NODES ///////////////////////
		if (mode == MODE_NODES) {
			
			
			if ( BUTTON("ADD NODE", SCREEN_W + 5, 50, 80, BUTTON_H) ) {
			
				mapchunk_node node;
				node.type = NODE_FLY;
				node.data = 0;
				node.x = 0;
				node.y = 0;
				
				clist_push( working_chunk->node_list, &node );
				selected_node = clist_size( working_chunk->node_list ) - 1;
				working_chunk->nodes = clist_size( working_chunk->node_list );
			}
			
			if ( BUTTON("NODE %l", SCREEN_W + 5, 150, 70, BUTTON_H) ) {
				selected_node--;
				
			}
			
			if ( BUTTON("NODE %r", SCREEN_W + 85, 150, 70, BUTTON_H) ) {
				selected_node++;
				
			}
			
			selected_node = iclamp( selected_node, 0, clist_size( working_chunk->node_list) - 1 );
			if (selected_node < 0) { selected_node = 0; } // just in case lol 
			
			
			//draw nodes and more 
			int node_id = 0;
			clist_iterator it = clist_start( working_chunk->node_list );
			while (it != NULL) {
				
				clist_iterator next = clist_next(it);
				
				bit1cd_sprite* node_sprite = NULL;
				int relative_offset = 5;
				
				switch( clist_get( it, mapchunk_node ).type ) {
					
					case NODE_SAVE:
						relative_offset = 2;
						node_sprite = SPR_SAVE;
					break;

					case NODE_BLOB:
						node_sprite = SPR_BLOB1;
						relative_offset = 2;
					break;
					
					case NODE_FLY:
						node_sprite = SPR_FLY0;
					break;
					
					case NODE_TURRET:
						node_sprite = SPR_TURRET;
					break;
					
					case NODE_PIPE:
						node_sprite = SPR_PIPE;
						relative_offset = 8;
					break;
					
					case NODE_ITEM:
						node_sprite = SPR_LANTERN;
					break;
					
					case NODE_SPIKE:
						relative_offset = 7;
						node_sprite = SPR_SPIKE_V;
					break;
					
					case NODE_ROCK:
						relative_offset = 7;
						node_sprite = SPR_ROCK;
					break;
					
					case NODE_PORTAL:
						node_sprite = SPR_CRYSTAL1;
					break;
					
				}
				
				//move node with mouse 
				if ( node_id == selected_node && app->input.mouse_held && mouse_in_area(0,0,SCREEN_W, SCREEN_H) ) {
					clist_get( it, mapchunk_node ).x = selected_tile_x * 8;
					clist_get( it, mapchunk_node ).y = selected_tile_y * 8;
				}
				
				if ( node_id == selected_node ) {
					
					if ( BUTTON("TYPE %l", SCREEN_W + 5, 80, 80, BUTTON_H) ) {
						
						clist_get( it, mapchunk_node ).type--;
						clist_get( it, mapchunk_node ).type = (uint8_t)iclamp( clist_get( it, mapchunk_node ).type, NODE_SAVE, NODE_PORTAL );
						
					}
					
					if ( BUTTON("TYPE %r", SCREEN_W + 90, 80, 80, BUTTON_H) ) {
						
						clist_get( it, mapchunk_node ).type++;
						clist_get( it, mapchunk_node ).type = (uint8_t)iclamp( clist_get( it, mapchunk_node ).type, NODE_SAVE, NODE_PORTAL );
						
					}
					
					if ( BUTTON("ERASE NODE", SCREEN_W + 5, 100, 150, BUTTON_H) ) {
						clist_delete(it);
						selected_node = 0;
						working_chunk->nodes = clist_size( working_chunk->node_list );
						//it = next;
						break;
					}
					
					sprintf( str, "DATA %d", (int) clist_get( it, mapchunk_node ).data );
					dhex_font_draw_text( &DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, str, DHEX_FONT_ALIGN_LEFT,
						SCREEN_W + 5, 130, 1,1 );
						
					if ( BUTTON("%l", SCREEN_W + 100, 125, 20, BUTTON_H) ) {
						clist_get( it, mapchunk_node ).data--;
					}
					
					if ( BUTTON("%r", SCREEN_W + 130, 125, 20, BUTTON_H) ) {
						clist_get( it, mapchunk_node ).data++;
					}
					
				}
				
				if ( node_id != selected_node || (tickcount % 2) ) {
					draw_sprite_relative(bit_canvas,
						clist_get( it, mapchunk_node ).x,
						clist_get( it, mapchunk_node ).y,
						0,0,0,
						node_sprite, relative_offset
					);
				}
				
				it = next;
				node_id++;
			}
			
		}
		
		//draw chunk 
		bit_to_dhex( dark_color, light_color, &bit_canvas->buffer, &chunk_showcase->pixelbuffer );
		dhex_draw_image(&chunk_showcase->pixelbuffer, app->main_canvas,
			0,0, 0,0,1,1);
			
			
		//////////////////// AREAS ///////////////////////
		if (mode == MODE_AREAS) {
			
			
			if ( BUTTON("ADD AREA", SCREEN_W + 5, 50, 80, BUTTON_H) ) {
			
				mapchunk_area area;
				area.type = AREA_BLOCK;
				area.data = 0;
				area.x = 0;
				area.y = 0;
				area.w = 8;
				area.h = 8;
				
				clist_push( working_chunk->area_list, &area );
				selected_area = clist_size( working_chunk->area_list ) - 1;
				working_chunk->areas = clist_size( working_chunk->area_list );
			}
			
			if ( BUTTON("AREA %l", SCREEN_W + 5, 150, 70, BUTTON_H) ) {
				selected_area--;
				
			}
			
			if ( BUTTON("AREA %r", SCREEN_W + 85, 150, 70, BUTTON_H) ) {
				selected_area++;
				
			}
			
			selected_area = iclamp( selected_area, 0, clist_size( working_chunk->area_list) - 1 );
			if (selected_area < 0) { selected_area = 0; } // just in case lol 
			
			
			//draw nodes and more 
			int area_id = 0;
			clist_iterator it = clist_start( working_chunk->area_list );
			while (it != NULL) {
				
				clist_iterator next = clist_next(it);
				
				bit1cd_sprite* node_sprite = NULL;
				int relative_offset = 5;
				dhex_rgba color = (dhex_rgba) {0,0,0,130};
				
				switch( clist_get( it, mapchunk_node ).type ) {
					
					case AREA_BLOCK:    color = (dhex_rgba){255,  0,  0,130}; break;
					case AREA_PLATFORM: color = (dhex_rgba){255,255,  0,130}; break;
					case AREA_WATER:    color = (dhex_rgba){  0, 80,255,130}; break;
				}
				
				//move/resize area with mouse 
				if ( area_id == selected_area && mouse_in_area(0,0,SCREEN_W, SCREEN_H) ) {
					
					if ( app->input.mouse_held ) {
					
						clist_get( it, mapchunk_area ).x = selected_tile_x * 8;
						clist_get( it, mapchunk_area ).y = selected_tile_y * 8;
					}
					
					if ( dhex_input_check ( app, DHEX_INPUT_X, 0 ) ) {
						int w,h;
						w = ((selected_tile_x + 1) * 8) - clist_get( it, mapchunk_area ).x;
						h = ((selected_tile_y + 1) * 8) - clist_get( it, mapchunk_area ).y;
						
						w = iclamp( w, 8, 8 * 20 );
						h = iclamp( h, 8, 8 * 18 );
						
						clist_get( it, mapchunk_area ).w = (uint8_t)w;
						clist_get( it, mapchunk_area ).h = (uint8_t)h;
					}
				}
				
				if ( area_id == selected_area ) {
					
					if ( BUTTON("TYPE %l", SCREEN_W + 5, 80, 80, BUTTON_H) ) {
						
						clist_get( it, mapchunk_area ).type--;
						clist_get( it, mapchunk_area ).type = (uint8_t)iclamp( clist_get( it, mapchunk_area ).type, AREA_BLOCK, AREA_WATER );
						
					}
					
					if ( BUTTON("TYPE %r", SCREEN_W + 90, 80, 80, BUTTON_H) ) {
						
						clist_get( it, mapchunk_area ).type++;
						clist_get( it, mapchunk_area ).type = (uint8_t)iclamp( clist_get( it, mapchunk_area ).type, AREA_BLOCK, AREA_WATER );
						
					}
					
					if ( BUTTON("ERASE AREA", SCREEN_W + 5, 100, 150, BUTTON_H) ) {
						clist_delete(it);
						selected_area = 0;
						working_chunk->areas = clist_size( working_chunk->area_list );
						//it = next;
						break;
					}
					
					sprintf( str, "DATA %d", (int) clist_get( it, mapchunk_area ).data );
					dhex_font_draw_text( &DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, str, DHEX_FONT_ALIGN_LEFT,
						SCREEN_W + 5, 130, 1,1 );
						
					if ( BUTTON("%l", SCREEN_W + 100, 125, 20, BUTTON_H) ) {
						clist_get( it, mapchunk_area ).data--;
					}
					
					if ( BUTTON("%r", SCREEN_W + 130, 125, 20, BUTTON_H) ) {
						clist_get( it, mapchunk_area ).data++;
					}
					
				}
				
				if ( area_id != selected_area || (tickcount % 2) ) {
					
					dhex_set_colormode(DHEX_COLORMODE_BLEND, app->main_canvas);
					
					draw_wire_rect( color, app->main_canvas,
						clist_get( it, mapchunk_area ).x,
						clist_get( it, mapchunk_area ).y,
						clist_get( it, mapchunk_area ).w,
						clist_get( it, mapchunk_area ).h
					);
					
					dhex_set_colormode(DHEX_COLORMODE_SET, app->main_canvas);
				}
				
				it = next;
				area_id++;
			}
			
		}
		
		dhex_render_canvas(app->main_canvas); 
		dhex_refresh_window(app); 
		dhex_microsleep(MICROS_PER_FRAME);
	}
	
	dhex_free_app(app);
	return 0;

}