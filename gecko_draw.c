#include "gecko_draw.h"
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include "gecko_sprite.h"

static char buffer[512];

void draw_sprite_relative(bit1cd_canvas* cv, int16_t x, int16_t y, int16_t xoffset, int16_t yoffset,
	uint8_t flip_flags, bit1cd_sprite* sprite, int relative_offset)
{
	
	int w = (int)sprite->w;
	int h = (int)sprite->h;
	
	int extra_xoffset = 0;
	int extra_yoffset = 0;
		
	if (relative_offset == 8 || relative_offset == 5 || relative_offset == 2) { extra_xoffset = w / 2; }
	if (relative_offset == 9 || relative_offset == 6 || relative_offset == 3) { extra_xoffset = w    ; }
		
	if (relative_offset == 4 || relative_offset == 5 || relative_offset == 6) { extra_yoffset = h / 2; }
	if (relative_offset == 1 || relative_offset == 2 || relative_offset == 3) { extra_yoffset = h    ; }
		
	bit1cd_draw_sprite(cv, x, y, xoffset + extra_xoffset, yoffset + extra_yoffset,
		flip_flags, sprite);
}

void draw_big_light  ( bit1cd_canvas* cv, int16_t x, int16_t y ) {
	
	int16_t xoffset = SPR_BIG_CIRCLE->w / 2;
	int16_t yoffset = SPR_BIG_CIRCLE->h / 2;
	
	bit1cd_set_colormode( cv, BIT1CD_COLORMODE_SUB_ALPHA );
	bit1cd_draw_sprite( cv, x, y, xoffset, yoffset, 0, SPR_BIG_CIRCLE );
	bit1cd_set_colormode( cv, BIT1CD_COLORMODE_NORMAL );
}

void draw_small_light( bit1cd_canvas* cv, int16_t x, int16_t y ) {
	
	int16_t xoffset = SPR_CIRCLE16->w / 2;
	int16_t yoffset = SPR_CIRCLE16->h / 2;
	
	bit1cd_set_colormode( cv, BIT1CD_COLORMODE_SUB_ALPHA );
	bit1cd_draw_sprite( cv, x, y, xoffset, yoffset, 0, SPR_CIRCLE16);
	bit1cd_set_colormode( cv, BIT1CD_COLORMODE_NORMAL );
	
}

static bit1cd_sprite* _char_to_sprite(char c) {
	
	c = toupper(c);
	
	switch(c) {
		
		case '0': return SPR_FONT_0;
		case '1': return SPR_FONT_1;
		case '2': return SPR_FONT_2;
		case '3': return SPR_FONT_3;
		case '4': return SPR_FONT_4;
		case '5': return SPR_FONT_5;
		case '6': return SPR_FONT_6;
		case '7': return SPR_FONT_7;
		case '8': return SPR_FONT_8;
		case '9': return SPR_FONT_9;
		
		case 'A': return SPR_FONT_A;
		case 'B': return SPR_FONT_B;
		case 'C': return SPR_FONT_C;
		case 'D': return SPR_FONT_D;
		case 'E': return SPR_FONT_E;
		case 'F': return SPR_FONT_F;
		case 'G': return SPR_FONT_G;
		case 'H': return SPR_FONT_H;
		case 'I': return SPR_FONT_I;
		case 'J': return SPR_FONT_J;
		case 'K': return SPR_FONT_K;
		case 'L': return SPR_FONT_L;
		case 'M': return SPR_FONT_M;
		case 'N': return SPR_FONT_N;
		case 'O': return SPR_FONT_O;
		case 'P': return SPR_FONT_P;
		case 'Q': return SPR_FONT_Q;
		case 'R': return SPR_FONT_R;
		case 'S': return SPR_FONT_S;
		case 'T': return SPR_FONT_T;
		case 'U': return SPR_FONT_U;
		case 'V': return SPR_FONT_V;
		case 'W': return SPR_FONT_W;
		case 'X': return SPR_FONT_X;
		case 'Y': return SPR_FONT_Y;
		case 'Z': return SPR_FONT_Z;
		
		case ' ': return NULL;
		
		case  39: return SPR_FONT_APOS;
		case '-': return SPR_FONT_MINUS;
		case ',': return SPR_FONT_COMMA;
		case '.': return SPR_FONT_DOT;
		case '?': return SPR_FONT_QMARK;
		
		default: break;
	}
	
	return SPR_FONT_NULL;
	
} 

void draw_text_centered( bit1cd_canvas* cv, int16_t x, int16_t y, const char* txt ) {
	
	int nchars = strlen( txt );
	int16_t pixel_w = nchars * 8;
	int16_t char_x = x - pixel_w / 2;
	
	for (int c = 0; c < nchars; c++) {
		bit1cd_sprite* sprchar = _char_to_sprite( txt[c] );
		if (sprchar) bit1cd_draw_sprite( cv, char_x, y, 0, 0, 0, sprchar );
		char_x += 8;
	}
	
}

void draw_text( bit1cd_canvas* cv, int16_t x, int16_t y, const char* txt ) {
	
	int nchars = strlen( txt );
	int16_t char_x = x;
	int16_t char_y = y;
	
	for (int c = 0; c < nchars; c++) {
		
		char ch = txt[c];
		
		if ( ch == '\n' ) {
			char_y += 9;
			char_x = x;
			continue;
		}
		
		bit1cd_sprite* sprchar = _char_to_sprite( ch );
		if (sprchar) bit1cd_draw_sprite( cv, char_x, char_y, 0, 0, 0, sprchar );
		char_x += 8;
	}
}