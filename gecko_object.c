#include "gecko_object.h"
#include "gecko_global.h"

static void _empty_action( gamestate* gs, object* self, int step_or_layer ) {
	return;
}

static void* _empty_component( object* self, int component_type ) {
	return NULL;
}

static void  _empty_release(object* self) {
	return;
}

object* object_new(int object_list) {
	
	clist* object_queue = ( object_list == OBJECT_LIST_MAIN ) ? global.gs.main_object_queue : global.gs.sub_object_queue;
	object* new = pool_get( global.object_pool );
	
	new->type = OBJECT_TYPE_NONE;
	new->id = global.gs.id_counter++;
	new->destroy = 0;
	new->ignore_pause = 0;
	new->origin_chunk_id = -1;
	new->step = _empty_action;
	new->draw = _empty_action;
	new->get_component = _empty_component;
	new->release = _empty_release;
	
	clist_push( object_queue, &new );
	return new;
}

object* object_find(void* gs, uint8_t type) {
	
	clist_iterator it = clist_start( ((gamestate*)gs)->main_object_list );
	while (it != NULL) {
		
		object* obj = clist_get(it, object*);
		if (obj->destroy) { it = clist_next(it); continue; }
		if (obj->type == type) {
			return obj;
		}
		
		it = clist_next(it);
	}
	
	return NULL;
}