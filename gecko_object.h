#ifndef _GECKO_OBJECT_H
#define _GECKO_OBJECT_H

#include "gecko_gamestate.h"
#include "gecko_body2d.h"
#include "gecko_enemy.h"
#include "bit1cd.h"

struct _object;
typedef struct _object object;

typedef void  (*OBJECT_ACTION_CALLBACK)(gamestate*, object*, int); //step and draw 
typedef void* (*OBJECT_COMPONENT_CALLBACK)(object*, int); //get component type (body, enemy, etc). Idea is to return null if doesn't apply 
typedef void  (*OBJECT_RELEASE_CALLBACK)(object*); //basically a destructor. By default does nothing. Only define for objects that allocate extra memory 

enum _OBJECT_LIST {
	OBJECT_LIST_MAIN = 0,
	OBJECT_LIST_SUB
};

enum _OBJECT_TYPES {
	OBJECT_TYPE_NONE = 0,
	OBJECT_TYPE_TEC,
	OBJECT_TYPE_GECKO,
	OBJECT_TYPE_BLOCK,
	OBJECT_TYPE_PLATFORM,
	OBJECT_TYPE_WATER,
	OBJECT_TYPE_SAVE,
	OBJECT_TYPE_GECKOBALL,
	OBJECT_TYPE_ITEM,
	OBJECT_TYPE_PIPE,
	OBJECT_TYPE_PORTAL,
	OBJECT_TYPE_MOONGAZER
};

enum _STEPS {
	STEP_0 = 0,
	STEP_1,
	STEP_2
};

enum _LAYERS {
	LAYER_START = 0,
	LAYER_SPR_0 = 0, //waterfall and item sparkles; also, add the darkness into the shadow canvas here  
	LAYER_SPR_1 = 1, //item floats and pipes; add light into shadow canvas here 
	LAYER_SPR_2 = 2, //player and enemies 
	LAYER_TEC_0 = 3, //draw the shadow canvas here 
	LAYER_SPR_3 = 4, //projectile particles 
	LAYER_TEC_1 = 5, //here tec will apply the heat effect, then draw the shadow canvas on top of everything and then draw the hud 
	LAYER_END   = 6,
	LAYER_PRE_TILES = 100
};

enum _COMPONENT_TYPES {
	COMPONENT_BODY2D = 0,
	COMPONENT_ENEMYDATA
};

struct _object {
	
	uint8_t type;
	object_id id;
	uint8_t destroy; //bool 
	uint8_t ignore_pause;
	int16_t origin_chunk_id; //values < 0 mean don't destroy this on chunk despawn 
	
	OBJECT_ACTION_CALLBACK step;
	OBJECT_ACTION_CALLBACK draw;
	OBJECT_COMPONENT_CALLBACK get_component;
	OBJECT_RELEASE_CALLBACK release;
	
	union {
		
		struct {
			body2d body;
			int8_t xdir;
			uint8_t can_release_jump; //bool 
			int8_t shot_delay;
			uint8_t air_jump; //bool
			int8_t air_time; //for ignoring bonking the top of your head for a few frames 
			int8_t jump_buffer; //frames left 
			int8_t water_signal_delay; //frames left. Can't signal if > 0 
			uint16_t walk_counter; //only increases when walking, used for animation 
			int8_t down_buffer; //frames left of the down press being buffered 
			int8_t platform_delay; //frames left before ignoring platforms. Acts at 1 stops at 0
			int8_t platform_ignore; //frames left of ignoring platforms 
			uint8_t orb_ignore; //bool. Set to false once gecko touches the ground 
			uint8_t orb_floating; //bool. Set to true if currently floating inside an orb 
			bit1cd_sprite* sprite; //current displayed sprite 
			
		} gecko;
		
		struct {
			body2d body;
		} block;
		
		struct {
			uint8_t pause_reason;
			int16_t transition_delay; //frames left 
			int16_t blackbar_position; //top left corner of the big square that covers the whole screen in transitions 
			int8_t transition_target_x; //in chunks 
			int8_t transition_target_y; //in chunks 
			int8_t title_left; //frames left of drawing the title 
			int16_t message_w; //used in the message animation
			int16_t message_h; //used in the message animation
			int16_t teleport_left;
			int8_t transition_load_inventory; //bool. If true, the transition will restore inventory state 
			int8_t transition_x, transition_y; //target chunk of the transition 
			int8_t transition_spawn_gecko; //if false, gecko won't be spawned 
			int16_t boss_cutscene_left;
			uint8_t last_portal_id;
			int8_t settings_cursor; //from 0 to two. 0= music, 1= sfx, 2= heat envelope 
			
		} tec;
		
		struct {
			body2d body;
			uint8_t inactive; //if true, doesn't draw anything or take in signals 
			int npoints;
			int sound_delay; //frames left of not playing sounds on signals 
			float* surface_level;
			float* surface_speed;
			float* surface_wave_dir; 
			float* surface_wave_speed;
			float* surface_wave_dir2; 
		} water;
		
		struct {			
			body2d body;
			int16_t TTL;
		} waterdrop;
		
		struct {
			int16_t TTL;
			float x, y;
			float xspeed, yspeed;
			float friction;
			uint8_t colormode;
			int current_frame;
			int nframes;
			int max_holds;
			int holds_left;
			bit1cd_sprite** sprite_array; //address to a non-owned array of sprites (must be passed on creation) 
			int layer;
			int relative_offset;
			bit1cd_sprite* sprite; //only used if created as static temp 
			
		} temp;
		
		struct {
			body2d body;
			int16_t TTL; //just in case 
			int8_t ignore_blocks; //frames left 
		} waterfall;
		
		struct {
			float x,y;
		} pipe;
		
		struct {
			body2d body;
			enemydata enemy;
			bit1cd_sprite* sprite;
			uint8_t flip_flags;
		} spike;
		
		struct {
			float origin_x, origin_y; //basically where the base is drawn 
			body2d body;
			int t;
		} save;
		
		struct {
			body2d body;
			enemydata enemy;
			bit1cd_sprite* sprite;
			int t_offset; //so that each fly has a different animation 
			int8_t water_signal_delay; //frames left. Can't signal if > 0 
		} fly;
		
		struct {
			body2d body;
			enemydata enemy;
			bit1cd_sprite* sprite;
			int8_t water_signal_delay;
			float origin_x, origin_y;
			int16_t jump_delay;
		} blob;
		
		struct {
			body2d body;
			enemydata enemy;
			int8_t shot_xspeed;
			int8_t shot_yspeed;
		} turret;
		
		struct {
			body2d body;
			enemydata enemy;
			int16_t TTL;
		} enemyball;
		
		struct {
			body2d body;
			int16_t TTL;
			int16_t OOB_TTL; //for destroying the ball n frames after leaving the screen 
		} geckoball;
		
		struct {
			body2d body;
			object* block;
		} rock;
		
		struct {
			body2d body;
			bit1cd_sprite* sprite;
			float origin_y;
			int unlocks;
			int destroy_next_frame;
		} item;
		
		struct {
			body2d body;
			int64_t t;
			uint8_t id; // id = 0, boss portal, id = 1 new game plus 
			uint8_t active; //if true, gets drawn and can teleport 
		} portal;
		
		struct {
			body2d body;
			enemydata enemy;
			object* orbs[2];
			bit1cd_sprite* sprite;
			int8_t mode;
			int8_t last_mode; //if rolled mode == last_mode, does 1 reroll 
			float max_speed;
			float target_x, target_y;
			float orb_target_x[2];
			float orb_target_y[2];
			int16_t state_left; //time left before changing mode 
			float radius; //for da orbs
			int64_t t;
			uint16_t rng;
			int8_t phase; //set to true for second phase 
			int8_t xoffset; //for the lil shake 
			int8_t last_hp; //used to know when a hit was taken 
			int8_t flicker_left;
			int8_t light_px; //from the center of the screen to either side, how many pixels of light are to be drawn 
			int8_t fireworks_left; //so that the screen doesn't get completely destroyed by particles 
			
		} moongazer;
		
		struct {
			body2d body;
			enemydata enemy;
			int64_t t;
		} moonorb;
		
	} class;
};

//generic object 
object* object_new(int object_list);

//returns first object it finds of a certain type 
object* object_find(void* gs, uint8_t type);

#endif